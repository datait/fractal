SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Struktura tabeli dla tabeli `tbl_account`
--

DROP TABLE IF EXISTS `tbl_account`;
CREATE TABLE IF NOT EXISTS `tbl_account` (
    `fld_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `fld_status_id` int(11) UNSIGNED NOT NULL,
    `fld_class_id` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_model_id` int(11) UNSIGNED DEFAULT NULL,
    `fld_group_id` int(11) DEFAULT NULL,
    `fld_role_id` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_login` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_key_id` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_reset_datetime` datetime DEFAULT NULL,
    `fld_reset_password` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `fld_creator_id` int(11) UNSIGNED DEFAULT NULL,
    `fld_updated_at` timestamp NULL DEFAULT NULL,
    `fld_updater_id` int(11) UNSIGNED DEFAULT NULL,
    `fld_deleted_at` timestamp NULL DEFAULT NULL,
    `fld_deleter_id` int(11) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`fld_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `tbl_account`
--

INSERT INTO `tbl_account` (`fld_id`, `fld_status_id`, `fld_class_id`, `fld_model_id`, `fld_group_id`, `fld_role_id`, `fld_login`, `fld_password`, `fld_key_id`, `fld_email`, `fld_reset_datetime`, `fld_reset_password`, `fld_created_at`, `fld_creator_id`, `fld_updated_at`, `fld_updater_id`, `fld_deleted_at`, `fld_deleter_id`) VALUES
(1, 1, 'cmsUser', 1, NULL, 'cmsUser', 'admin', '$2y$13$L41lkT53U1u7cZT9zwnZReuEgxhmTYDbQ3ebGro/nLRnBcMN1AHfG', NULL, '', NULL, '', '0000-00-00 00:00:00', NULL, '2015-07-04 15:41:07', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_content`
--

DROP TABLE IF EXISTS `tbl_content`;
CREATE TABLE IF NOT EXISTS `tbl_content` (
    `fld_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `fld_system` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_type` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_group` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_params` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_sort_id` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_uri` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_parent_id` int(11) DEFAULT NULL,
    `fld_icon` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_active_id` varchar(11) COLLATE utf8_polish_ci NOT NULL DEFAULT 'yes',
    `fld_sort` int(11) UNSIGNED NOT NULL DEFAULT '0',
    `fld_default_id` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT 'no',
    `fld_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `fld_creator_id` int(11) UNSIGNED DEFAULT NULL,
    `fld_updated_at` timestamp NULL DEFAULT NULL,
    `fld_updater_id` int(11) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`fld_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `tbl_content`
--

INSERT INTO `tbl_content` (`fld_id`, `fld_system`, `fld_type`, `fld_group`, `fld_params`, `fld_sort_id`, `fld_uri`, `fld_parent_id`, `fld_icon`, `fld_name`, `fld_active_id`, `fld_sort`, `fld_default_id`, `fld_created_at`, `fld_creator_id`, `fld_updated_at`, `fld_updater_id`) VALUES
(1, 'pages', '', '', '', NULL, '', NULL, '', 'Strony', 'yes', 0, 'no', '2021-04-08 20:41:43', 1, '2021-04-08 20:41:43', 1),
(2, 'index', '', '', '', NULL, '', 1, NULL, 'Strona główna', 'yes', 0, 'yes', '2021-04-08 20:42:14', 1, '2021-04-08 20:42:14', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_content_language`
--

DROP TABLE IF EXISTS `tbl_content_language`;
CREATE TABLE IF NOT EXISTS `tbl_content_language` (
    `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `content_id` int(11) UNSIGNED NOT NULL,
    `lang` varchar(10) COLLATE utf8_polish_ci NOT NULL,
    `name` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `system` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `uri` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `created_at` int(11) DEFAULT NULL,
    `updated_at` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_content_language_content` (`content_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_feature`
--

DROP TABLE IF EXISTS `tbl_feature`;
CREATE TABLE IF NOT EXISTS `tbl_feature` (
    `fld_id` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_type_id` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_restriction` text COLLATE utf8_polish_ci NOT NULL,
    PRIMARY KEY (`fld_id`),
    KEY `fld_type_id` (`fld_type_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_log`
--

DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE IF NOT EXISTS `tbl_log` (
    `fld_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `fld_type_id` int(11) UNSIGNED NOT NULL,
    `fld_action_id` int(11) UNSIGNED NOT NULL,
    `fld_action_name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_action_color` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_kind_id` int(11) UNSIGNED DEFAULT NULL,
    `fld_object_id` int(11) UNSIGNED DEFAULT NULL,
    `fld_comment` text COLLATE utf8_polish_ci NOT NULL,
    `fld_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `fld_creator_id` int(11) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`fld_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_property`
--

DROP TABLE IF EXISTS `tbl_property`;
CREATE TABLE IF NOT EXISTS `tbl_property` (
    `fld_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `fld_feature_id` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_content_id` int(11) UNSIGNED NOT NULL,
    `fld_own_id` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT 'no',
    `fld_public_id` varchar(255) COLLATE utf8_polish_ci NOT NULL DEFAULT 'yes',
    `fld_params` tinytext COLLATE utf8_polish_ci NOT NULL,
    `fld_sort` int(11) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`fld_id`),
    KEY `fld_content_id` (`fld_content_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_schema`
--

DROP TABLE IF EXISTS `tbl_schema`;
CREATE TABLE IF NOT EXISTS `tbl_schema` (
    `fld_id` int(11) UNSIGNED NOT NULL,
    `fld_parent_id` int(11) UNSIGNED DEFAULT NULL,
    `fld_system` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_type` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_params` text COLLATE utf8_polish_ci NOT NULL,
    `fld_name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_icon` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_active_id` int(11) UNSIGNED NOT NULL,
    `fld_sort_id` int(11) UNSIGNED NOT NULL,
    PRIMARY KEY (`fld_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_search`
--

DROP TABLE IF EXISTS `tbl_search`;
CREATE TABLE IF NOT EXISTS `tbl_search` (
    `fld_name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_view` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_value` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_account_id` int(11) UNSIGNED NOT NULL,
    PRIMARY KEY (`fld_name`,`fld_view`,`fld_account_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_settings`
--

DROP TABLE IF EXISTS `tbl_settings`;
CREATE TABLE IF NOT EXISTS `tbl_settings` (
    `fld_name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_domain` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_lang_id` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_refresh_id` int(1) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`fld_domain`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_value`
--

DROP TABLE IF EXISTS `tbl_value`;
CREATE TABLE IF NOT EXISTS `tbl_value` (
    `fld_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `fld_feature_id` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_content_id` int(11) UNSIGNED NOT NULL,
    `fld_own_id` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_value` text COLLATE utf8_polish_ci NOT NULL,
    `fld_sort` int(11) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`fld_id`),
    KEY `fld_content_id` (`fld_content_id`),
    KEY `fld_feature_id` (`fld_feature_id`),
    KEY `idx_value_search` (`fld_content_id`,`fld_feature_id`,`fld_own_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_value_language`
--

DROP TABLE IF EXISTS `tbl_value_language`;
CREATE TABLE IF NOT EXISTS `tbl_value_language` (
    `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `value_id` int(11) UNSIGNED NOT NULL,
    `lang` varchar(10) COLLATE utf8_polish_ci NOT NULL,
    `value` text COLLATE utf8_polish_ci NOT NULL,
    `created_at` int(11) DEFAULT NULL,
    `updated_at` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_value_language_value` (`value_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tbl_worker`
--

DROP TABLE IF EXISTS `tbl_worker`;
CREATE TABLE IF NOT EXISTS `tbl_worker` (
    `fld_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `fld_type_id` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_status_id` int(11) UNSIGNED NOT NULL DEFAULT '1',
    `fld_last_name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_first_name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
    `fld_email` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_mobile` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
    `fld_description` text COLLATE utf8_polish_ci,
    `fld_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `fld_creator_id` int(11) UNSIGNED DEFAULT NULL,
    `fld_updated_at` timestamp NULL DEFAULT NULL,
    `fld_updater_id` int(11) UNSIGNED DEFAULT NULL,
    `fld_deleted_at` timestamp NULL DEFAULT NULL,
    `fld_deleter_id` int(11) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`fld_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `tbl_worker`
--

INSERT INTO `tbl_worker` (`fld_id`, `fld_type_id`, `fld_status_id`, `fld_last_name`, `fld_first_name`, `fld_email`, `fld_mobile`, `fld_description`, `fld_created_at`, `fld_creator_id`, `fld_updated_at`, `fld_updater_id`, `fld_deleted_at`, `fld_deleter_id`) VALUES
(1, 'webmaster', 1, 'admin', 'admin', NULL, '', '', '0000-00-00 00:00:00', NULL, '2015-03-20 18:28:39', 1, NULL, NULL);

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
