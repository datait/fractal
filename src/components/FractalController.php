<?php

namespace datait\fractal\components;

use yii\filters\AccessControl;
use yii\web\Controller;

class FractalController extends Controller
{
    public $layout = 'main';

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            '@',
                        ],
                    ],
                ],
            ],
        ];
    }
}
