<?php

namespace datait\fractal\controllers;

use datait\fractal\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class UserController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'login',
                        ],
                        'allow' => true,
                        'roles' => [
                            '?',
                            '@',
                        ],
                    ],
                    [
                        'actions' => [
                            'logout',
                        ],
                        'allow' => true,
                        'roles' => [
                            '@',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'guest';

        $model = new LoginForm();
        if (
            $model->load(Yii::$app->request->post())
            && $model->login()
        ) {
            return $this->redirect('/cms');
        }

        return $this->render(
            'login',
            [
                'model' => $model,
            ]
        );
    }

    /**
     * @return Response
     */
    public function actionLogout(): Response
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
