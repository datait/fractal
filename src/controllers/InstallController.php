<?php

namespace datait\fractal\controllers;

use datait\fractal\models\Account;
use datait\fractal\models\Worker;
use Yii;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\Response;

class InstallController extends Controller {
	public $defaultAction = 'run';

    /**
     * @return Response
     * @throws Exception
     */
	public function actionRun(): Response
    {
		if (count(Yii::$app->db->createCommand('SHOW TABLES')->queryAll()) == 0) {
			$sqls = file_get_contents(__DIR__ . '/../assets/install.sql');
			
			foreach (explode(';', $sqls) as $sql) {
			    if (!empty(trim($sql))) {
                    Yii::$app->db->createCommand($sql)->execute();
                }
			}

			if (!Account::find()->one()) {
				$worker = new Worker;
				$worker->fld_first_name = 'admin';
				$worker->fld_last_name = 'admin';
				$worker->fld_type_id = Worker::TYPE_WEBMASTER;
				$worker->fld_status_id = Worker::STATUS_ACTIVE;
				$worker->save();

				$account = new Account;
				$account->fld_worker_id = $worker->fld_id;
				$account->fld_status_id = Account::STATUS_ACTIVE;
				$account->fld_first_name = 'admin';
				$account->fld_last_name = 'admin';
				$account->fld_login = 'admin';
				$account->fld_password = 'admin';
				$account->save();
			}
		}

		return $this->redirect(
		    '/cms/dashboard'
        );
	}
}
