<?php

namespace datait\fractal\controllers;

use datait\fractal\components\FractalController;
use Yii;
use yii\db\Exception;

class DashboardController extends FractalController {
    /**
     * @return string
     * @throws Exception
     */
	public function actionIndex(): string
    {
		if (!Yii::$app->user->can('cms/dashboard/index')) {
			throw new Exception('Brak uprawnień');
		}

		return $this->render('index');
	}
}
