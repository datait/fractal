<?php

namespace datait\fractal\controllers;

use datait\fractal\models\Content;
use datait\fractal\models\ContentLanguage;
use datait\fractal\components\FractalController;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use function array_merge;
use function count;

/**
 * Class ContentController
 * @package app\controllers
 */
class ContentController extends FractalController
{
    /**
     * @param null $parent_id
     * @return string|Response
     * @throws Exception
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionCreate($parent_id = null)
    {
        $model = new Content();
        $model->fld_active_id = 'yes';

        if ($parent_id !== null) {
            $model->fld_parent_id = $parent_id;
        }

        if (!Yii::$app->user->can('cms/content/create', ['content' => $model])) {
            throw new Exception('Brak uprawnień');
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();

            $model->saveProperties(Yii::$app->request->post('Feature'));

            if (Yii::$app->user->can('cms/content/update', ['content' => $model])) {
                return $this->redirect(['update', 'id' => $model->fld_id]);
            }

            return $this->redirect(['view', 'id' => $model->fld_id]);
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * @param $id
     * @param null $active
     * @return string
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionView($id, $active = null)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('cms/content/view', ['content' => $model])) {
            throw new Exception('Brak uprawnień');
        }

        if (isset($active)) {
            return $this->render('view', ['model' => $model, 'active' => $active]);
        }

        return $this->render('view', ['model' => $model]);
    }

    /**
     * @param $id
     * @param null $active
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionUpdate($id, $active = null)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('cms/content/update', ['content' => $model])) {
            throw new Exception('Brak uprawnień');
        }

        $translations = $this->prepareTranslations($model);

        if ($post = Yii::$app->request->post()) {
            if ($model->load($post)) {
                $model->save();
            }

            ContentLanguage::saveTranslations($translations, $post, $model->fld_id);

            $model->saveProperties(Yii::$app->request->post('Feature'));

            if (Yii::$app->user->can('cms/content/update', ['content' => $model])) {
                return $this->redirect(['update', 'id' => $model->fld_id, 'active' => 'content']);
            }

            return $this->redirect(['view', 'id' => $model->fld_id, 'active' => 'content']);
        }

        return $this->render('update', array_merge(
            [
                'model' => $model,
                'translations' => $translations,
            ],
            $active === null ? [] : ['active' => $active]
        ));
    }

    public function actionSort()
    {
        if (($post = Yii::$app->request->post()) && $ids = explode(',', $post['ids'])) {
            $model = $this->findModel($ids[0]);
            if ($model->root->fld_sort_id === Content::SORT_DESC_YES) {
                $index = count($ids) + 1;
                foreach ($ids as $id) {
                    if ($model = Content::findOne($id)) {
                        $model->fld_sort = $index;
                        $model->save();
                    }

                    $index--;
                }
            } else {
                $index = 1;
                foreach ($ids as $id) {
                    if ($model = Content::findOne($id)) {
                        $model->fld_sort = $index;
                        $model->save();
                    }

                    $index++;
                }
            }
        }
    }

    /**
     * @param $id
     * @param null $active
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionActivate($id, $active = null)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('cms/content/activate', ['content' => $model])) {
            throw new Exception('Brak uprawnień');
        }

        $model->doActivate();

        if (Yii::$app->user->can('cms/content/update', ['content' => $model])) {
            return $this->redirect(['update', 'id' => $model->fld_id, 'active' => 'content']);
        }

        return $this->redirect(['view', 'id' => $model->fld_id, 'active' => 'content']);
    }

    /**
     * @param $id
     * @param null $action
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDeactivate($id, $action = null)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('cms/content/deactivate', ['content' => $model])) {
            throw new Exception('Brak uprawnień');
        }

        $model->doDeactivate();

        if (Yii::$app->user->can('cms/content/update', ['content' => $model])) {
            return $this->redirect(['update', 'id' => $model->fld_id, 'active' => 'content']);
        }

        return $this->redirect(['view', 'id' => $model->fld_id, 'active' => 'content']);
    }

    /**
     * @param $id
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionUp($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('cms/content/update', ['content' => $model])) {
            throw new Exception('Brak uprawnień');
        }

        $model->doUp();

        if ($model->fld_parent_id) {
            if (Yii::$app->user->can('cms/content/update', ['content' => $model->parent])) {
                return $this->redirect(['update', 'id' => $model->fld_parent_id, 'active' => 'contents']);
            }

            return $this->redirect(['view', 'id' => $model->fld_parent_id, 'active' => 'contents']);
        }

        return $this->redirect(['update', 'id' => $model->fld_id]);
    }

    /**
     * @param $id
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDown($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('cms/content/update', ['content' => $model])) {
            throw new Exception('Brak uprawnień');
        }

        $model->doDown();

        if ($model->fld_parent_id) {
            if (Yii::$app->user->can('cms/content/update', ['content' => $model->parent])) {
                return $this->redirect(['update', 'id' => $model->fld_parent_id, 'active' => 'contents']);
            }

            return $this->redirect(['view', 'id' => $model->fld_parent_id, 'active' => 'contents']);
        }

        return $this->redirect(['update', 'id' => $model->fld_id]);
    }

    /**
     * @return Response
     */
    public function actionFlush(): Response
    {
	    if (Yii::$app->cache->flush()) {
			Yii::$app->session->setFlash(
				'info',
				'Cache został wyczyszczony'
			);
	    }

    	return $this->redirect('/');
    }

    /**
     * @return Response
     */
    public function actionClearSession(): Response
    {
        Yii::$app->session->removeAll();

        Yii::$app->session->setFlash(
            'info',
            'Zmienne sesyjne zostały usunięte'
        );

        return $this->redirect('/');
    }

    /**
     * @param $id
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('cms/content/delete', ['content' => $model])) {
            throw new Exception('Brak uprawnień');
        }

        $model->delete();

        if ($model->fld_parent_id) {
            if (Yii::$app->user->can('cms/content/update', ['content' => $model->parent])) {
                return $this->redirect(['update', 'id' => $model->fld_parent_id]);
            }

            return $this->redirect(['view', 'id' => $model->fld_parent_id]);
        }

        return $this->redirect('/');
    }

    /**
     * @param $id
     * @return Content|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param Content $model
     * @return array [lang] => [translation]
     */
    public function prepareTranslations($model)
    {
        $translations = [];

        $languages = !empty(Yii::$app->params['additionalLanguages']) ? Yii::$app->params['additionalLanguages'] : [];
        $translatedContent = $model->translatedContent;

        foreach ($translatedContent as $trans) {
            if (!in_array($trans->lang, $languages, true)) {
                continue;
            }

            $translations[$trans->lang] = $trans;
        }

        return $translations;
    }
}
