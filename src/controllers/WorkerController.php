<?php

namespace datait\fractal\controllers;

use datait\fractal\models\Account;
use datait\fractal\models\Worker;
use datait\fractal\components\FractalController;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class WorkerController extends FractalController {
    /**
     * @return string
     */
    public function actionIndex(): string
    {
		return $this->render('index');
	}

    /**
     * @param $type_id
     * @return string|Response
     * @throws Exception
     */
	public function actionCreate($type_id) {
		$model = new Worker;

		$model->fld_type_id = $type_id;
		$model->fld_status_id = Worker::STATUS_INACTIVE;
		/*
		if (!Yii::$app->user->can('worker/create', ['worker' => $model])) {
			throw new \yii\db\Exception('Brak uprawnień');
		}
		*/
		$account = new Account;

		if (Yii::$app->request->post()) {
			if ($model->load(Yii::$app->request->post())) {
				if (!$model->save()) {
					throw new Exception('Błąd zapisu pracownika do bazy danych');
				}
			} else {
				return $this->render('create', ['model' => $model, 'account' => $account]);
			}

			if ($account->load(Yii::$app->request->post())) {
			    $account->fld_class_id = 'cmsUser';
				$account->fld_model_id = $model->fld_id;
				$account->fld_email = $model->fld_email;
				$account->fld_role_id = Account::ROLE_CMS_USER;
				$account->fld_reset_password = null;
				
				if (!$account->save()) {
					throw new Exception('Błąd zapisu konta do bazy danych');
				}
			}

			return $this->redirect(['view', 'id' => $model->fld_id]);
		} else {
			return $this->render('create', ['model' => $model, 'account' => $account]);
		}
	}

    /**
     * @param $id
     * @param null $active
     * @return string
     * @throws NotFoundHttpException
     */
	public function actionView($id, $active = null): string
    {
		/*
		if (!Yii::$app->user->can('worker/view', ['worker_id' => $id])) {
			throw new \yii\db\Exception('Brak uprawnień');
		}
		*/

		$model = $this->findModel($id);

		if (isset($active)) {
			return $this->render('view', ['model' => $model, 'active' => $active]);
		} else {
			return $this->render('view', ['model' => $model]);
		}
	}

    /**
     * @param $id
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		/*
		if (!Yii::$app->user->can('worker/update', ['worker' => $model])) {
			throw new \yii\db\Exception('Brak uprawnień');
		}
		*/

		if (!$account = $model->account) {
			$account = new Account;
			$account->fld_class_id = 'cmsUser';
			$account->fld_model_id = $model->fld_id;
		}

		unset($account->fld_password);

		if ($model->load(Yii::$app->request->post())) {
			if (!$model->save()) {
				throw new Exception('Błąd zapisu pracownika do bazy danych');
			}

			if ($account->load(Yii::$app->request->post())) {
				if (empty($account->fld_password)) {
					unset($account->fld_password);
				}

				if (!$account->save()) {
					throw new Exception('Błąd zapisu konta do bazy danych');
				}
			}

			return $this->redirect(['view', 'id' => $model->fld_id]);
		} else {
			return $this->render('update', ['model' => $model, 'account' => $account]);
		}
	}

    /**
     * @param $id
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
	public function actionActivate($id): Response
    {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/worker/activate', ['worker' => $model])) {
			throw new Exception('Brak uprawnień');
		}

		$model->activate();

		return $this->redirect(['view', 'id' => $model->fld_id]);
	}

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Exception
     */
	public function actionDeactivate($id): Response
    {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/worker/deactivate', ['worker' => $model])) {
			throw new Exception('Brak uprawnień');
		}

		$model->deactivate();

		return $this->redirect(['view', 'id' => $model->fld_id]);
	}

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
	public function actionDelete($id): Response
    {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/worker/delete', ['worker' => $model])) {
			throw new \yii\db\Exception('Brak uprawnień');
		}

		$model->delete();

		return $this->redirect(['index']);
	}

    /**
     * @param $id
     * @return Worker|null
     * @throws NotFoundHttpException
     */
	protected function findModel($id): Worker
    {
		if (($model = Worker::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
