<?php

namespace datait\fractal\controllers;

use datait\fractal\components\FractalController;
use datait\fractal\models\Property;
use Yii;
use yii\db\Exception;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class PropertyController extends FractalController {
    /**
     * @return string
     * @throws Exception
     */
	public function actionIndex(): string
    {
		if (!Yii::$app->user->can('cms/property/index')) {
			throw new Exception('Brak uprawnień');
		}

		return $this->render('index');
	}

    /**
     * @param null $content_id
     * @param string $own_id
     * @return string|Response
     * @throws Exception
     */
	public function actionCreate($content_id = null, $own_id = Property::OWN_NO) {
		$model = new Property;
		$model->fld_content_id = $content_id;
		$model->fld_own_id = $own_id;

		if (!Yii::$app->user->can('cms/property/create', ['property' => $model])) {
			throw new Exception('Brak uprawnień');
		}

		if ($model->load(Yii::$app->request->post())) {
			if (!$model->save()) {
				throw new Exception('Błąd zapisu cechy do bazy danych');
			}

			if (Yii::$app->user->can('cms/content/update', ['property' => $model->content])) {
				return $this->redirect(['/cms/content/update', 'id' => $model->fld_content_id, 'active' => 'properties']);
			} else {
				return $this->redirect(['/cms/content/view', 'id' => $model->fld_content_id, 'active' => 'properties']);
			}
		} else {
			return $this->render('create', ['model' => $model]);
		}
	}

    /**
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('cms/property/update', ['property' => $model])) {
            throw new Exception('Brak uprawnień');
        }

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->save()) {
                throw new Exception('Błąd zapisu cechy do bazy danych');
            }

            if (Yii::$app->user->can('cms/content/update', ['property' => $model->content])) {
                return $this->redirect(['/cms/content/update', 'id' => $model->fld_content_id, 'active' => 'properties']);
            } else {
                return $this->redirect(['/cms/content/view', 'id' => $model->fld_content_id, 'active' => 'properties']);
            }
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    /**
     * @param $id
     * @param null $active
     * @return string
     * @throws Exception
     * @throws NotFoundHttpException
     */
	public function actionView($id, $active = null): string
    {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/property/view', ['property' => $model])) {
			throw new Exception('Brak uprawnień');
		}

		return $this->render('view', isset($active) ? ['model' => $model, 'active' => $active] : ['model' => $model]);
	}

    public function actionUp($id) {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/property/update', ['property' => $model])) {
			throw new Exception('Brak uprawnień');
		}

        if ($prev = $model->getPrev()) {
            if (!Yii::$app->user->can('cms/property/update', ['property' => $prev])) {
                throw new \yii\db\Exception('Brak uprawnień');
            }
        }

		$model->doUp($prev);

		if (Yii::$app->user->can('cms/content/update', ['content' => $model->content])) {
			return $this->redirect(['/cms/content/update', 'id' => $model->fld_content_id, 'active' => 'properties']);
		} else {
			return $this->redirect(['/cms/content/view', 'id' => $model->fld_content_id, 'active' => 'properties']);
		}
	}

    /**
     * @param $id
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
	public function actionDown($id): Response
    {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/property/update', ['property' => $model])) {
			throw new Exception('Brak uprawnień');
		}

        if ($next = $model->getNext()) {
            if (!Yii::$app->user->can('cms/property/update', ['property' => $next])) {
                throw new \yii\db\Exception('Brak uprawnień');
            }
        }

		$model->doDown($next);

		if (Yii::$app->user->can('cms/content/update', ['content' => $model->content])) {
			return $this->redirect(['/cms/content/update', 'id' => $model->fld_content_id, 'active' => 'properties']);
		} else {
			return $this->redirect(['/cms/content/view', 'id' => $model->fld_content_id, 'active' => 'properties']);
		}
	}

    /**
     * @param $id
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
	public function actionDelete($id): Response
    {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/property/delete', ['model' => $model])) {
			throw new Exception('Brak uprawnień');
		}

		$model->delete();

		if (Yii::$app->user->can('cms/content/update', ['property' => $model->content])) {
			return $this->redirect(['/cms/content/update', 'id' => $model->fld_content_id, 'active' => 'properties']);
		} else {
			return $this->redirect(['/cms/content/view', 'id' => $model->fld_content_id, 'active' => 'properties']);
		}
	}

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
	protected function findModel($id) {
		if (($model = Property::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
