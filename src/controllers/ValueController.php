<?php

namespace datait\fractal\controllers;

use datait\fractal\components\FractalController;
use datait\fractal\models\Value;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ValueController extends FractalController {
    /**
     * @param $id
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
	public function actionUp($id): Response
    {
		if (!Yii::$app->user->can('cms/value/update')) {
			throw new Exception('Brak uprawnień');
		}

		$model = $this->findModel($id);
		$model->doUp();

		if (Yii::$app->user->can('cms/content/update', ['content' => $model->content])) {
			return $this->redirect(['/cms/content/update', 'id' => $model->fld_content_id, 'active' => 'content']);
		} else {
			return $this->redirect(['/cms/content/view', 'id' => $model->fld_content_id, 'active' => 'content']);
		}
	}

    /**
     * @param $id
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
	public function actionDown($id): Response
    {
		if (!Yii::$app->user->can('cms/value/update')) {
			throw new Exception('Brak uprawnień');
		}

		$model = $this->findModel($id);
		$model->doDown();

		if (Yii::$app->user->can('cms/content/update', ['content' => $model->content])) {
			return $this->redirect(['/cms/content/update', 'id' => $model->fld_content_id, 'active' => 'content']);
		} else {
			return $this->redirect(['/cms/content/view', 'id' => $model->fld_content_id, 'active' => 'content']);
		}
	}

    /**
     * @throws Exception
     */
	public function actionSort() {
		if (!Yii::$app->user->can('cms/value/update')) {
			throw new Exception('Brak uprawnień');
		}

		if (($post = Yii::$app->request->post()) && $ids = explode(',', $post['ids'])) {
			Value::doSort($ids);
		}
	}

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Throwable
     * @throws Exception
     * @throws StaleObjectException
     */
	public function actionDelete($id): Response
    {
		if (!Yii::$app->user->can('cms/value/delete')) {
			throw new Exception('Brak uprawnień');
		}

		$model = $this->findModel($id);
		$model->delete();

		return $this->redirect(['/cms/content/update', 'id' => $model->fld_content_id, 'active' => 'content']);
	}

    /**
     * @param $id
     * @return Value
     * @throws NotFoundHttpException
     */
	protected function findModel($id): Value
    {
		if (($model = Value::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
