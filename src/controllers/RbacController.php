<?php

namespace datait\fractal\controllers;

use app\rbac\ContentActivateRule;
use app\rbac\ContentCreateRule;
use app\rbac\ContentDeactivateRule;
use app\rbac\ContentDeleteRule;
use app\rbac\ContentHelpRule;
use app\rbac\ContentIndexRule;
use app\rbac\ContentUpdateRule;
use app\rbac\ContentViewRule;
use app\rbac\DashboardIndexRule;
use app\rbac\FeatureCreateRule;
use app\rbac\FeatureDeleteRule;
use app\rbac\FeatureIndexRule;
use app\rbac\FeatureUpdateRule;
use app\rbac\FeatureViewRule;
use app\rbac\LogIndexRule;
use app\rbac\PropertyCreateRule;
use app\rbac\PropertyDeleteRule;
use app\rbac\PropertyIndexRule;
use app\rbac\PropertyUpdateRule;
use app\rbac\PropertyViewRule;
use app\rbac\ValueDeleteRule;
use app\rbac\ValueUpdateRule;
use app\rbac\WorkerActivateRule;
use app\rbac\WorkerCreateRule;
use app\rbac\WorkerDeactivateRule;
use app\rbac\WorkerUpdateRule;
use app\rbac\WorkerViewRule;
use datait\fractal\components\FractalController;
use Yii;

class RbacController extends FractalController {
	public function actionInit() {
		$auth = Yii::$app->authManager;
		$auth->removeAll();

		$worker = $auth->createRole('worker');
		$auth->add($worker);

		$rule = new DashboardIndexRule; $auth->add($rule); $perm = $auth->createPermission('/cms/dashboard/index'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);

		$rule = new WorkerCreateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/worker/create'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new WorkerUpdateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/worker/update'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new WorkerActivateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/worker/activate'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new WorkerDeactivateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/worker/deactivate'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new WorkerViewRule; $auth->add($rule); $perm = $auth->createPermission('/cms/worker/view'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);

		$rule = new LogIndexRule; $auth->add($rule); $perm = $auth->createPermission('/cms/log/index'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);

		$rule = new ContentIndexRule; $auth->add($rule); $perm = $auth->createPermission('/cms/content/index'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new ContentCreateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/content/create'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new ContentViewRule; $auth->add($rule); $perm = $auth->createPermission('/cms/content/view'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new ContentUpdateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/content/update'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new ContentHelpRule; $auth->add($rule); $perm = $auth->createPermission('/cms/content/help'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);

		$rule = new ContentActivateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/content/activate'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new ContentDeactivateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/content/deactivate'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new ContentDeleteRule; $auth->add($rule); $perm = $auth->createPermission('/cms/content/delete'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);

		$rule = new ValueUpdateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/value/update'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new ValueDeleteRule; $auth->add($rule); $perm = $auth->createPermission('/cms/value/delete'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);

		$rule = new FeatureIndexRule; $auth->add($rule); $perm = $auth->createPermission('/cms/feature/index'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new FeatureCreateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/feature/create'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new FeatureViewRule; $auth->add($rule); $perm = $auth->createPermission('/cms/feature/view'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new FeatureUpdateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/feature/update'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new FeatureDeleteRule; $auth->add($rule); $perm = $auth->createPermission('/cms/feature/delete'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);

		$rule = new PropertyIndexRule; $auth->add($rule); $perm = $auth->createPermission('/cms/property/index'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new PropertyCreateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/property/create'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new PropertyViewRule; $auth->add($rule); $perm = $auth->createPermission('/cms/property/view'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new PropertyUpdateRule; $auth->add($rule); $perm = $auth->createPermission('/cms/property/update'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
		$rule = new PropertyDeleteRule; $auth->add($rule); $perm = $auth->createPermission('/cms/property/delete'); $perm->ruleName = $rule->name; $auth->add($perm); $auth->addChild($worker, $perm);
	}
}