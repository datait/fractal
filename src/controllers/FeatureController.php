<?php

namespace datait\fractal\controllers;

use datait\fractal\components\FractalController;
use datait\fractal\models\Feature;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FeatureController extends FractalController {
    /**
     * @return string
     * @throws Exception
     */
	public function actionIndex(): string
    {
		if (!Yii::$app->user->can('cms/feature/index')) {
			throw new Exception('Brak uprawnień');
		}

		return $this->render('index');
	}

    /**
     * @param null $parent_id
     * @return string|Response
     * @throws Exception
     */
	public function actionCreate($parent_id = null) {
		$model = new Feature;

		if (!Yii::$app->user->can('cms/feature/create', ['feature' => $model])) {
			throw new Exception('Brak uprawnień');
		}

		if ($model->load(Yii::$app->request->post())) {
			if (!$model->save()) {
				throw new Exception('Błąd zapisu cechy do bazy danych');
			}

			if (Yii::$app->user->can('cms/feature/update', ['feature' => $model])) {
				return $this->redirect(['update', 'id' => $model->fld_id]);
			} else {
				return $this->redirect(['view', 'id' => $model->fld_id]);
			}
		} else {
			return $this->render('create', ['model' => $model]);
		}
	}

    /**
     * @param $id
     * @param null $active
     * @return string
     * @throws Exception
     * @throws NotFoundHttpException
     */
	public function actionView($id, $active = null): string
    {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/feature/view', ['feature' => $model])) {
			throw new Exception('Brak uprawnień');
		}

		if (isset($active)) {
			return $this->render('view', ['model' => $model, 'active' => $active]);
		} else {
			return $this->render('view', ['model' => $model]);
		}
	}

    /**
     * @param $id
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/feature/update', ['feature' => $model])) {
			throw new Exception('Brak uprawnień');
		}

		if ($model->load(Yii::$app->request->post())) {
			if (!$model->save()) {
				throw new Exception('Błąd zapisu cechy do bazy danych');
			}

			if (Yii::$app->user->can('cms/feature/update', ['feature' => $model])) {
				return $this->redirect(['update', 'id' => $model->fld_id]);
			} else {
				return $this->redirect(['view', 'id' => $model->fld_id]);
			}
		} else {
			return $this->render('update', ['model' => $model]);
		}
	}

    /**
     * @param $id
     * @return Response
     * @throws Exception
     * @throws Throwable
     * @throws StaleObjectException
     */
	public function actionDelete($id): Response
    {
		$model = $this->findModel($id);

		if (!Yii::$app->user->can('cms/feature/delete', ['feature' => $model])) {
			throw new Exception('Brak uprawnień');
		}

		$model->delete();

		return $this->redirect(['index']);
	}

    /**
     * @param $id
     * @return Feature|null
     * @throws NotFoundHttpException
     */
	protected function findModel($id): Feature
    {
		if (($model = Feature::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
