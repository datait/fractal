<?php

namespace datait\fractal\models;

use yii\base\Model;

class Lang extends Model {
	const LANG_POLISH = 'PL';
	const LANG_ENGLISCH = 'EN';
	const LANG_DEUTSCH = 'DE';

	public static function langs() {
		return [
			Lang::LANG_POLISH => ['fld_id' => Lang::LANG_POLISH, 'fld_name' => 'Polski'],
			Lang::LANG_ENGLISCH => ['fld_id' => Lang::LANG_ENGLISCH, 'fld_name' => 'Angielski'],
			Lang::LANG_DEUTSCH => ['fld_id' => Lang::LANG_DEUTSCH, 'fld_name' => 'Niemiecki']
		];
	}

	public static function lang($lang_id) {
		return self::langs()[$lang_id];
	}
}