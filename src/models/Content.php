<?php

namespace datait\fractal\models;

use Throwable;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\UploadedFile;
use function array_merge;
use function array_values;
use function count;
use function date;
use function is_array;
use function is_string;
use function str_replace;
use function strtolower;
use function trim;

/**
 * Klasa zawartości
 * @author Marcin Korytkowski
 *
 * @property int fld_id
 * @property string fld_system
 * @property string fld_type
 * @property string fld_group
 * @property string fld_params
 * @property string fld_sort_id
 * @property string fld_uri
 * @property int fld_parent_id
 * @property string fld_icon
 * @property string fld_name
 * @property string fld_active_id
 * @property int fld_sort
 * @property string fld_default_id
 * @property int fld_created_at
 * @property int fld_creator_id
 * @property int fld_updated_at
 * @property int fld_updater_id
 *
 * @property array valuesByFeatureId
 * @property array valuesValueByFeatureId
 * @property ContentLanguage[] translatedContent
 */
class Content extends ActiveRecord
{
    const ACTIVE_NO = 'no';
    const ACTIVE_YES = 'yes';

    const SORT_DESC_NO = 'no';
    const SORT_DESC_YES = 'yes';

    const DEFAULT_NO = 'no';
    const DEFAULT_YES = 'yes';

    public static $actives = [
        self::ACTIVE_NO => [
            'fld_id' => self::ACTIVE_NO,
            'fld_name' => 'Nie',
        ],
        self::ACTIVE_YES => [
            'fld_id' => self::ACTIVE_YES,
            'fld_name' => 'Tak',
        ],
    ];

    public static $sorts = [
        self::SORT_DESC_NO => [
            'fld_id' => self::SORT_DESC_NO,
            'fld_name' => 'Nie',
        ],
        self::SORT_DESC_YES => [
            'fld_id' => self::SORT_DESC_YES,
            'fld_name' => 'Tak',
        ],
    ];

    public static $defaults = [
        self::DEFAULT_NO => [
            'fld_id' => self::DEFAULT_NO,
            'fld_name' => 'Nie',
        ],
        self::DEFAULT_YES => [
            'fld_id' => self::DEFAULT_YES,
            'fld_name' => 'Tak',
        ],
    ];

    public static function actives()
    {
        return self::$actives;
    }

    public static function sorts()
    {
        return self::$sorts;
    }

    public static function defaults()
    {
        return self::$defaults;
    }

    public static function tableName()
    {
        return 'tbl_content';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            ['fld_parent_id', 'exist', 'targetClass' => Content::class, 'targetAttribute' => 'fld_id', 'skipOnEmpty' => true],
            ['fld_active_id', 'in', 'range' => array_keys(self::actives())],
            ['fld_sort_id', 'in', 'range' => array_keys(self::sorts())],
            ['fld_default_id', 'in', 'range' => array_keys(self::defaults())],
            [['fld_system', 'fld_type', 'fld_group', 'fld_params', 'fld_uri', 'fld_icon', 'fld_name'], 'string'],
            [['fld_created_at', 'fld_updated_at'], 'date', 'format' => 'yyyy-MM-dd HH:mm:ss'],

            [['fld_name'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'fld_id' => 'Id',
            'fld_name' => 'Nazwa',
            'fld_parent_id' => 'Rodzic',
            'fld_system' => 'Nazwa systemowa',
            'fld_type' => 'Typ',
            'fld_group' => 'Grupa',
            'fld_params' => 'Parametry',
            'fld_sort_id' => 'Sortowanie odwrotne',
            'fld_active_id' => 'Status',
            'fld_default_id' => 'Domyślna',
            'fld_uri' => 'Adres URI',
            'fld_icon' => 'Ikonka',
        ];
    }

    public function getTranslatedContent()
    {
        return $this->hasMany(ContentLanguage::class, ['content_id' => 'fld_id']);
    }

    public function getParent()
    {
        return $this->hasOne(Content::class, ['fld_id' => 'fld_parent_id']);
    }

    public function getCreator()
    {
        return $this->hasOne(Account::class, ['fld_id' => 'fld_creator_id']);
    }

    public function getUpdater()
    {
        return $this->hasOne(Account::class, ['fld_id' => 'fld_updater_id']);
    }

    public function getProperty($own_id = null, $feature_id = null)
    {
        $properties = $this->getProperties($own_id, $feature_id);
        if ($properties) {
            return $properties[0];
        }

        return null;
    }

    public function getProperties($own_id = null, $feature_id = null)
    {
        $query = Property::find()->where(['fld_content_id' => $this->fld_id]);

        if ($own_id !== null) {
            $query->andWhere(['fld_own_id' => $own_id]);
        }

        if ($feature_id !== null) {
            $query->andWhere(['fld_feature_id' => $feature_id]);
        }

        return $query->orderBy('fld_sort')->all();
    }

    public function getValues($feature_id = null)
    {
        if ($feature_id === null) {
            return $this->hasMany(Value::class, ['fld_content_id' => 'fld_id']);
        }

        return Value::find()->where(['fld_content_id' => $this->fld_id, 'fld_feature_id' => $feature_id])->orderBy('fld_sort')->all();
    }

    public function getContents($conditions = [])
    {
        return $this
            ->hasMany(Content::class, ['fld_parent_id' => 'fld_id'])
            ->onCondition(['fld_active_id' => 'yes'])
            ->andWhere($conditions)
            ->orderBy('fld_sort' . ($this->fld_sort_id === self::SORT_DESC_YES ? ' DESC' : ''));
    }

    public static function getContent($system, $type = null)
    {
        if ($type === null) {
            return static::find()->where(['fld_system' => $system])->one();
        }

        return static::find()->where(['fld_system' => $system, 'fld_type' => $type])->one();
    }

    public static function getRootContent($system, $type = null)
    {
        return static::find()
            ->where(array_merge(
                ['fld_system' => $system, 'fld_parent_id' => null],
                $type !== null ? ['fld_type' => $type] : []
            ))
            ->one();
    }

    public function getChildContent($system, $type = null)
    {
        if ($type === null) {
            return static::find()->where(['fld_system' => $system, 'fld_parent_id' => $this->fld_id])->one();
        }

        return static::find()->where(['fld_system' => $system, 'fld_parent_id' => $this->fld_id, 'fld_type' => $type])->one();
    }

    private $_valuesByFeatureId;

    public function getValuesByFeatureId()
    {
        if ($this->_valuesByFeatureId === null) {
            $this->_valuesByFeatureId = [];

            foreach ($this->values as $value) {
                $this->_valuesByFeatureId[$value->fld_feature_id][] = $value;
            }
        }

        return $this->_valuesByFeatureId;
    }

    public function getValueByFeatureId($feature_id)
    {
        if (isset($this->valuesByFeatureId[$feature_id])) {
            if (count($this->valuesByFeatureId[$feature_id]) === 1) {
                return $this->valuesByFeatureId[$feature_id][0];
            }

            return $this->valuesByFeatureId[$feature_id];
        }

        return null;
    }

    private $_valuesValueByFeatureId;

    public function getValuesValueByFeatureId()
    {
        if ($this->_valuesValueByFeatureId === null) {
            $this->_valuesValueByFeatureId = [];

            foreach ($this->values as $value) {
                $this->_valuesValueByFeatureId[$value->fld_feature_id][] = $value->fld_value;
            }
        }

        return $this->_valuesValueByFeatureId;
    }

    public function getValueValueByFeatureId($feature_id)
    {
        if (isset($this->valuesValueByFeatureId[$feature_id])) {
            if (count($this->valuesValueByFeatureId[$feature_id]) === 1) {
                return $this->valuesValueByFeatureId[$feature_id][0];
            }

            return $this->valuesValueByFeatureId[$feature_id];
        }

        return null;
    }

    public function getRoot()
    {
        if ($this->fld_parent_id === null) {
            return $this;
        }

        return $this->parent->root;
    }

    public function getLevel()
    {
        if ($this->fld_parent_id === null) {
            return 0;
        }

        return $this->parent->level + 1;
    }

    public function getSubvalue($name, $feature_id)
    {
        if ($value = $this->getValueByFeatureId($feature_id)) {
            return $value->getSubvalue($name);
        }

        return null;
    }

    /**
     * @throws Exception
     */
    public function doActivate()
    {
        if (!Yii::$app->user->can('cms/content/activate', ['content' => $this])) {
            throw new Exception('Brak uprawnień');
        }

        $this->fld_active_id = self::ACTIVE_YES;
        $this->save();
    }

    /**
     * @throws Exception
     */
    public function doDeactivate()
    {
        if (!Yii::$app->user->can('cms/content/deactivate', ['content' => $this])) {
            throw new Exception('Brak uprawnień');
        }

        $this->fld_active_id = self::ACTIVE_NO;
        $this->save();
    }

    public function getFirst()
    {
        return static::find()->where(['fld_parent_id' => $this->fld_parent_id])->orderBy('fld_sort')->one();
    }

    public function getLast()
    {
        return static::find()->where(['fld_parent_id' => $this->fld_parent_id])->orderBy('fld_sort DESC')->one();
    }

    public function getPrev()
    {
        return static::find()
            ->where(['fld_parent_id' => $this->fld_parent_id])
            ->andWhere('fld_sort < ' . $this->fld_sort)
            ->orderBy('fld_sort DESC')->one();
    }

    public function getNext()
    {
        return static::find()
            ->where(['fld_parent_id' => $this->fld_parent_id])
            ->andWhere('fld_sort > ' . $this->fld_sort)
            ->orderBy('fld_sort')
            ->one();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (empty($this->fld_system) && !empty($this->fld_name)) {
                    $this->fld_system = Helper::safeAlias($this->fld_name);
                }

                $this->fld_creator_id = Yii::$app->user->id;
                $this->fld_created_at = date('Y-m-d H:i:s');

                $this->fld_sort = $this->getLast() ? $this->getLast()->fld_sort + 1 : 0;
            }

            if (preg_match('/[^a-z0-9=\s—–-]+/u', $this->fld_system)) {
                $this->fld_system = Helper::safeAlias($this->fld_name);
            }

            $this->fld_updater_id = Yii::$app->user->id;
            $this->fld_updated_at = date('Y-m-d H:i:s');

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (
            isset($changedAttributes['fld_default_id'])
            && $this->fld_default_id == self::DEFAULT_YES
            && $currentDefault = Content::find()
                ->andWhere([
                    'fld_default_id' => self::DEFAULT_YES,
                ])
                ->andWhere([
                    '!=',
                    'fld_id',
                    $this->fld_id,
                ])
                ->one()
        ) {
            $currentDefault->fld_default_id = self::DEFAULT_NO;
            $currentDefault->save();

            Yii::$app->session->remove('web');
        }

        Yii::$app->cache->flush();

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @throws Exception
     */
    public function doDown()
    {
        if (!Yii::$app->user->can('cms/content/update', ['content' => $this])) {
            throw new Exception('Brak uprawnień');
        }

        if ($next = $this->getNext()) {
            if (!Yii::$app->user->can('cms/content/update', ['content' => $next])) {
                throw new Exception('Brak uprawnień');
            }

            $sort = $this->fld_sort;

            $this->fld_sort = $next->fld_sort;
            $this->save();

            $next->fld_sort = $sort;
            $next->save();
        }
    }

    /**
     * @throws Exception
     */
    public function doUp()
    {
        if (!Yii::$app->user->can('cms/content/update', ['content' => $this])) {
            throw new Exception('Brak uprawnień');
        }

        if ($prev = $this->getPrev()) {
            if (!Yii::$app->user->can('cms/content/update', ['content' => $prev])) {
                throw new Exception('Brak uprawnień');
            }

            $sort = $this->fld_sort;

            $this->fld_sort = $prev->fld_sort;
            $this->save();

            $prev->fld_sort = $sort;
            $prev->save();
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->values as $value) {
                $value->delete();
            }

            foreach ($this->properties as $property) {
                $property->delete();
            }

            foreach ($this->contents as $content) {
                $content->delete();
            }

            return true;
        }

        return false;
    }

    public function getParams()
    {
        return Helper::isJson($this->fld_params) ? Json::decode($this->fld_params) : [];
    }

    public function getParam($key, $default = null)
    {
        if ($params = $this->getParams()) {
            return isset($params[$key]) ? $params[$key] : $default;
        }

        return $default;
    }

    /**
     * @param $values
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function saveProperties($values)
    {
        if ($this->fld_parent_id) {
            foreach ($this->parent->getProperties(Property::OWN_NO) as $property) {
                if (isset($values[$property->fld_feature_id])) {
                    $this->saveProperty($property, $values[$property->fld_feature_id]);
                } else {
                    $this->saveProperty($property, null);
                }
            }
        }

        foreach ($this->getProperties(Property::OWN_YES) as $property) {
            if (isset($values[$property->fld_feature_id])) {
                $this->saveProperty($property, $values[$property->fld_feature_id]);
            } else {
                $this->saveProperty($property, null);
            }
        }
    }

    /**
     * @param $property
     * @param $value
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function saveProperty($property, $value)
    {
        switch ($property->feature->fld_type_id) {
            case Feature::TYPE_CHECKBOXLIST:
                $this->saveCheckboxlistProperty($property, $value);
                break;

            case Feature::TYPE_IMAGESIMPLE:
                $this->saveImagesimpleProperty($property, $value);
                break;

            case Feature::TYPE_IMAGEADVANCED:
                $this->saveImageadvancedProperty($property, $value);
                break;

            case Feature::TYPE_IMAGEADVANCEDLIST:
                $this->saveImageadvancedlistProperty($property, $value);
                break;

            case Feature::TYPE_FILESIMPLE:
                $this->saveFilesimpleProperty($property, $value);
                break;

            case Feature::TYPE_FILEADVANCED:
                $this->saveFileadvancedProperty($property, $value);
                break;

            case Feature::TYPE_FILEADVANCEDLIST:
                $this->saveFileadvancedlistProperty($property, $value);
                break;

            default:
                $this->saveSimpleProperty($property, $value);
        }
    }

    /**
     * @param $property
     * @param $valueValue
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function saveSimpleProperty($property, $valueValue)
    {
        $translations = [];
        // checking if translations have been sent
        if (is_array($valueValue) && array_key_exists('pl', $valueValue)) {
            $translations = $valueValue;
            $valueValue = ArrayHelper::remove($translations, 'pl');
        }

        if ((is_string($valueValue) && $valueValue !== '') || is_array($valueValue)) {
            $value = Value::findOne(['fld_feature_id' => $property->fld_feature_id, 'fld_content_id' => $this->fld_id]);
            if ($value === null) {
                $value = new Value();
                $value->fld_feature_id = $property->fld_feature_id;
                $value->fld_content_id = $this->fld_id;
            }

            $value->fld_own_id = $property->fld_own_id;
            $value->fld_value = $valueValue;

            if ($value->save() && $translations) { // keep $translations last in if
                ValueLanguage::saveTranslations($translations, $value->fld_id);
            }
        } elseif ($value = Value::findOne(['fld_feature_id' => $property->fld_feature_id, 'fld_content_id' => $this->fld_id])) {
            $value->delete();
        }
    }

    public function saveCheckboxlistProperty($property, $valueValue)
    {
        foreach ($this->getPropertyItems($property) as $propertyItemKey => $propertyItem) {
            if (is_array($valueValue) && in_array($propertyItemKey, array_values($valueValue), false)) {
                if (!$value = Value::findOne(['fld_feature_id' => $property->fld_feature_id, 'fld_content_id' => $this->fld_id, 'fld_value' => $propertyItemKey])) {
                    $value = new Value();

                    $value->fld_feature_id = $property->fld_feature_id;
                    $value->fld_content_id = $this->fld_id;
                    $value->fld_own_id = $property->fld_own_id;

                    $value->fld_value = $propertyItemKey;
                    $value->save();
                }
            } elseif ($value = Value::findOne(['fld_feature_id' => $property->fld_feature_id, 'fld_content_id' => $this->fld_id, 'fld_value' => $propertyItemKey])) {
                $value->delete();
            }
        }
    }

    public function saveImagesimpleProperty($property, $valueValue)
    {
        if (!$value = Value::findOne(['fld_feature_id' => $property->fld_feature_id, 'fld_content_id' => $this->fld_id])) {
            $value = new Value();

            $value->fld_feature_id = $property->fld_feature_id;
            $value->fld_content_id = $this->fld_id;
        }

        $value->fld_own_id = $property->fld_own_id;

        if ($value->frm_image = UploadedFile::getInstanceByName('Feature[' . $property->fld_feature_id . '][file]')) {
            $valueValue['name'] = str_replace(' ', '-', trim($value->frm_image->baseName));
            $valueValue['baseName'] = str_replace(' ', '-', trim($value->frm_image->baseName));
            $valueValue['extension'] = $value->frm_image->extension;
            $valueValue['type'] = $value->frm_image->type;
            $valueValue['size'] = $value->frm_image->size;

            $value->fld_value = Json::encode($valueValue);
            $value->save();

            $value->saveImage();
        }
    }

    public function saveImageadvancedProperty($property, $valueValue)
    {
        if ((is_string($valueValue) && $valueValue !== '') || is_array($valueValue)) {
            if (!$value = Value::findOne(['fld_feature_id' => $property->fld_feature_id, 'fld_content_id' => $this->fld_id])) {
                $value = new Value();

                $value->fld_feature_id = $property->fld_feature_id;
                $value->fld_content_id = $this->fld_id;
            }

            $value->fld_own_id = $property->fld_own_id;
            $name = strtolower(trim($valueValue['name']));

            if ($value->frm_image = UploadedFile::getInstanceByName('Feature[' . $property->fld_feature_id . '][file]')) {
                if ($name === '') {
                    $name = strtolower(trim($value->frm_image->baseName));
                }

                $name = str_replace(' ', '-', $name);

                if ($postfix = $property->feature->getParam('postfix')) {
                    $name .= $postfix;
                }

                $valueValue = [
                    'name' => $name,
                    'alt' => $valueValue['alt'],
                ];

                $valueValue['name'] = $name;
                $valueValue['baseName'] = str_replace(' ', '-', trim($value->frm_image->baseName));
                $valueValue['extension'] = $value->frm_image->extension;
                $valueValue['type'] = $value->frm_image->type;
                $valueValue['size'] = $value->frm_image->size;

                $value->fld_value = Json::encode($valueValue);
                $value->save();

                $value->saveImage();
            } else {
                $lastValue = Helper::isJson($value->fld_value) ? Json::decode($value->fld_value) : [];
                $valueValue = array_merge($lastValue, $valueValue);

                if (isset($name, $valueValue['baseName'])) {
                    $value->fld_value = Json::encode($valueValue);
                    $value->save();

                    if (isset($lastValue['name']) && $lastValue['name'] !== $name) {
                        $value->moveImage($lastValue['name']);
                    }
                }
            }
        }
    }

    public function saveImageadvancedlistProperty($property, $valueValue)
    {
        if ((is_string($valueValue) && $valueValue !== '') || is_array($valueValue)) {
            $value = new Value();

            $value->fld_feature_id = $property->fld_feature_id;
            $value->fld_content_id = $this->fld_id;
            $value->fld_sort = $value->getLast() ? $value->getLast()->fld_sort + 1 : 0;
            $value->fld_own_id = $property->fld_own_id;

            $valueValue = [
                'name' => str_replace(' ', '-', trim($valueValue['name'])),
                'alt' => $valueValue['alt'],
            ];

            if ($value->frm_image = UploadedFile::getInstanceByName('Feature[' . $property->fld_feature_id . '][file]')) {
                $valueValue['name'] = $valueValue['name'] ?: str_replace(' ', '-', trim($value->frm_image->baseName));
                $valueValue['baseName'] = str_replace(' ', '-', trim($value->frm_image->baseName));
                $valueValue['extension'] = $value->frm_image->extension;
                $valueValue['type'] = $value->frm_image->type;
                $valueValue['size'] = $value->frm_image->size;

                $value->fld_value = Json::encode($valueValue);
                $value->save();

                $value->saveImage();
            } else {
                $lastValue = Helper::isJson($value->fld_value) ? Json::decode($value->fld_value) : [];
                $valueValue = array_merge($lastValue, $valueValue);

                if ($valueValue['name']) {
                    $value->fld_value = Json::encode($valueValue);
                    $value->save();

                    if ($lastValue['name'] !== $valueValue['name']) {
                        $value->moveImage($lastValue['name']);
                    }
                }
            }
        }
    }

    public function saveFilesimpleProperty($property, $valueValue)
    {
        if (!$value = Value::findOne(['fld_feature_id' => $property->fld_feature_id, 'fld_content_id' => $this->fld_id])) {
            $value = new Value();

            $value->fld_feature_id = $property->fld_feature_id;
            $value->fld_content_id = $this->fld_id;
        }

        $value->fld_own_id = $property->fld_own_id;

        if ($value->frm_file = UploadedFile::getInstanceByName('Feature[' . $property->fld_feature_id . '][file]')) {
            $valueValue['name'] = str_replace(' ', '-', trim($value->frm_file->baseName));
            $valueValue['baseName'] = str_replace(' ', '-', trim($value->frm_file->baseName));
            $valueValue['extension'] = $value->frm_file->extension;
            $valueValue['type'] = $value->frm_file->type;
            $valueValue['size'] = $value->frm_file->size;

            $value->fld_value = Json::encode($valueValue);
            $value->save();

            $value->saveFile();
        }
    }

    public function saveFileadvancedProperty($property, $valueValue)
    {
        if ((is_string($valueValue) && $valueValue !== '') || is_array($valueValue)) {
            if (!$value = Value::findOne(['fld_feature_id' => $property->fld_feature_id, 'fld_content_id' => $this->fld_id])) {
                $value = new Value();

                $value->fld_feature_id = $property->fld_feature_id;
                $value->fld_content_id = $this->fld_id;
            }

            $value->fld_own_id = $property->fld_own_id;
            $name = strtolower(trim($valueValue['name']));

            if ($value->frm_file = UploadedFile::getInstanceByName('Feature[' . $property->fld_feature_id . '][file]')) {
                if ($name === '') {
                    $name = strtolower(trim($value->frm_file->baseName));
                }

                $name = Helper::safeAlias($name);

                $valueValue = [
                    'name' => $name,
                    'title' => $valueValue['title'],
                ];

                $valueValue['name'] = $name;
                $valueValue['baseName'] = str_replace(' ', '-', trim($value->frm_file->baseName));
                $valueValue['extension'] = $value->frm_file->extension;
                $valueValue['type'] = $value->frm_file->type;
                $valueValue['size'] = $value->frm_file->size;

                $value->fld_value = Json::encode($valueValue);
                $value->save();

                $value->saveFile();
            } else {
                $lastValue = Helper::isJson($value->fld_value) ? Json::decode($value->fld_value) : [];
                $valueValue = array_merge($lastValue, $valueValue);

                if (isset($name, $valueValue['baseName'])) {
                    $value->fld_value = Json::encode($valueValue);
                    $value->save();

                    if (isset($lastValue['name']) && $lastValue['name'] !== $name) {
                        $value->moveFile($lastValue['name']);
                    }
                }
            }
        }
    }

    public function saveFileadvancedlistProperty($property, $valueValue)
    {
        if ((is_string($valueValue) && $valueValue !== '') || is_array($valueValue)) {
            $value = new Value();

            $value->fld_feature_id = $property->fld_feature_id;
            $value->fld_content_id = $this->fld_id;
            $value->fld_sort = $value->getLast() ? $value->getLast()->fld_sort + 1 : 0;
            $value->fld_own_id = $property->fld_own_id;

            $valueValue = [
                'name' => str_replace(' ', '-', trim($valueValue['name'])),
                'title' => $valueValue['title'],
            ];

            if ($value->frm_file = UploadedFile::getInstanceByName('Feature[' . $property->fld_feature_id . '][file]')) {
                $valueValue['name'] = $valueValue['name'] ?: str_replace(' ', '-', trim($value->frm_file->baseName));
                $valueValue['baseName'] = str_replace(' ', '-', trim($value->frm_file->baseName));
                $valueValue['extension'] = $value->frm_file->extension;
                $valueValue['type'] = $value->frm_file->type;
                $valueValue['size'] = $value->frm_file->size;

                $value->fld_value = Json::encode($valueValue);
                $value->save();

                $value->saveFile();
            } else {
                $lastValue = Helper::isJson($value->fld_value) ? Json::decode($value->fld_value) : [];
                $valueValue = array_merge($lastValue, $valueValue);

                if ($valueValue['name']) {
                    $value->fld_value = Json::encode($valueValue);
                    $value->save();

                    if ($lastValue['name'] !== $valueValue['name']) {
                        $value->moveFile($lastValue['name']);
                    }
                }
            }
        }
    }

    public function getPropertyItems($property)
    {
        $items = [];
        if ($featureItems = $property->feature->getParam('items', [])) {
            $items = $featureItems;
        } elseif (($contentName = $property->feature->getParam('content'))
            && $content = static::getContent($contentName, $property->feature->getParam('type'))
        ) {
            foreach ($content->getContents()->all() as $item) {
                $ok = true;

                if ($conditions = $property->feature->getParam('conditions')) {
                    foreach ($conditions as $condition) {
                        if ($condition['type'] === 'feature'
                            && !($item->getValueByFeatureId($condition['feature'])
                                && $item->getValueByFeatureId($condition['feature'])->fld_value == $condition['value'])
                        ) {
                            $ok = false;
                        }
                    }
                }

                if ($ok && $value = $this->getValueByFeatureId($property->fld_feature_id)) {
                    if (is_array($value)) {
                        $valueOk = true;

                        foreach ($value as $valueItem) {
                            if ($item->fld_id == $valueItem->fld_value) {
                                $valueOk = false;
                            }
                        }

                        if ($valueOk) {
                            foreach ($property->getParam('showOnly', []) as $key => $valueItem) {
                                if ($item->getValueByFeatureId($key)->fld_value != $valueItem) {
                                    $ok = false;
                                }
                            }
                        }
                    } elseif ($item->fld_id != $value->fld_value) {
                        foreach ($property->getParam('showOnly', []) as $key => $value) {
                            if ($item->getValueByFeatureId($key)->fld_value != $value) {
                                $ok = false;
                            }
                        }
                    }
                }

                if ($ok) {
                    $items[$item->fld_id] = $item->fld_name;
                }
            }
        }

        return $items;
    }

    private $_inheritedValues = [];

    /**
     * @param string $feature_id
     * @param null $key
     * @return mixed|null
     */
    public function getInheritedValue($feature_id, $key = null)
    {
        if (!array_key_exists($feature_id, $this->_inheritedValues)) {
            $this->_inheritedValues[$feature_id] = [];
        }

        if (!array_key_exists('object', $this->_inheritedValues[$feature_id])) {
            $this->_inheritedValues[$feature_id]['object'] = Value::findOne([
                'fld_content_id' => $this->fld_id,
                'fld_feature_id' => $feature_id,
                'fld_own_id' => Value::OWN_NO,
            ]);
        }

        if (!array_key_exists('values', $this->_inheritedValues[$feature_id])) {
            $this->_inheritedValues[$feature_id]['values'] = [];
        }

        $keyIndex = $key ?? '--NULL--';
        if (!array_key_exists($keyIndex, $this->_inheritedValues[$feature_id]['values'])) {
            $this->_inheritedValues[$feature_id]['values'][$keyIndex] = null;

            if ($this->_inheritedValues[$feature_id]['object'] !== null) {
                $this->_inheritedValues[$feature_id]['values'][$keyIndex] = $this->_inheritedValues[$feature_id]['object']->getValue($key);
            }
        }

        return $this->_inheritedValues[$feature_id]['values'][$keyIndex];
    }

    private $_ownValues = [];

    /**
     * @param string $feature_id
     * @param null $key
     * @return Value|mixed|null
     */
    public function getOwnValue($feature_id, $key = null)
    {
        if (!array_key_exists($feature_id, $this->_ownValues)) {
            $this->_ownValues[$feature_id] = [];
        }

        if (!array_key_exists('object', $this->_ownValues[$feature_id])) {
            $this->_ownValues[$feature_id]['object'] = Value::findOne([
                'fld_content_id' => $this->fld_id,
                'fld_feature_id' => $feature_id,
                'fld_own_id' => Value::OWN_YES,
            ]);
        }

        if (!array_key_exists('values', $this->_ownValues[$feature_id])) {
            $this->_ownValues[$feature_id]['values'] = [];
        }

        $keyIndex = $key ?? '--NULL--';
        if (!array_key_exists($keyIndex, $this->_ownValues[$feature_id]['values'])) {
            $this->_ownValues[$feature_id]['values'][$keyIndex] = null;

            if ($this->_ownValues[$feature_id]['object'] !== null) {
                $this->_ownValues[$feature_id]['values'][$keyIndex] = $this->_ownValues[$feature_id]['object']->getValue($key);
            }
        }

        return $this->_ownValues[$feature_id]['values'][$keyIndex];
    }

    /**
     * @param string $feature_id
     * @param null $thumbnailName
     * @return string
     */
    public function getImagePathValue($feature_id, $thumbnailName = null)
    {
        return $this->getImagePathInheritedValue($feature_id, $thumbnailName);
    }

    /**
     * @param string $feature_id
     * @param null $thumbnailName
     * @return string|null
     */
    public function getImagePathInheritedValue($feature_id, $thumbnailName = null)
    {
        if (($inheritedValue = $this->getInheritedValue($feature_id)) && isset($inheritedValue['name']) && $inheritedValue['name'] !== '') {
            if ($thumbnailName && ($feature = Feature::findOne($feature_id)) && ($restrictions = $feature->getParams())) {
                foreach ($restrictions['thumbnails'] as $thumbnail) {
                    if ($thumbnail['name'] === $thumbnailName) {
                        $restriction = $thumbnail;
                    }
                }

                if (isset($thumbnail)) {
                    return '/' . implode('/', [Yii::$app->params['imagesPublicPath'], $inheritedValue['name']]) . $thumbnail['postfix'] . '.' . $thumbnail['format'];
                }
            } else {
                return '/' . implode('/', [Yii::$app->params['imagesPublicPath'], $inheritedValue['name']]) . '.' . $inheritedValue['extension'];
            }
        }

        return null;
    }

    /**
     * @param string $feature_id
     * @param null $thumbnailName
     * @return string
     */
    public function getImagePathOwnValue($feature_id, $thumbnailName = null)
    {
        if (($ownValue = $this->getOwnValue($feature_id)) && isset($ownValue['name']) && $ownValue['name'] !== '') {
            if ($thumbnailName && ($feature = Feature::findOne($feature_id)) && ($restrictions = $feature->getParams())) {
                return '/' . implode('/', [Yii::$app->params['imagesPublicPath'], $ownValue['name']]) . $feature->getParams()['thumbnails'][$thumbnailName]['postfix'] . '.' . $feature->getParams()['format'];
            }

            return '/' . implode('/', [Yii::$app->params['imagesPublicPath'], $ownValue['name']]) . '.' . $ownValue['extension'];
        }

        return null;
    }

    /**
     * @param string $feature_id
     * @return string
     */
    public function getFilePathValue($feature_id)
    {
        return $this->getFilePathInheritedValue($feature_id);
    }

    /**
     * @param string $feature_id
     * @return string
     */
    public function getFilePathInheritedValue($feature_id)
    {
        return '/' . implode('/', [Yii::$app->params['filesPublicPath'], $this->getInheritedValue($feature_id, 'name')]) . '.' . $this->getInheritedValue($feature_id, 'extension');
    }

    /**
     * @param string $feature_id
     * @return string
     */
    public function getFilePathOwnValue($feature_id)
    {
        return '/' . implode('/', [Yii::$app->params['filesPublicPath'], $this->getOwnValue($feature_id, 'name')]) . '.' . $this->getOwnValue($feature_id, 'extension');
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->fld_default_id == self::DEFAULT_YES
            ? ''
            : (
                $this->fld_uri
                    ? $this->fld_uri
                    : $this->fld_system
            );
    }

    /**
     * @param $feature_id
     * @return Content|null
     */
    public function getPointedContent($feature_id): ?Content
    {
        $value = $this->getValueByFeatureId($feature_id);

        if ($value) {
            return static::findOne($value);
        }

        return null;
    }
}
