<?php

namespace datait\fractal\models;

use Yii;
use yii\db\ActiveRecord;

class Log extends ActiveRecord {
	const TYPE_ACTION = 1;

	const KIND_WORKER = 1;
	const KIND_CUSTOMER = 2;
	const KIND_PROJECT = 3;
	const KIND_TASK = 4;
	const KIND_SCHEMA = 5;
	const KIND_INVOICE = 6;

	public static function tableName() {
		return 'tbl_log';
	}

	public function rules() {
		return [
			['fld_creator_id', 'exist', 'targetClass' => '\app\models\Account', 'targetAttribute' => 'fld_id', 'skipOnEmpty' => true],
			[['fld_type_id', 'fld_action_id', 'fld_kind_id', 'fld_object_id'], 'number'],
			[['fld_action_name', 'fld_action_color', 'fld_comment'], 'string'],

			[['fld_type_id', 'fld_action_id'], 'required'],
		];
	}

	public function attributeLabels() {
		return [
			'fld_action_name' => 'Nazwa akcji',
			'fld_type_id' => 'Typ',
			'fld_object_id' => 'Obiekt',
			'fld_created_at' => 'Data i czas',
			'fld_creator_id' => 'Utworzył',
		];
	}

	public function getCreator() {
		return $this->hasOne(Account::className(), ['fld_id' => 'fld_creator_id']);
	}

	public static function types() {
		return [
			self::TYPE_ACTION => ['fld_id' => self::TYPE_ACTION, 'fld_name' => 'Akcja', 'fld_color' => 'green'],
		];
	}

	public function getType() {
		return self::types()[$this->fld_type_id];
	}

	public static function kinds() {
		return [
			self::KIND_WORKER => ['fld_id' => self::KIND_WORKER, 'fld_name' => 'Pracownik', 'fld_view' => '/worker/view'],
			self::KIND_CUSTOMER => ['fld_id' => self::KIND_CUSTOMER, 'fld_name' => 'Klient', 'fld_view' => '/customer/view'],
			self::KIND_PROJECT => ['fld_id' => self::KIND_PROJECT, 'fld_name' => 'Projekt', 'fld_view' => '/project/view'],
			self::KIND_TASK => ['fld_id' => self::KIND_TASK, 'fld_name' => 'Praca', 'fld_view' => '/task/view'],
			self::KIND_SCHEMA => ['fld_id' => self::KIND_SCHEMA, 'fld_name' => 'Schemat rozliczeń', 'fld_view' => '/schema/view'],
		];
	}

	public static function kind($kind_id) {
		return self::kinds()[$kind_id];
	}

	public function getKind() {
		return self::kind($this->fld_kind_id);
	}

	public static function saveAction($kind_id, $action, $object_id, $comment = '') {
		$log = new Log;

		$log->fld_creator_id = Yii::$app->user->id;
		$log->fld_type_id = self::TYPE_ACTION;
		$log->fld_kind_id = $kind_id;
		$log->fld_action_id = $action['fld_id'];
		$log->fld_action_name = $action['fld_name'];
		$log->fld_action_color = $action['fld_color'];
		$log->fld_object_id = $object_id;
		$log->fld_comment = $comment;

		if (!$log->save()) {
			Yii::trace('Błąd zapisu loga do bazy danych ' . var_export($log, true));
		}
	}
}
