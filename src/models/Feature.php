<?php

namespace datait\fractal\models;

use yii\db\ActiveRecord;

/**
 * Klasa cech
 *
 * @author Marcin Korytkowski
 **/
class Feature extends ActiveRecord {
	const TYPE_HEADER = 'header';
	const TYPE_TEXTBOX = 'textbox';
	const TYPE_TEXTAREA = 'textarea';
	const TYPE_HTML = 'html';
	const TYPE_CHECKBOX = 'checkbox';
	const TYPE_CHECKBOXLIST = 'checkboxlist';
	const TYPE_RADIOBUTTONLIST = 'radiobuttonlist';
	const TYPE_DROPDOWNLIST = 'dropdownlist';
	const TYPE_FILESIMPLE = 'filesimple';
	const TYPE_FILEADVANCED = 'fileadvanced';
	const TYPE_FILESIMPLELIST = 'filesimplelist';
	const TYPE_FILEADVANCEDLIST = 'fileadvancedlist';
	const TYPE_IMAGESIMPLE = 'imagesimple';
	const TYPE_IMAGEADVANCED = 'imageadvanced';
	const TYPE_IMAGESIMPLELIST = 'imagesimplelist';
	const TYPE_IMAGEADVANCEDLIST = 'imageadvancedlist';
	const TYPE_SPECIAL = 'special';

	public static $types = [
		Feature::TYPE_HEADER => ['fld_id' => Feature::TYPE_HEADER, 'fld_name' => 'Nagłówek'],
		Feature::TYPE_TEXTBOX => ['fld_id' => Feature::TYPE_TEXTBOX, 'fld_name' => 'Pole tekstowe'],
		Feature::TYPE_TEXTAREA => ['fld_id' => Feature::TYPE_TEXTAREA, 'fld_name' => 'Obszar tekstu'],
		Feature::TYPE_HTML => ['fld_id' => Feature::TYPE_HTML, 'fld_name' => 'Obszar HTML'],
		Feature::TYPE_CHECKBOX => ['fld_id' => Feature::TYPE_CHECKBOX, 'fld_name' => 'Checkbox'],
		Feature::TYPE_CHECKBOXLIST => ['fld_id' => Feature::TYPE_CHECKBOXLIST, 'fld_name' => 'Lista checkboxów'],
		Feature::TYPE_RADIOBUTTONLIST => ['fld_id' => Feature::TYPE_RADIOBUTTONLIST, 'fld_name' => 'Lista ratiobuttonów'],
		Feature::TYPE_DROPDOWNLIST => ['fld_id' => Feature::TYPE_DROPDOWNLIST, 'fld_name' => 'Lista dropdown'],
		Feature::TYPE_FILESIMPLE => ['fld_id' => Feature::TYPE_FILESIMPLE, 'fld_name' => 'Plik'],
		Feature::TYPE_FILEADVANCED => ['fld_id' => Feature::TYPE_FILEADVANCED, 'fld_name' => 'Plik (zaawansowany)'],
		Feature::TYPE_FILESIMPLELIST => ['fld_id' => Feature::TYPE_FILESIMPLELIST, 'fld_name' => 'Lista plików'],
		Feature::TYPE_FILEADVANCEDLIST => ['fld_id' => Feature::TYPE_FILEADVANCEDLIST, 'fld_name' => 'Lista plików (zaawansowana)'],
		Feature::TYPE_IMAGESIMPLE => ['fld_id' => Feature::TYPE_IMAGESIMPLE, 'fld_name' => 'Obrazek'],
		Feature::TYPE_IMAGEADVANCED => ['fld_id' => Feature::TYPE_IMAGEADVANCED, 'fld_name' => 'Obrazek (zaawansowany)'],
		Feature::TYPE_IMAGESIMPLELIST => ['fld_id' => Feature::TYPE_IMAGESIMPLELIST, 'fld_name' => 'Lista obrazków'],
		Feature::TYPE_IMAGEADVANCEDLIST => ['fld_id' => Feature::TYPE_IMAGEADVANCEDLIST, 'fld_name' => 'Lista obrazków (zaawansowana)'],
		Feature::TYPE_SPECIAL => ['fld_id' => Feature::TYPE_SPECIAL, 'fld_name' => 'Pole specjalne'],
	];

	public static function types($id = null) {
		return is_null($id) ? self::$types : Feature::$types[$id];
	}

	public static function tableName() {
		return 'tbl_feature';
	}

	public function rules() {
		return [
			['fld_id', 'match', 'pattern' => '/^[a-z]+\-*/'],
			['fld_type_id', 'in', 'range' => array_keys(Feature::$types)],
			[['fld_name', 'fld_restriction'], 'string'],

			[['fld_id', 'fld_name', 'fld_type_id'], 'required'],
		];
	}

	public function attributeLabels() {
		return [
			'fld_id' => 'Nazwa systemowa',
			'fld_name' => 'Nazwa',
			'fld_type_id' => 'Typ',
			'fld_restriction' => 'Ograniczenia i parametry',
		];
	}

	public function getProperties() {
		return $this->hasMany(Property::className(), ['fld_feature_id' => 'fld_id']);
	}

	public function getType() {
		return Feature::types()[$this->fld_type_id];
	}

	public function getTypesDropDown() {
		$result = [];
		foreach (Feature::types() as $type) {
			$result[$type['fld_id']] = $type['fld_name'];
		}

		return $result;
	}

	public function getParams() {
		return Helper::isJson($this->fld_restriction) ? json_decode($this->fld_restriction, true) : [];
	}

	public function getParam($key, $default = null) {
		if ($params = $this->getParams()) {
			return isset($params[$key]) ? $params[$key] : $default;
		} else {
			return $default;
		}
	}
}