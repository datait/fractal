<?php

namespace datait\fractal\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use function array_keys;
use function date;
use function implode;

/**
 * Class Worker
 * @package app\models
 *
 * @property int fld_id
 * @property int fld_type_id
 * @property int fld_status_id
 * @property string fld_last_name
 * @property string fld_first_name
 * @property string fld_email
 * @property string fld_mobile
 * @property string fld_description
 * @property int fld_created_at
 * @property int fld_creator_id
 * @property int fld_updated_at
 * @property int fld_updater_id
 * @property int fld_deleted_at
 * @property int fld_deleter_id
 */

class Worker extends ActiveRecord
{
    const ACTION_CREATE = 1;
    const ACTION_UPDATE = 2;
    const ACTION_STATUS = 3;

    const TYPE_EDITOR = 'editor';
    const TYPE_PARTNER = 'partner';
    const TYPE_WEBMASTER = 'webmaster';

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @var array
     */
    private static $types = [
        self::TYPE_EDITOR => [
            'fld_id' => self::TYPE_EDITOR,
            'fld_name' => 'Edytor',
            'fld_class' => 'default',
            'role' => 'editor',
        ],
        self::TYPE_PARTNER => [
            'fld_id' => self::TYPE_PARTNER,
            'fld_name' => 'Administrator',
            'fld_class' => 'primary',
            'role' => 'partner',
        ],
        self::TYPE_WEBMASTER => [
            'fld_id' => self::TYPE_WEBMASTER,
            'fld_name' => 'Webmaster',
            'fld_class' => 'master',
            'role' => 'webmaster',
        ],
    ];

    /**
     * @var array
     */
    private static $statuses = [
        self::STATUS_INACTIVE => [
            'fld_name' => 'Nieaktywny',
            'fld_class' => 'danger',
        ],
        self::STATUS_ACTIVE => [
            'fld_name' => 'Aktywny',
            'fld_class' => 'success',
        ],
    ];

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'tbl_worker';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['fld_last_name', 'fld_first_name', 'fld_email'], 'string'],
            ['fld_type_id', 'in', 'range' => array_keys(static::$types)],
            ['fld_status_id', 'in', 'range' => array_keys(static::$statuses)],
            ['fld_email', 'email'],
            ['fld_mobile', 'string'],
            ['fld_description', 'string'],
            [['fld_last_name', 'fld_type_id', 'fld_email'], 'required'],
            ['fld_status_id', 'default', 'value' => self::STATUS_ACTIVE],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'fld_id' => 'Id',
            'fld_name' => 'Nazwa',
            'fld_last_name' => 'Nazwisko',
            'fld_first_name' => 'Imię',
            'fld_full_name' => 'Nazwisko i imię',
            'fld_email' => 'E-mail',
            'fld_mobile' => 'Telefon komórkowy',
            'fld_status_id' => 'Status',
            'fld_description' => 'Opis',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAccount(): ActiveQuery
    {
        return $this->hasOne(
            Account::class,
            [
                'fld_model_id' => 'fld_id',
            ]
        )
        ->onCondition([
            'fld_class_id' => 'cmsUser',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCreator(): ActiveQuery
    {
        return $this->hasOne(
            Account::class,
            [
                'fld_id' => 'fld_creator_id',
            ]
        );
    }

    /**
     * @return ActiveQuery
     */
    public function getUpdater(): ActiveQuery
    {
        return $this->hasOne(
            Account::class,
            [
                'fld_id' => 'fld_updater_id',
            ]
        );
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return static::$types;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return self::$types[$this->fld_type_id];
    }

    /**
     * @return array
     */
    public static function getTypesButtonDropdown(): array
    {
        $result = [];

        foreach (static::$types as $type) {
            $worker = new self;
            $worker->fld_type_id = $type['fld_id'];

            if (Yii::$app->user->can('cms/worker/create', ['worker' => $worker])) {
                $result[] = [
                    'label' => $type['fld_name'],
                    'url' => ['/cms/worker/create', 'type_id' => $type['fld_id']]
                ];
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return static::$statuses;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return static::$statuses[$this->fld_status_id];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return implode(
            ' ',
            [
                $this->fld_last_name,
                $this->fld_first_name,
            ]);
    }

    /**
     * @return array
     */
    public static function actions(): array
    {
        return [
            self::ACTION_CREATE => [
                'fld_id' => self::ACTION_CREATE,
                'fld_name' => 'Utworzenie pracownika',
                'fld_color' => '#00a2f9',
            ],
            self::ACTION_UPDATE => [
                'fld_id' => self::ACTION_UPDATE,
                'fld_name' => 'Edytowanie pracownika',
                'fld_color' => '#00a2f9',
            ],
            self::ACTION_STATUS => [
                'fld_id' => self::ACTION_STATUS,
                'fld_name' => 'Zmiana statusu pracownika',
                'fld_color' => '#00a2f9',
            ],
        ];
    }

    /**
     * @param $action_id
     * @return array
     */
    public static function action($action_id): array
    {
        return static::actions()[$action_id];
    }

    /**
     * @return array
     */
    public function getAction(): array
    {
        return static::action($this->fld_action_id);
    }

    public function activate()
    {
        if ((int)$this->fld_status_id !== self::STATUS_ACTIVE) {
            $this->fld_status_id = self::STATUS_ACTIVE;

            if ($this->save() && $account = Account::findOne([
                'fld_model_id' => $this->fld_id,
                'fld_class_id' => 'cmsUser',
            ])) {
                $account->setStatus(self::STATUS_ACTIVE);
            }
        }
    }

    public function deactivate()
    {
        if ((int)$this->fld_status_id !== self::STATUS_INACTIVE) {
            $this->fld_status_id = self::STATUS_INACTIVE;

            if ($this->save() && $account = Account::findOne([
                'fld_worker_id' => $this->fld_id,
                'fld_class_id' => 'cmsUser',
            ])) {
                $account->setStatus(self::STATUS_INACTIVE);
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->fld_creator_id = Yii::$app->user->id;
            } else {
                $this->fld_updater_id = Yii::$app->user->id;
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function beforeDelete(): bool
    {
        if (parent::beforeDelete()) {
            $this->fld_deleted_at = date('Y-m-d H:i:s');
            $this->fld_deleter_id = Yii::$app->user->id;

            return true;
        }

        return false;
    }
}
