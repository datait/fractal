<?php

namespace datait\fractal\models;

use yii\base\Model;
use yii\helpers\Inflector;

/**
 * Klasa pomocnika
 *
 * @author Marcin Korytkowski
 **/
class Helper extends Model {
	/**
	 * Zwraca informację czy podany $string jest poprawnym ciągiem w formacie JSON
	 *
	 * @return boolean
	 * @author Marcin Korytkowski
	 **/
	public static function isJson($string)
    {
		return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string, true)))));
	}

	/**
	 * Zwraca string wyczyszczony z polskich znaków, spacji (zamienia na -) oraz zmienia litery na małe
	 *
	 * @return string
	 * @author Marcin Korytkowski
	 **/
	public static function safeAlias($string)
    {
        return Inflector::slug($string);
	}

    /**
     * @param $bytes
     * @param int $precision
     * @return string
     */
    public static function sizeFormat($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}
