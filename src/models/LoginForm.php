<?php

namespace datait\fractal\models;

use Yii;
use yii\base\Model;

class LoginForm extends Model {
	public $username;
	public $password;
	public $rememberMe = true;

	private $_user = false;

	public function rules()	{
		return [
			[['username', 'password'], 'required'],
			['rememberMe', 'boolean'],
			['password', 'validatePassword'],
		];
	}

	public function attributeLabels() {
		return [
			'username' => 'Login',
			'password' => 'Hasło',
		];
	}

	public function validatePassword($attribute, $params) {
		if (!$this->hasErrors()) {
			$user = $this->getUser();

			if (!$user || !$user->validatePassword($this->password)) {
				$this->addError($attribute, 'Incorrect username or password.');
			}
		}
	}

	public function login() {
		if ($this->validate()) {
			if ($login = Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0)) {

				$auth = Yii::$app->authManager;
				if (isset(Yii::$app->user->identity->fld_worker_id)) {
					$role = $auth->getRole('worker');
				} elseif (isset(Yii::$app->user->identity->fld_customer_id)) {
					$role = $auth->getRole('customer');
				}

				if (!Yii::$app->authManager->getAssignment($role->name, Yii::$app->user->id)) {
					Yii::$app->authManager->assign($role, Yii::$app->user->id);
				}

				return true;
			}
		} else {
			return false;
		}
	}

	public function getUser() {
		if ($this->_user === false) {
			$this->_user = Account::findByUsername($this->username);
		}

		return $this->_user;
	}
}
