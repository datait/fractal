<?php

namespace datait\fractal\models;

use Imagick;
use ImagickPixel;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\Json;

/**
 * Klasa wartości cech zawartości
 * @author Marcin Korytkowski
 *
 * @property int $fld_id [int(11) unsigned]
 * @property string $fld_feature_id [varchar(255)]
 * @property int $fld_content_id [int(11) unsigned]
 * @property string $fld_own_id [varchar(255)]
 * @property string $fld_value
 * @property int $fld_sort [int(11) unsigned]
 */
class Value extends ActiveRecord
{
    const OWN_NO = 'no';
    const OWN_YES = 'yes';

    public $frm_image;
    public $frm_file;

    public static function tableName()
    {
        return 'tbl_value';
    }

    public function rules()
    {
        return [
            [['fld_feature_id'], 'exist', 'targetClass' => Feature::class, 'targetAttribute' => 'fld_id'],
            [['fld_content_id'], 'exist', 'targetClass' => Content::class, 'targetAttribute' => 'fld_id'],
            [['fld_own_id'], 'in', 'range' => array_keys(Property::owns())],
            ['fld_value', 'safe'],

            [['fld_feature_id', 'fld_content_id', 'fld_own_id'], 'required'],
        ];
    }

    /**
     * @param $lang
     * @return array|ActiveRecord|null
     */
    public function getTranslatedValue($lang)
    {
        return $this->hasOne(ValueLanguage::class, ['value_id' => 'fld_id'])->where(['lang' => $lang])->one();
    }

    public function getFeature()
    {
        return $this->hasOne(Feature::class, ['fld_id' => 'fld_feature_id']);
    }

    public function getContent()
    {
        return $this->hasOne(Content::class, ['fld_id' => 'fld_content_id']);
    }

    public function saveImage()
    {
        if (!Yii::$app->user->can('cms/content/update', ['content' => $this->content])) {
            throw new Exception('Brak uprawnień');
        }

        $valueValue = json_decode($this->fld_value, true);

        $basePath = implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['filesPath'], $this->fld_id]) . '.' . $valueValue['extension'];
        $this->frm_image->saveAs($basePath);

        $this->generateImage();
    }

    public function generateImage()
    {
        $valueValue = json_decode($this->fld_value, true);
        if (!$basePath = implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['filesPath'], $this->fld_id]) . '.' . $valueValue['extension']) {
            $basePath = implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['filesPath'], $this->fld_id]) . '.png';
        }

        $format = $this->feature->getParam('format');
        $resolution = $this->feature->getParam('resolution');
        $compression = $this->feature->getParam('compression', Yii::$app->params['jpgCompression']);

        if ($format || $resolution) {
            $image = new Imagick($basePath);

            $transform = $this->feature->getParam('transform');

            if ($format) {
                $image->setImageFormat($format);
            }

            if ($background = $this->feature->getParam('background', 'white')) {
                if ($background == 'transparent') {
                    $image->setBackgroundColor(new ImagickPixel('transparent'));
                    Yii::trace('Tło przezroczyste');
                    $image->setImageFormat('png32');
                } else {
                    $image->setBackgroundColor($background);
                }
            }

            $image->setImageCompression(Imagick::COMPRESSION_JPEG);
            $image->setImageCompressionQuality($compression);

            if ($resolution) {
                switch ($transform) {
                    case 'fill':
                        $imageOrg = new Imagick($basePath);

                        $geometry = $imageOrg->getImageGeometry();
                        $widthOrg = $geometry['width'];
                        $heightOrg = $geometry['height'];

                        $borderWidth = ($resolution['width'] - $widthOrg) / 2;
                        $borderHeight = ($resolution['height'] - $heightOrg) / 2;

                        $image->borderImage($background, $borderWidth, $borderHeight);

                        break;

                    case 'resize':
                        $imageOrg = new Imagick($basePath);

                        $geometry = $imageOrg->getImageGeometry();
                        $widthOrg = $geometry['width'];
                        $heightOrg = $geometry['height'];

                        if (isset($resolution['width']) && $widthNew = $resolution['width']) {
                            $scale = $widthNew / $widthOrg;
                            $heightNew = $heightOrg * $scale;
                        } elseif (isset($resolution['height']) && $heightNew = $resolution['height']) {
                            $scale = $heightNew / $heightOrg;
                            $widthNew = $widthOrg * $scale;
                        } else {
                            $widthNew = $widthOrg;
                            $heightNew = $heightOrg;
                        }

                        $image->thumbnailImage($widthNew, $heightNew, true, true);

                        break;

                    case 'contain':
                        $image->setBackgroundColor($background);
                        $image->thumbnailImage($resolution['width'], $resolution['height'], true, true);

                        break;

                    case 'cover':
                        $image->cropThumbnailImage($resolution['width'], $resolution['height']);

                        break;
                }
            }

            $copyPath = implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['imagesPublicPath'], $valueValue['name']]) . $this->feature->getParam('postfix', '') . '.' . ($format ? $format : $valueValue['extension']);

            $image->writeImage($copyPath);

            if ($thumbnails = $this->feature->getParam('thumbnails')) {
                foreach ($thumbnails as $thumbnail) {
                    $image = new Imagick($basePath);

                    $format = isset($thumbnail['format']) ? $thumbnail['format'] : $this->feature->getParam('format', 'jpg');

                    $image->setImageFormat($format);
                    $compression = isset($thumbnail['compression']) ? $thumbnail['compression'] : $this->feature->getParam('compression', Yii::$app->params['jpgCompression']);

                    if ($background = $thumbnail['background']) {
                        if ($background == 'transparent') {
                            $image->setBackgroundColor(new ImagickPixel('transparent'));
                            $image->setImageFormat('png32');
                        } else {
                            $image->setBackgroundColor($background);
                        }
                    }

                    $image->setImageCompression(Imagick::COMPRESSION_JPEG);
                    $image->setImageCompressionQuality($compression);

                    switch ($thumbnail['transform']) {
                        case 'resize':
                            $imageOrg = new Imagick($basePath);

                            $geometry = $imageOrg->getImageGeometry();
                            $widthOrg = $geometry['width'];
                            $heightOrg = $geometry['height'];

                            if (isset($thumbnail['resolution']['width']) && $widthNew = $thumbnail['resolution']['width']) {
                                $scale = $widthNew / $widthOrg;
                                $heightNew = $heightOrg * $scale;
                            } elseif (isset($thumbnail['resolution']['height']) && $heightNew = $thumbnail['resolution']['height']) {
                                $scale = $heightNew / $heightOrg;
                                $widthNew = $widthOrg * $scale;
                            } else {
                                $widthNew = $widthOrg;
                                $heightNew = $heightOrg;
                            }

                            $image->thumbnailImage($widthNew, $heightNew, true, true);

                            break;

                        case 'contain':
                            $image->thumbnailImage($thumbnail['resolution']['width'], $thumbnail['resolution']['height'], true, true);

                            break;

                        case 'cover':
                            $image->cropThumbnailImage($thumbnail['resolution']['width'], $thumbnail['resolution']['height']);

                            break;
                    }

                    $copyPath = implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['imagesPublicPath'], $valueValue['baseName']]) . (isset($thumbnail['postfix']) ? $thumbnail['postfix'] : '') . '.' . ($format ? $format : $valueValue['extension']);

                    $image->writeImage($copyPath);
                }
            }
        } else {
            $copyPath = implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['imagesPublicPath'], $valueValue['name']]) . '.' . $valueValue['extension'];

            copy($basePath, $copyPath);
        }
    }

    public function saveFile()
    {
        if (!Yii::$app->user->can('cms/content/update', ['content' => $this->content])) {
            throw new Exception('Brak uprawnień');
        }

        $valueValue = json_decode($this->fld_value, true);

        $basePath = implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['filesPath'], $this->fld_id]) . '.' . $valueValue['extension'];
        $this->frm_file->saveAs($basePath);

        $copyPath = implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['filesPublicPath'], $valueValue['name']]) . '.' . $valueValue['extension'];

        copy($basePath, $copyPath);
    }

    public function moveImage($lastName)
    {
        if (!Yii::$app->user->can('cms/content/update', ['content' => $this->content])) {
            throw new Exception('Brak uprawnień');
        }

        $valueValue = json_decode($this->fld_value, true);

        if ($lastPath = realpath(implode('/', [$_SERVER['DOCUMENT_ROOT'], Yii::$app->params['imagesPublicPath'], $lastName]) . '.' . $valueValue['extension'])) {
            unlink($lastPath);
        }

        if ($sourcePath = realpath(implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['filesPath'], $this->fld_id]) . '.' . $valueValue['extension'])) {
            $destPath = implode('/', [$_SERVER['DOCUMENT_ROOT'], Yii::$app->params['imagesPublicPath'], $valueValue['name']]) . '.' . $valueValue['extension'];

            copy($sourcePath, $destPath);
        }
    }

    public function moveFile($lastName)
    {
        if (!Yii::$app->user->can('cms/content/update', ['content' => $this->content])) {
            throw new Exception('Brak uprawnień');
        }

        $valueValue = json_decode($this->fld_value, true);

        if ($lastPath = realpath(implode('/', [$_SERVER['DOCUMENT_ROOT'], Yii::$app->params['filesPublicPath'], $lastName]) . '.' . $valueValue['extension'])) {
            unlink($lastPath);
        }

        if ($sourcePath = realpath(implode('/', [$_SERVER["DOCUMENT_ROOT"], Yii::$app->params['filesPath'], $this->fld_id]) . '.' . $valueValue['extension'])) {
            $destPath = implode('/', [$_SERVER['DOCUMENT_ROOT'], Yii::$app->params['filesPublicPath'], $valueValue['name']]) . '.' . $valueValue['extension'];

            copy($sourcePath, $destPath);
        }
    }

    public function getSubvalue($name)
    {
        Yii::trace('Value: ' . var_export($this, true));
        $subvalue = json_decode($this['fld_value'], true);

        Yii::trace('Decoded: ' . var_export($subvalue, true));
        Yii::trace('Decoded[' . $name . ']: ' . $subvalue[$name]);

        return isset($subvalue[$name]) ? $subvalue[$name] : null;
    }

    public function getFirst()
    {
        return self::find()->where(['fld_content_id' => $this->fld_content_id, 'fld_feature_id' => $this->fld_feature_id])->orderBy('fld_sort')->one();
    }

    public function getLast()
    {
        return self::find()->where(['fld_content_id' => $this->fld_content_id, 'fld_feature_id' => $this->fld_feature_id])->orderBy('fld_sort DESC')->one();
    }

    public function getPrev()
    {
        return self::find()->where(['fld_content_id' => $this->fld_content_id, 'fld_feature_id' => $this->fld_feature_id])->andWhere('fld_sort < ' . $this->fld_sort)->orderBy('fld_sort DESC')->one();
    }

    public function getNext()
    {
        return self::find()->where(['fld_content_id' => $this->fld_content_id, 'fld_feature_id' => $this->fld_feature_id])->andWhere('fld_sort > ' . $this->fld_sort)->orderBy('fld_sort')->one();
    }

    public function doDown()
    {
        if (!Yii::$app->user->can('cms/content/update', ['content' => $this->content])) {
            throw new Exception('Brak uprawnień');
        }

        if ($next = $this->getNext()) {
            if (!Yii::$app->user->can('cms/content/update', ['content' => $next->content])) {
                throw new Exception('Brak uprawnień');
            }

            $sort = $this->fld_sort;

            $this->fld_sort = $next->fld_sort;
            $this->save();

            $next->fld_sort = $sort;
            $next->save();
        }
    }

    public function doUp()
    {
        if (!Yii::$app->user->can('cms/content/update', ['content' => $this->content])) {
            throw new Exception('Brak uprawnień');
        }

        if ($prev = $this->getPrev()) {
            if (!Yii::$app->user->can('cms/content/update', ['content' => $prev->content])) {
                throw new Exception('Brak uprawnień');
            }

            $sort = $this->fld_sort;

            $this->fld_sort = $prev->fld_sort;
            $this->save();

            $prev->fld_sort = $sort;
            $prev->save();
        }
    }

    public static function doSort($ids)
    {
        $index = 0;
        foreach ($ids as $id) {
            $model = self::findOne($id);
            $model->fld_sort = $index;
            $model->save();

            $index++;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (in_array($this->feature->fld_type_id, [Feature::TYPE_IMAGEADVANCED, Feature::TYPE_IMAGEADVANCEDLIST])) {
                if (!Yii::$app->user->can('cms/value/delete', ['value' => $this])) {
                    throw new Exception('Brak uprawnień');
                }

                $valueValue = json_decode($this->fld_value, true);

                if (isset($valueValue['baseName'])) {
                    if ($destPath = realpath(implode('/', [$_SERVER['DOCUMENT_ROOT'], Yii::$app->params['filesPath'], $this->fld_id]) . '.' . $valueValue['extension'])) {
                        unlink($destPath);
                    }

                    if ($copyPath = realpath(implode('/', [$_SERVER['DOCUMENT_ROOT'], Yii::$app->params['imagesPublicPath'], $valueValue['name']]) . '.' . $valueValue['extension'])) {
                        unlink($copyPath);
                    }

                    if ($thumbnails = $this->feature->getParam('thumbnails')) {
                        foreach ($thumbnails as $thumbnail) {
                            if ($thumbnailPath = realpath(implode('/', [$_SERVER['DOCUMENT_ROOT'], Yii::$app->params['imagesPublicPath'], $valueValue['name']]) . $thumbnail['postfix'] . '.' . $thumbnail['format'])) {
                                unlink($thumbnailPath);
                            }
                        }
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        //$this->serveParams($changedAttributes);
    }

    public function getProperty()
    {
        return Property::findOne(['fld_content_id' => $this->fld_content_id, 'fld_feature_id' => $this->fld_feature_id]);
    }

    public function serveParams($changedAttributes)
    {
        if ($property = $this->getProperty()) {
            foreach ($this->property->getParams() as $key => $value) {
                $this->serveParam($key, $value, $changedAttributes);
            }
        }
    }

    public function serveParam($param, $params, $changedAttributes)
    {
        switch ($param) {
            case 'afterSelect':
                if ($this->content->getValueByFeatureId($this->fld_feature_id)) {
                    foreach ($params as $key => $value) {
                        $this->executeFunction($key, $value);
                    }
                }

                break;

            case 'afterUnselect':
                if ($value = $this->getValueByFeatureId($property->fld_feature_id)) {
                    $oldValueValue = isset($changedAttributes[$property->fld_feature_id]) ? $changedAttributes[$property->fld_feature_id] : $value;

                    if ($value->fld_value != $oldValueValue) {
                        foreach ($params as $key => $value) {
                            $this->executeFunction($property, $key, $value);
                        }
                    }
                }

                break;
        }
    }

    public function executeFunction($property, $function, $params)
    {
        switch ($function) {
            case 'setValue':
                if ($value = $this->getValueByFeatureId($property->fld_feature_id)) {
                    $changedContent = Content::findOne($value->fld_value);

                    foreach ($params as $key => $param) {
                        Func::setValue($changedContent, $property, $key, $param);
                    }
                }

                break;
        }
    }

    public function regenerateImage()
    {
        $this->generateImage();
    }

    public function compress()
    {
        // Do zrobienia

        //10's digit:
//
//        0 or omitted: Use Z_HUFFMAN_ONLY strategy with the
//           zlib default compression level
//
//        1-9: the zlib compression level
//
//     1's digit:
//
//        0-4: the PNG filter method
//
//        5:   libpng adaptive filtering if compression level > 5
//             libpng filter type "none" if compression level <= 5
//or if image is grayscale or palette
//
//        6:   libpng adaptive filtering
//
//        7:   "LOCO" filtering (intrapixel differing) if writing
//a MNG, otherwise "none".  Did not work in IM-6.7.0-9
//and earlier because of a missing "else".
//
//8:   Z_RLE strategy (or Z_HUFFMAN_ONLY if quality < 10), adaptive
//             filtering. Unused prior to IM-6.7.0-10, was same as 6
//
//        9:   Z_RLE strategy (or Z_HUFFMAN_ONLY if quality < 10), no PNG filters
//             Unused prior to IM-6.7.0-10, was same as 6

        $imagick = new Imagick("./Biter_500.jpg");

        $imagick->setImageFormat('png');

        $imagick->writeimage("./output/original.png");
        compressAllTypes($imagick, "./output/FullColor");


        function compressAllTypes(Imagick $imagick, $filename)
        {
            for ($compression = 0; $compression <= 9; $compression++) {
                echo "Compression $compression \n";
                for ($filter = 0; $filter <= 9; $filter++) {
                    echo "Filter $filter";
                    $output = clone $imagick;
                    $output->setImageFormat('png');
                    //$output->setOption('png:format', 'png8');
                    $compressionType = intval($compression . $filter);
                    $output->setImageCompressionQuality($compressionType);
                    $outputName = $filename . "$compression$filter.png";
                    $output->writeImage($outputName);
                }
                echo "\n";
            }
        }
    }

    public function getImagePathValue($thumbnailName = null)
    {
        /*if (YII_ENV === 'dev') {
            return null;
        }*/

        $imageName = '';

        if ($thumbnailName === null) {
            $imageName = $this->getValue('name') . '.' . $this->getValue('extension');
        } elseif ($thumbnails = $this->feature->getParam('thumbnails')) {
            foreach ($thumbnails as $thumbnail) {
                if (isset($thumbnail['name']) && $thumbnail['name'] === $thumbnailName) {
                    $imageName = $this->getValue('baseName')
                        . ($thumbnail['postfix'] ?? '') . '.'
                        . ($thumbnail['extension'] ?? $this->getValue('extension'));
                }
            }
        }

        return $this->getValue('name') !== ''
            ? '/' . implode('/', [Yii::$app->params['imagesPublicPath'], $imageName])
            : '';
    }

    public function getFilePathValue()
    {
        return $this->getValue('name') !== ''
            ? '/' . implode('/', [Yii::$app->params['filesPublicPath'], $this->getValue('name')]) . '.' . $this->getValue('extension')
            : '';
    }

    /**
     * @param $processedValue
     * @param null $key
     * @return mixed|null
     */
    private function _getValue($processedValue, $key = null)
    {
        if (Helper::isJson($processedValue)) {
            $value = Json::decode($processedValue);

            if ($key === null) {
                return $value;
            }

            return $value[$key] ?? null;
        }

        return $processedValue;
    }

    private $translatedValues = [];

    /**
     * Returns value's value translated to selected language with PL fallback.
     * @param null $key
     * @return mixed|null
     */
    public function getValue($key = null)
    {
        $defaultName = $this->fld_value;

        if (Yii::$app->language === 'pl') {
            return $this->_getValue($defaultName, $key);
        }

        if (array_key_exists(Yii::$app->language, $this->translatedValues)
            && array_key_exists($this->fld_id, $this->translatedValues[Yii::$app->language])
        ) {
            return $this->_getValue($this->translatedValues[Yii::$app->language][$this->fld_id], $key);
        }

        $translatedValue = $this->getTranslatedValue(Yii::$app->language);
        if ($translatedValue === null) {
            return $this->_getValue($defaultName, $key);
        }

        $this->translatedValues[Yii::$app->language][$this->fld_id] = $translatedValue->value;

        return $this->_getValue($translatedValue->value, $key);
    }
}