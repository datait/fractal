<?php

namespace datait\fractal\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use function array_keys;

/**
 * Class Account
 * @package app\models
 *
 * @property int fld_id
 * @property int fld_status_id
 * @property string fld_role_id
 * @property string|null fld_key_id
 * @property string|null fld_class_id
 * @property int fld_model_id
 * @property string fld_login
 * @property string fld_password
 * @property string fld_email
 * @property string fld_reset_datetime
 * @property string fld_reset_password
 * @property int fld_created_at
 * @property int fld_creator_id
 * @property int fld_updated_at
 * @property int fld_updater_id
 * @property int fld_deleted_at
 * @property int fld_deleter_id
 *
 * @property string|null classId
 * @property int|null modelId
 * @property ActiveRecord|null ident
 * @property string roleId
 * @property array|string role
 * @property string groupId
 * @property Worker|null worker
 * @property string|null keyId
 */
class Account extends ActiveRecord implements IdentityInterface
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    private static $statuses = [
        self::STATUS_INACTIVE => ['name' => 'Nieaktywny', 'color' => 'firebrick'],
        self::STATUS_ACTIVE => ['name' => 'Aktywny', 'color' => 'green'],
    ];

    const ROLE_CMS_USER = 'cmsUser';

    public static $roles = [
        self::ROLE_CMS_USER => [
            'id' => self::ROLE_CMS_USER,
            'name' => 'Użytkownik CMS',
        ],
    ];

    public $authKey;
    public $new_pass;
    public $repeat_pass;

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'tbl_account';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['fld_email'], 'string'],
            ['fld_status_id', 'in', 'range' => array_keys(self::$statuses)],
            ['fld_email', 'email'],
            ['fld_password', 'string'],

            [['new_pass', 'repeat_pass'], 'required', 'on' => 'pass_change'],
            ['new_pass', 'string', 'min' => 6, 'on' => 'pass_change'],
            ['repeat_pass', 'compare', 'compareAttribute' => 'new_pass', 'on' => 'pass_change'],

            [['fld_login', 'fld_password'], 'filter', 'filter' => 'trim'],
            ['fld_login', 'match', 'pattern' => '/^[0-9a-zA-Z@._+]{1,255}$/'],

            ['fld_key_id', 'string'],

            ['fld_login', 'required'],

            ['fld_status_id', 'default', 'value' => self::STATUS_ACTIVE],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'fld_id' => 'Id',
            'fld_email' => 'E-mail',
            'fld_login' => 'Login',
            'fld_password' => 'Hasło',
            'fld_status_id' => 'Status',
            'new_pass' => 'Nowe hasło',
            'repeat_pass' => 'Powtórz nowe hasło',
        ];
    }

    /**
     * @return string
     */
    public function getRoleId(): string
    {
        return $this->fld_role_id;
    }

    /**
     * @param string|null $roleId
     * @param string|null $key
     * @return array|string
     */
    public function getRole(string $roleId = null, string $key = null)
    {
        return is_null($roleId)
            ? self::$roles
            : (
            is_null($key)
                ? self::$roles[$roleId]
                : self::$roles[$roleId][$key]
            );
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->fld_group_id;
    }

    /**
     * @return string|null
     */
    public function getClassId(): ?string
    {
        return $this->fld_class_id;
    }

    /**
     * @return int|null
     */
    public function getModelId(): ?int
    {
        return $this->fld_model_id;
    }

    /**
     * @return ActiveRecord|null
     */
    public function getIdent(): ?ActiveRecord
    {
        if ($this->classId) {
            $class = Yii::$app->cms->classMap[$this->classId];
            $modelId = $this->modelId;

            return $class::findOne($modelId);
        }

        return null;
    }

    /**
     * @return ActiveQuery
     */
    public function getCreator(): ActiveQuery
    {
        $identityClass = Yii::$app->user->identityClass;

        return $this->hasOne(
            $identityClass,
            [
                'fld_id' => 'fld_creator_id',
            ]
        );
    }

    /**
     * @return ActiveQuery
     */
    public function getUpdater(): ActiveQuery
    {
        $identityClass = Yii::$app->user->identityClass;

        return $this->hasOne(
            $identityClass,
            [
                'fld_id' => 'fld_updater_id',
            ]
        );
    }

    /**
     * @param int|string $id
     * @return IdentityInterface|null
     */
    public static function findIdentity($id): ?IdentityInterface
    {
        return static::findOne([
            'fld_id' => $id,
            'fld_status_id' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @param $login
     * @return IdentityInterface|null
     */
    public static function findByUsername($login): ?IdentityInterface
    {
        return static::findOne([
            'fld_login' => $login,
            'fld_status_id' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return IdentityInterface|null
     */
    public static function findIdentityByAccessToken($token, $type = null): ?IdentityInterface
    {
        return static::findOne([
            'fld_key_id' => $token,
        ]);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->fld_id;
    }

    /**
     * @return string|null
     */
    public function getAuthKey(): ?string
    {
        return $this->authKey;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->authKey === $authKey;
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password): bool
    {
        return Yii::$app->security->validatePassword($password, $this->fld_password);
    }

    /**
     * @param $status_id
     */
    public function setStatus($status_id)
    {
        $this->fld_status_id = $status_id;
        $this->save();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert): bool
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        try {
            $this->fld_password = Yii::$app->security->generatePasswordHash($this->fld_password);
        } catch (Exception $e) {
            throw new RuntimeException('Błąd w hashowaniu hasła.');
        }

        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        if ($this->ident) {
            return $this->ident->name;
        }
    }

    /**
     * @return string|null
     */
    public function getKeyId(): ?string
    {
        return $this->fld_key_id;
    }

    /**
     * @param $roles
     * @return bool
     */
    public function isInRole($roles): bool
    {
        return is_string($roles)
            ? $this->roleId == $roles
            : is_array($roles)
                && in_array(
                    $this->roleId,
                    $roles
                );
    }
}
