<?php

namespace datait\fractal\models;

use yii\db\ActiveRecord;

class Search extends ActiveRecord {
	public static function tableName() {
		return 'tbl_search';
	}

	public function rules() {
		return [
			[['fld_account_id'], 'integer'],
			[['fld_view', 'fld_value'], 'string'],
		];
	}

	public static function saveFilters($params, $view, $filterTable) {
		foreach ($params as $paramKey => $param) {
			if (in_array($paramKey, $filterTable)) {
				if ($search = Search::findOne(['fld_account_id' => \Yii::$app->user->id, 'fld_view' => $view, 'fld_name' => $paramKey])) {
					if ($param == 'null') {
						$search->delete();
					} else {
						$search->fld_name = $paramKey;
						$search->fld_value = $param;

						$search->save();
					}
				} elseif ($param != 'null') {
					$search = new Search;
					
					$search->fld_name = $paramKey;
					$search->fld_value = $param;
					$search->fld_view = $view;
					$search->fld_account_id = Yii::$app->user->id;

					$search->save();
				}
			}
		}
	}

	public static function loadFilters($view) {
		return Search::findAll(['fld_account_id' => \Yii::$app->user->id, 'fld_view' => $view]);
	}
}
