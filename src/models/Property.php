<?php

namespace datait\fractal\models;

use yii\db\ActiveRecord;

/**
 * Klasa właściwości (cechy przypisane do contentu)
 *
 * @author Marcin Korytkowski
 **/
class Property extends ActiveRecord {
	const OWN_NO = 'no';
	const OWN_YES = 'yes';

	const PUBLIC_NO = 'no';
	const PUBLIC_YES = 'yes';

	public static $owns = [
		self::OWN_NO => ['fld_id' => self::OWN_NO, 'fld_name' => 'Dziedziczona', 'fld_class' => 'default'],
		self::OWN_YES => ['fld_id' => self::OWN_YES, 'fld_name' => 'Własna', 'fld_class' => 'primary'],
	];

	public static function owns() {
		return self::$owns;
	}

	public static $publics = [
		self::PUBLIC_NO => ['fld_id' => self::PUBLIC_NO, 'fld_name' => 'Ukryta', 'fld_class' => 'warning'],
		self::PUBLIC_YES => ['fld_id' => self::PUBLIC_YES, 'fld_name' => 'Publiczna', 'fld_class' => 'success'],
	];

	public static function publics() {
		return self::$publics;
	}

	public static function tableName() {
		return 'tbl_property';
	}

	public function rules() {
		return [
			['fld_feature_id', 'exist', 'targetClass' => '\app\models\Feature', 'targetAttribute' => 'fld_id'],
			['fld_content_id', 'exist', 'targetClass' => '\app\models\Content', 'targetAttribute' => 'fld_id'],
			['fld_own_id', 'in', 'range' => array_keys(self::owns())],
			['fld_public_id', 'in', 'range' => array_keys(self::publics())],
			['fld_params', 'string'],

			[['fld_feature_id', 'fld_content_id'], 'required'],
		];
	}

	public function attributeLabels() {
		return [
			'fld_id' => 'Id',
			'fld_content_id' => 'Zawartość',
			'fld_feature_id' => 'Cecha',
			'fld_own_id' => 'Własna',
			'fld_public_id' => 'Publiczna',
			'fld_params' => 'Parametry',
		];
	}

	public function getFeature() {
		return $this->hasOne(Feature::className(), ['fld_id' => 'fld_feature_id']);
	}

	public function getContent() {
		return $this->hasOne(Content::className(), ['fld_id' => 'fld_content_id']);
	}

	public function getOwn() {
		return self::owns()[$this->fld_own_id];
	}

	public function getPublic() {
		return self::publics()[$this->fld_public_id];
	}

	public function getFeaturesDropDown() {
		$result = [];
		foreach (\app\models\Feature::find()->orderBy('fld_name')->all() as $feature) {
			if (!Property::find()->where(['fld_content_id' => $this->fld_content_id, 'fld_feature_id' => $feature->fld_id, 'fld_own_id' => $this->fld_own_id])->one() || $feature->fld_id == $this->fld_feature_id) {
				$result[$feature->fld_id] = $feature->fld_name . ' (' . $feature->fld_id . ')';
			}
		}

		return $result;
	}

	public function getLast() {
		return self::find()->where(['fld_content_id' => $this->fld_content_id])->orderBy('fld_sort DESC')->one();
	}

	public function getPrev() {
		return self::find()->where(['fld_content_id' => $this->fld_content_id])->andWhere('fld_sort < ' . $this->fld_sort)->orderBy('fld_sort DESC')->one();
	}

	public function getNext() {
		return self::find()->where(['fld_content_id' => $this->fld_content_id])->andWhere('fld_sort > ' . $this->fld_sort)->orderBy('fld_sort')->one();
	}

	public function beforeSave($insert) {
		if (parent::beforeSave($insert)) {
			if ($this->isNewRecord) {
				$this->fld_sort = $this->getLast() ? $this->getLast()->fld_sort + 1 : 0;
			}

			return true;
		} else {
			return false;
		}
	}

	public function doDown($next) {
        $sort = $this->fld_sort;

        $this->fld_sort = $next->fld_sort;
        $this->save();

        $next->fld_sort = $sort;
        $next->save();
	}

	public function doUp($prev) {
        $sort = $this->fld_sort;

        $this->fld_sort = $prev->fld_sort;
        $this->save();

        $prev->fld_sort = $sort;
        $prev->save();
	}

	public function getParams() {
		return Helper::isJson($this->fld_params) ? json_decode($this->fld_params, true) : [];
	}

	public function getParam($key, $default = null) {
		if ($params = $this->getParams()) {
			return isset($params[$key]) ? $params[$key] : $default;
		} else {
			return $default;
		}
	}

	public function beforeDelete() {
		if (parent::beforeDelete()) {
			if ($this->fld_own_id == self::OWN_YES) {
				if ($value = Value::find()->where(['fld_content_id' => $this->fld_content_id, 'fld_feature_id' => $this->fld_feature_id])->one()) {
					$value->delete();
				}
			} else {
				foreach ($this->content->contents as $content) {
					foreach (Value::find()->where(['fld_content_id' => $content->fld_id, 'fld_feature_id' => $this->fld_feature_id])->all() as $value) {
						$value->delete();
					}
				}
			}

			return true;
		} else {
			return false;
		}
	}
}