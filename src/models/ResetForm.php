<?php

namespace datait\fractal\models;

use yii\base\Model;

class ResetForm extends Model
{
    public $username;

    private $_user = false;

    public function rules()
    {
        return [
            ['username', 'string'],
            ['username', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Login',
        ];
    }

    /**
     * TODO: not working at all, wtf
     * @return bool
     */
    public function reset()
    {
        if ($user = $this->getUser()) {
            if ($user->fld_reset_datetime === null
                || $user->fld_reset_datetime < date('Y-m-d H:i:s', strtotime('-10 minute'))) {
                $password = $this->generatePassword();

                $this->fld_reset_password = $password;
                $this->fld_reset_datetime = date("Y-m-d H:i:s");
                unset($this->fld_password);

                if ($this->save()) {
                    $this->sendEmail(
                        "Nowe hasło w systemie",
                        'resetPassword', [
                            'password' => $password,
                            'login' => $this->fld_login,
                            'email' => $this->fld_email,
                        ]
                    );
                }

                return true;
            }
        }

        return false;
    }

    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Account::findByUsername($this->username);
        }

        return $this->_user;
    }
}
