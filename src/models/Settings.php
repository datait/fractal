<?php

namespace datait\fractal\models;

use yii\db\ActiveRecord;

class Settings extends ActiveRecord {
	const STATUS_OFF = 0;
	const STATUS_ON = 1;
	const STATUS_MAINTENANCE = 2;

	private static $statuses = [
		self::STATUS_OFF => ['fld_id' => self::STATUS_OFF, 'fld_name' => 'Wyłączony', 'fld_color' => 'red', 'fld_class' => 'danger'],
		self::STATUS_ON => ['fld_id' => self::STATUS_ON, 'fld_name' => 'Włączony', 'fld_color' => 'green', 'fld_class' => 'success'],
		self::STATUS_MAINTENANCE => ['fld_id' => self::STATUS_MAINTENANCE, 'fld_name' => 'Maintenance', 'fld_color' => 'orange', 'fld_class' => 'warning'],
	];

	public static function tableName() {
		return 'tbl_settings';
	}

	public function rules() {
		return [
			[['fld_name', 'fld_domain'], 'string'],
			['fld_status_id', 'in', 'range' => array_keys(self::$statuses)],
			['fld_lang_id', 'in', 'range' => array_keys(\app\models\Lang::langs()), 'skipOnEmpty' => true],
			['fld_email', 'email'],

			[['fld_name', 'fld_url'], 'required'],

			['fld_status_id', 'default', 'value' => self::STATUS_ON],
		];
	}

	public function attributeLabels() {
		return [
			'fld_name' => 'Nazwa',
			'fld_domain' => 'Domena',
			'fld_email' => 'E-mail',
			'fld_lang_id' => 'Język',
			'fld_status_id' => 'Status',
		];
	}
}
