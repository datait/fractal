<?php

namespace datait\fractal\models;

use yii\base\Model;

/**
 * Klasa funkcji
 *
 * @author Marcin Korytkowski
 **/
class Func extends Model {
	public static function setValue($content, $property, $key, $param) {
		if (!$value = $content->getValueByFeatureId($key)) {
			$value = new Value;
			$value->fld_feature_id = $key;
			$value->fld_content_id = $content->fld_id;
			$value->fld_own_id = $property->fld_own_id;
		}

		$value->fld_value = $param;
		$value->save();
	}
}