<?php

namespace datait\fractal\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use function array_key_exists;
use function trim;

/**
 * Class ContentLanguage
 * @package app\models
 *
 * @property int $id
 * @property int $content_id
 * @property string $lang
 * @property string $name
 * @property string $system
 * @property string $uri
 * @property int $created_at
 * @property int $updated_at
 */
class ContentLanguage extends ActiveRecord
{
    /**
     * {@inheritDoc}
     */
    public static function tableName()
    {
        return 'tbl_content_language';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['lang', 'content_id'], 'required'],
            [['lang'], 'string', 'max' => 10],
            [['name', 'system', 'uri'], 'string', 'max' => 255],
            [['content_id'], 'exist', 'targetClass' => Content::class, 'targetAttribute' => 'fld_id'],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class
        ];
    }

    /**
     * @param array $translations
     * @param array $post
     * @param int $contentId
     */
    public static function saveTranslations($translations, $post, $contentId)
    {
        if (array_key_exists('lang', $post)) {
            $postLanguages = $post['lang'];

            foreach (['name', 'system', 'uri'] as $type) {
                if (array_key_exists($type, $postLanguages)) {
                    foreach ($postLanguages[$type] as $postLanguage => $newValue) {
                        $value = trim($newValue);

                        // update all existing translations that has been changed
                        if (array_key_exists($postLanguage, $translations)
                            && $value !== $translations[$postLanguage]->$type
                        ) {
                            $translations[$postLanguage]->$type = $value;

                            if (!$translations[$postLanguage]->save()) {
                                Yii::error([
                                    'Can not update translation for Content',
                                    'contentId' => $translations[$postLanguage]->content_id,
                                    'contentLanguageId' => $translations[$postLanguage]->id,
                                    'translation' => $value,
                                    'field' => $type,
                                ]);
                            }
                        }

                        // add all new translations that has been added
                        if ($value !== ''
                            && (!array_key_exists($postLanguage, $translations)
                                || $translations[$postLanguage]->$type === null)
                        ) {
                            $translation = static::findOne([
                                'lang' => $postLanguage,
                                'content_id' => $contentId,
                            ]);
                            if ($translation === null) {
                                $translation = new self;
                                $translation->lang = $postLanguage;
                                $translation->content_id = $contentId;
                            }

                            $translation->$type = $value;

                            if ($type === 'name' && empty($translation->system)) {
                                $translation->system = Helper::safeAlias($translation->name);
                            }

                            if ($type === 'system' && preg_match('/[^a-z0-9=\s—–-]+/u', $translation->system)) {
                                $translation->system = Helper::safeAlias($translation->name);
                            }

                            if (!$translation->save()) {
                                Yii::error([
                                    'Can not add translation for Content',
                                    'contentId' => $contentId,
                                    'translation' => $value,
                                    'field' => $type,
                                ]);
                            }
                        }
                    }
                }
            }
        }
    }
}
