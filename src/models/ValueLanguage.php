<?php

namespace datait\fractal\models;

use Throwable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use function in_array;
use function trim;

/**
 * Class ValueLanguage
 * @package app\models
 *
 * @property int $id
 * @property int $value_id
 * @property string $lang
 * @property string $value
 * @property int $created_at
 * @property int $updated_at
 */
class ValueLanguage extends ActiveRecord
{
    /**
     * {@inheritDoc}
     */
    public static function tableName()
    {
        return 'tbl_value_language';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['value', 'lang', 'value_id'], 'required'],
            [['lang'], 'string', 'max' => 10],
            [['value'], 'string'],
            [['value_id'], 'exist', 'targetClass' => Value::class, 'targetAttribute' => 'fld_id'],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class
        ];
    }

    /**
     * @param array $translations
     * @param int $valueId
     * @throws Throwable
     * @throws StaleObjectException
     */
    public static function saveTranslations($translations, $valueId)
    {
        $availableLanguages = !empty(Yii::$app->params['additionalLanguages']) ? Yii::$app->params['additionalLanguages'] : [];

        if ($availableLanguages) {
            foreach ($translations as $language => $newValue) {
                if (in_array($language, $availableLanguages, true)) {
                    $value = trim($newValue);

                    $transValue = static::findOne([
                        'lang' => $language,
                        'value_id' => $valueId,
                    ]);

                    if ($value === '') {
                        // remove existing translation that has been cleared
                        if ($transValue !== null && !$transValue->delete()) {
                            Yii::error([
                                'Can not remove translation for Value',
                                'valueId' => $valueId,
                                'valueLanguageId' => $transValue->id,
                                'translation' => $value,
                            ]);
                        }
                    } elseif ($transValue !== null && $transValue->value !== $value) {
                        // update existing translation that has been changed
                        $transValue->value = $value;

                        if (!$transValue->save()) {
                            Yii::error([
                                'Can not update translation for Value',
                                'valueId' => $valueId,
                                'valueLanguageId' => $transValue->id,
                                'translation' => $value,
                            ]);
                        }
                    } elseif ($transValue === null) {
                        // add new translation that has been added
                        $translation = new self;
                        $translation->lang = $language;
                        $translation->value = $value;
                        $translation->value_id = $valueId;

                        if (!$translation->save()) {
                            Yii::error([
                                'Can not add translation for Value',
                                'valueId' => $valueId,
                                'translation' => $value,
                            ]);
                        }
                    }
                }
            }
        }
    }
}
