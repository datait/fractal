<?php

namespace datait\fractal\models;

use Yii;
use yii\bootstrap\Html;
use yii\bootstrap\InputWidget;
use function implode;
use function strtoupper;
use function trim;

/**
 * Class LanguageVersion
 * @package app\models
 */
class LanguageVersion extends InputWidget
{
    /**
     * @var array
     */
    public $translations = [];

    /**
     * @var string
     */
    public $type = 'text';

    /**
     * @var Value
     */
    public $valueModel;

    private $_languages;

    public function init()
    {
        parent::init();

        if (!array_key_exists('class', $this->options)) {
            $this->options['class'] = '';
        }
        if (!array_key_exists('style', $this->options)) {
            $this->options['style'] = '';
        }
        $this->options['style'] = trim($this->options['style']);
        if ($this->options['style'] !== '' && substr($this->options['style'], -1, 1) !== ';') {
            $this->options['style'] .= ';';
        }

        $this->options['class'] .= ' form-control lang-version';

        $this->_languages = !empty(Yii::$app->params['additionalLanguages']) ? Yii::$app->params['additionalLanguages'] : [];
    }

    public function run()
    {
        $this->view->registerJs(<<<JS
$('.switch-language').on('click', function(e) {
    e.preventDefault();
    let lang = $(this).data('lang');
    let wrapper = $(this).parent().parent();

    wrapper.find('.label').removeClass('label-success').addClass('label-primary');
    $(this).addClass('label-success').removeClass('label-primary');

    wrapper.find('.lang-version').hide();
    wrapper.find('.lang-' + lang).show();
});
JS
        );

        $out = Html::beginTag('div', ['id' => 'lang_' . $this->options['id']]);

        if ($this->hasModel()) {
            $out .= $this->renderActiveField();
        } else {
            $out .= $this->renderStandAloneField();
        }

        $out .= Html::tag(
            'div',
            'Język: ' . implode(' ', $this->getLangSwitchers()),
            [
                'class' => 'pull-right',
                'style' => 'font-size:.7em',
            ]
        );
        $out .= Html::endTag('div');

        return $out;
    }

    private $_languagesValueEmpty = [];

    public function renderActiveField()
    {
        $out = '';

        $options = $this->options;
        $options['class'] .= ' lang-pl';
        $out .= Html::activeTextInput($this->model, $this->attribute, $options);
        $this->_languagesValueEmpty['pl'] = empty($this->model->{$this->attribute});

        foreach ($this->_languages as $language) {
            $options = $this->options;
            $options['id'] .= '-lang-' . $language;
            $options['class'] .= ' lang-' . $language;
            $options['style'] .= 'display:none';

            $value = null;
            if (array_key_exists($language, $this->translations)) {
                $value = $this->translations[$language]->{$this->type};
            }
            $out .= Html::textInput('lang[' . $this->type . '][' . $language . ']', $value, $options);
            $this->_languagesValueEmpty[$language] = empty($value);
        }

        return $out;
    }

    public function renderStandAloneField()
    {
        $value = $this->valueModel ? $this->valueModel->fld_value : null;

        $out = '';

        $options = $this->options;
        $options['class'] .= ' lang-pl';

        if ($this->type === 'textarea') {
            $out .= Html::textarea($this->name . '[pl]', $value, $options);
        } else {
            $out .= Html::textInput($this->name . '[pl]', $value, $options);
        }
        $this->_languagesValueEmpty['pl'] = empty($value);

        foreach ($this->_languages as $language) {
            $options = $this->options;
            $options['id'] .= '-lang-' . $language;
            $options['class'] .= ' lang-' . $language;
            $options['style'] .= 'display:none';

            $value = null;
            if ($this->valueModel && $translatedValue = $this->valueModel->getTranslatedValue($language)) {
                $value = $translatedValue->value;
            }

            if ($this->type === 'textarea') {
                $out .= Html::textarea($this->name . '[' . $language . ']', $value, $options);
            } else {
                $out .= Html::textInput($this->name . '[' . $language . ']', $value, $options);
            }
            $this->_languagesValueEmpty[$language] = empty($value);
        }

        return $out;
    }

    public function getLangSwitchers()
    {
        $langSwitchers = [Html::a(
            ($this->_languagesValueEmpty['pl'] ? '<i class="fa fa-exclamation-triangle"></i> ' : '') . 'PL',
            '#',
            [
                'data-lang' => 'pl',
                'class' => 'label label-success switch-language',
                'style' => 'font-size:.8em',
            ]
        )];

        foreach ($this->_languages as $language) {
            $langSwitchers[] = Html::a(
                ($this->_languagesValueEmpty[$language] ? '<i class="fa fa-exclamation-triangle"></i> ' : '') . strtoupper($language),
                '#',
                [
                    'data-lang' => $language,
                    'class' => 'label label-primary switch-language',
                    'style' => 'font-size:.8em',
                ]
            );
        }

        return $langSwitchers;
    }
}
