<div class="section">
	<div class="form-horizontal">
		<h3>Parametry</h3>
		<table class="table">
			<thead>
				<tr>
					<th class="col col-md">Parametr</th>
					<th>Opis</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>height</th>
					<td>
						Wysokość obszaru tekstowego w trakcie edycji. Dla jednej linii należy podać wysokość 34px, dla dwóch linii 54px, dla trzech linii 74px, na każdą kolejną linię należy dodać 20px.
						<br><strong>możliwe wartości</strong>: string|null
						</br><strong>wartość domyślna</strong>: 380px
						<br><code>{"height":"54px"}</code>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>