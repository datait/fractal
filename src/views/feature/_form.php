<?php

use yii\helpers\Html;

?>

<div class="section section-search">
	<?php if (!$model->isNewRecord && \Yii::$app->user->can('cms/feature/delete', ['feature' => $model])): ?>
		<div class="btn-group pull-right">
			<?= Html::a('Usuń', ['/cms/feature/delete', 'id' => $model->fld_id], ['class' => 'btn btn-danger']) ?>
		</div>
	<?php endif ?>
</div>

<div class="section">
	<?php $form = \yii\bootstrap\ActiveForm::begin([
		'id' => 'content-form',
		'layout' => 'horizontal',
		'fieldConfig' => [
			'template' => '{label}<div class="col-sm-9">{input} {error}</div>'
		],
	]); ?>

		<h3>Informacje podstawowe</h3>
		<?= $form->field($model, 'fld_name', ['options' => ['class' => 'form-group form-group-lg']])->textInput() ?>
		<?= $form->field($model, 'fld_id', ['options' => ['class' => 'form-group']])->textInput() ?>
		<?= $form->field($model, 'fld_type_id')->dropDownList($model->getTypesDropDown()) ?>
		<?= $form->field($model, 'fld_restriction', ['options' => ['class' => 'form-group']])->textarea() ?>

		<div class="bottom">
			<?= Html::submitButton($model->isNewRecord ? 'Utwórz' : 'Zapisz', ['class' => 'btn btn-success']) ?>
		</div>

	<?php \yii\bootstrap\ActiveForm::end(); ?>
</div>