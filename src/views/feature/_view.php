<?php

use yii\helpers\Html;

?>

<div class="section section-search">
	<?php if (Yii::$app->user->can('cms/feature/update', ['feature' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Edytuj', ['/cms/feature/update', 'id' => $model->fld_id], ['class' => 'btn btn-primary']) ?>
		</div>
	<?php endif ?>
	<?php if (Yii::$app->user->can('cms/feature/delete', ['feature' => $model])): ?>
		<div class="btn-group pull-right">
			<?= Html::a('Usuń', ['/cms/feature/delete', 'id' => $model->fld_id], ['class' => 'btn btn-danger']) ?>
		</div>
	<?php endif ?>
</div>

<div class="section">
	<div class="form-horizontal">
		<h3>Informacje podstawowe</h3>
		<div class="form-group form-group-lg">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_name') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<strong><?= Html::a(Html::encode($model->fld_name), ['/cms/feature/view', 'id' => $model->fld_id]) ?></strong>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_id) ?>
				</div>
			</div>
		</div>

		<div class="form-group form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_type_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->type['fld_name']) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_restriction') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= nl2br(Html::encode($model->fld_restriction)) ?>
				</div>
			</div>
		</div>
	</div>
</div>