<?php

use datait\fractal\search\FeatureSearch;

?>

<?php
	$searchModel = new FeatureSearch;
	$searchModel->view = 'features';

	$searchModel->saveFilters(Yii::$app->request->getQueryParams());
	$searchModel->loadFilters();

	$dataProvider = $searchModel->search();
	$dataProvider->pagination = false;
?>

<?= $this->render(
	'/feature/_search',
	[
		'searchModel' => $searchModel,
	]
) ?>
<?= $this->render(
	'/feature/_index',
	[
		'dataProvider' => $dataProvider,
	]
) ?>