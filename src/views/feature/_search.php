<?php

use yii\helpers\Html;

?>

<div class="section section-search">
	<?php if (Yii::$app->user->can('cms/feature/create')): ?>
		<div class="btn-group">
			<?= Html::a('Utwórz cechę', ['/cms/feature/create'], ['class' => 'btn btn-primary']) ?>
		</div>
	<?php endif ?>
</div>