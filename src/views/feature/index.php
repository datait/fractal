<h1>Właściwości</h1>

<?php
	$items[] = [
		'label' => 'Cechy',
		'content' => $this->render('_features'),
		'active' => !isset($active) || (isset($active) && $active == 'features'),
		'headerOptions' => ['class' => 'main'],
	];

	echo \yii\bootstrap\Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>