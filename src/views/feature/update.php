<?php

use yii\helpers\Html;

?>

<?php
    $this->title = 'Edycja cechy';
?>

<div class="row row-title">
	<div class="col col-main">
		<h1><?= Html::encode($this->title) ?></h1>
	</div>
</div>

<?php
	$items[] = [
		'label' => 'Cecha',
		'content' => $this->render('/feature/_form', ['model' => $model, 'view' => '/cms/feature/update']),
		'active' => !isset($active) || (isset($active) && $active == 'feature'),
		'headerOptions' => ['class' => 'main'],
	];

	echo \yii\bootstrap\Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>