<?php

use yii\helpers\Html;

?>

<div class="section">
	<?= \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => '{items}<div class="row"><div class="col-lg-6">{summary}</div><div class="col-lg-6">{pager}</div></div>',
		'tableOptions' => [
			'class' => 'table table-hover'
		],
		'pager' => [
			'options' => [
				'class' => 'pagination pull-right'
			]
		],
		'columns' => [
			[
				'label' => 'Nazwa',
				'value' => function($data) {
					if (\Yii::$app->user->can('cms/feature/update', ['feature' => $data])) {
						$main = '<strong>' . Html::a(Html::encode($data->fld_name) . (strlen($data->fld_name) == 0 ? ' <span class="none">(brak)</span>' : ''), ['/cms/feature/update', 'id' => $data->fld_id]) . '</strong>';
					} else {
						$main = '<strong>' . Html::a(Html::encode($data->fld_name) . (strlen($data->fld_name) == 0 ? ' <span class="none">(brak)</span>' : ''), ['/cms/feature/view', 'id' => $data->fld_id]) . '</strong>';
					}

					$adds = '<strong>' . Html::encode($data->fld_id) . '</strong>';

					return $main . (strlen($adds) ? '<br><small>' . $adds . '</small>' : '');
				},
				'format' => 'html',
				'headerOptions' => [
					'class' => 'col-main'
				],
				'contentOptions' => [
					'class' => 'col-main'
				],
			],
		],
	]); ?>
</div>