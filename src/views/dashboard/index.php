<?php

use datait\fractal\models\Content;
use yii\helpers\Html;

?>

    <div class="row row-title">
	<div class="col col-main">
		<h1><?= Html::a('Pulpit', ['/cms/dashboard']) ?></h1>
	</div>
</div>

<?php if (Yii::$app->user->can('cms/content/create')): ?>
	<?= Html::a('Utwórz zawartosć', ['/cms/content/create'], ['class' => 'btn btn-primary']) ?>
	<?= Html::a('Wyczyść cache', ['/cms/content/flush'], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Wyczyść zmienne sesyjne', ['/cms/content/clear-session'], ['class' => 'btn btn-primary']) ?>
<?php endif ?>