<?php

use datait\fractal\models\Settings;
use yii\helpers\Html;

?>
<div class="form-group" style="position: relative">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) ?></label>
	<div class="col-sm-9 image-padding">
		<?php if ($model->getSubvalue('size', $property->fld_feature_id)): ?>
			<div class="form-control-static">
				<?php
					$domainName = Settings::find()->one()->fld_domain;
					$imageName = Html::encode((strlen($model->getSubvalue('name', $property->fld_feature_id)) ? $model->getSubvalue('name', $property->fld_feature_id) : $model->getSubvalue('baseName', $property->fld_feature_id)) . '.' . $model->getSubvalue('extension', $property->fld_feature_id));
				?>

				<?= Html::a($imageName, $domainName . \Yii::$app->params['imagesPagePath'] . $imageName) ?>&nbsp;&nbsp;&nbsp;<span class="labels"><?= Html::a('Usuń plik', ['/value/delete', 'id' => $model->getValueByFeatureId($property->fld_feature_id)->fld_id], ['class' => 'btn btn-xxs btn-danger']) ?></span>
			</div>
			<div class="image-wrapper image-simple" style="background-image: url('<?= $domainName . \Yii::$app->params['imagesPagePath'] . $imageName ?>')"></div>
		<?php else: ?>
			<div class="form-control-static">
				<?= Html::fileInput('Feature[' . $property->fld_feature_id . '][file]') ?>
			</div>
		<?php endif ?>
	</div>
</div>