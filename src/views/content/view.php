<?php

use yii\helpers\Html;
use datait\fractal\models\Worker;


?>

<?php
    $this->title = 'Zawartość #' . $model->fld_id . ': ' . Html::encode($model->fld_name);
?>

<div class="row row-title">
	<div class="col col-main">
		<div class="labels-top">
			<span class="btn btn-xxs btn-<?= $model->fld_active_id == 'yes' ? 'success' : 'warning' ?>" disabled="disabled"><?= $model->fld_active_id == 'yes' ? 'aktywna' : 'nieaktywna' ?></span>
		</div>
		<h1><?= Html::a($this->title, ['/cms/content/view', 'id' => $model->fld_id]) ?></h1>
	</div>
</div>

<?php
	if ($model->fld_parent_id) {
		if ($model->root->getParam('showParent') == 'false' && !in_array(Yii::$app->user->identity->ident->fld_type_id, [Worker::TYPE_WEBMASTER, Worker::TYPE_PARTNER])) {
			$active = 'contents';
			$main = 'contents';
		} else {
			$items[] = [
				'label' => $model->parent->getParam('maxLevel') == 0 ? (strlen($model->parent->fld_name) ? $model->parent->fld_name : 'Zawartość') : 'Zawartość',
				'content' => $this->render('/content/_view', ['model' => $model->parent, 'view' => '/cms/content/update', 'params' => ['id' => $model->fld_id]]),
				'active' => isset($active) && $active == 'parent',
			];
		}
	}

	if ($model->root->fld_id == $model->fld_id && $model->root->getParam('showParent') == 'false' && !in_array(Yii::$app->user->identity->ident->fld_type_id, [Worker::TYPE_WEBMASTER, Worker::TYPE_PARTNER])) {
		$active = 'contents';
		$main = 'contents';
	} else {
		$items[] = [
			'label' => $model->root->getParam('maxLevel') == 0 ? ($model->getParam('tabLabel') ? $model->getParam('tabLabel') : $model->fld_name) : ($model->getParam('tabLabel') ? $model->getParam('tabLabel') : ($model->fld_parent_id ? $model->parent->getParam('tabChildLabel', 'Zawartość') : 'Zawartość')),
			'content' => $this->render('/content/_view', ['model' => $model, 'view' => '/cms/content/view', 'params' => ['id' => $model->fld_id]]),
			'headerOptions' => !isset($main) || (isset($main) && $main == 'content') ? ['class' => 'main'] : [],
			'active' => !isset($active) || (isset($active) && $active == 'content'),
		];
	}

	if (is_null($model->root->getParam('maxLevel')) || $model->getLevel() < $model->root->getParam('maxLevel')) {
		$items[] = [
			'label' => $model->getParam('maxLevel') > 0 ? ($model->getParam('tabChildrenLabel') ? $model->getParam('tabChildrenLabel') : (strlen($model->fld_name) ? $model->fld_name : 'Elementy')) : 'Elementy',
			'content' => $this->render('/content/_contents', ['model' => $model, 'view' => '/cms/content/view', 'params' => ['id' => $model->fld_id]]),
			'headerOptions' => isset($main) && $main == 'contents' ? ['class' => 'main'] : [],
			'active' => isset($active) && $active == 'contents',
		];
	}

	if (Yii::$app->user->can('cms/content/help')) {
		$items[] = [
			'label' => 'Pomoc',
			'content' => $this->render('/content/_help'),
			'active' => isset($active) && $active == 'help',
			'headerOptions' => ['class' => 'pull-right'],
		];
	}

	if (Yii::$app->user->can('cms/property/index', ['content' => $model])) {
		$items[] = [
			'label' => 'Właściwości',
			'content' => $this->render('/content/_properties', ['model' => $model, 'view' => '/cms/content/view', 'params' => ['id' => $model->fld_id]]),
			'active' => isset($active) && $active == 'properties',
			'headerOptions' => ['class' => 'pull-right'],
		];
	}

	echo \yii\bootstrap\Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>