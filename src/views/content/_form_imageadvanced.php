<?php

use yii\helpers\Html;

?>

<div class="form-group" style="position: relative">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) . ' (nazwa pliku)' ?></label>
	<div class="col-sm-9 image-padding">
		<?php
			$domainName = Yii::$app->params['domain'];
			$imageName = Html::encode((strlen($model->getSubvalue('name', $property->fld_feature_id)) ? $model->getSubvalue('name', $property->fld_feature_id) : $model->getSubvalue('baseName', $property->fld_feature_id)) . '.' . $model->getSubvalue('extension', $property->fld_feature_id));
		?>

		<?= Html::textInput('Feature[' . $property->fld_feature_id . '][name]', $model->getSubvalue('name', $property->fld_feature_id), ['class' => 'form-control']) ?>
	</div>
	<div class="image-wrapper image-advanced" style="background-image: url('<?= '/' . implode('/', [Yii::$app->params['imagesPublicPath'], $imageName]) ?>')"></div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) . ' (alt)' ?></label>
	<div class="col-sm-9 image-padding">
		<?= Html::textInput('Feature[' . $property->fld_feature_id . '][alt]', $model->getSubvalue('alt', $property->fld_feature_id), ['class' => 'form-control']) ?>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) ?></label>
	<div class="col-sm-9 image-padding">
		<?php if ($model->getSubvalue('size', $property->fld_feature_id)): ?>
			<div class="form-control-static">
				<?= Html::a($imageName, '/' . implode('/', [Yii::$app->params['imagesPublicPath'], $imageName])) ?>&nbsp;&nbsp;&nbsp;<?php if (Yii::$app->user->can('cms/content/update', ['content' => $model])): ?><span class="labels"><?= Html::a('Usuń plik', ['/cms/value/delete', 'id' => $model->getValueByFeatureId($property->fld_feature_id)->fld_id], ['class' => 'btn btn-xxs btn-danger']) ?></span><?php endif ?>
			</div>
		<?php else: ?>
			<div class="form-control-static">
				<?= Html::fileInput('Feature[' . $property->fld_feature_id . '][file]') ?>
			</div>
		<?php endif ?>
	</div>
</div>