<?php

use datait\fractal\models\Content;
use datait\fractal\search\ContentSearch;

$searchModel = new ContentSearch;
	$searchModel->view = 'contents';

	$searchModel->saveFilters(Yii::$app->request->getQueryParams());
	$searchModel->loadFilters();

	if (isset($model)) {
		$searchModel->parent = $model->fld_id;
	} else {
		$searchModel->parent = null;
	}

	if (isset($model) && $model->fld_sort_id === Content::SORT_DESC_YES) {
		$searchModel->sort = 'fld_sort DESC';
	} else {
		$searchModel->sort = 'fld_sort';
	}

	$dataProvider = $searchModel->search();
	$dataProvider->setPagination(false);
?>

<?php
	if (isset($model)) {
		echo $this->render('/content/_search', ['searchModel' => $searchModel, 'model' => $model]);
	} else {
		echo $this->render('/content/_search', ['searchModel' => $searchModel]);
	}
	
	echo $this->render('/content/_index', ['dataProvider' => $dataProvider]);
?>