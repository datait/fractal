<?php

use datait\fractal\models\LanguageVersion;
use yii\helpers\Html;

?>
<div class="form-group">
	<label class="control-label col-sm-3"><?= $property->getParam('label') ? $property->getParam('label') : Html::encode($property->feature->fld_name) ?></label>
	<div class="col-sm-9">
        <?= LanguageVersion::widget([
            'name' => 'Feature[' . $property->fld_feature_id . ']',
            'valueModel' => $model->getValueByFeatureId($property->fld_feature_id),
            'type' => 'textarea',
            'options' => [
                'style' => $property->getParam('height') ? 'height: ' . $property->getParam('height') : ''
            ],
        ]) ?>
	</div>
</div>