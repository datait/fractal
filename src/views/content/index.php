<h1>Zawartości</h1>

<?php
	$items[] = [
		'label' => 'Elementy',
		'content' => $this->render('/content/_contents'),
		'active' => !isset($active) || (isset($active) && $active == 'contents'),
		'headerOptions' => ['class' => 'main'],
	];

	echo \yii\bootstrap\Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>