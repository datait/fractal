<?php

use datait\fractal\models\Settings;
use yii\helpers\Html;

?>

<?php if ($value): ?>
	<div class="form-group" style="position: relative">
		<label class="control-label col-sm-3"><?= Html::encode($value->feature->fld_name) . ' (nazwa pliku)' ?></label>
		<div class="col-sm-9">
			<div class="form-control-static">
				<?php
					$domainName = Settings::find()->one()->fld_domain;
					$fileName = Html::encode((strlen($value->getSubvalue('name')) ? $value->getSubvalue('name') : $value->getSubvalue('baseName')) . '.' . $value->getSubvalue('extension'));
				?>

				<?= Html::encode($value->getSubvalue('name')) ?>

				<div class="image-wrapper image-advanced" style="background-image: url('/img/filetype/<?= $value->getSubvalue('extension') . '.png' ?>')">
					<?php
						if ($value->getFirst() && $value->getFirst()->fld_id != $value->fld_id) {
							$up = Html::a('<i class="fa fa-fw fa-caret-up"></i>', ['/value/up', 'id' => $value->fld_id]);
						} else {
							$up = '<i class="fa fa-fw"></i>';
						}

						if ($value->getLast() && $value->getLast()->fld_id != $value->fld_id) {
							$down = Html::a('<i class="fa fa-fw fa-caret-down"></i>', ['/value/down', 'id' => $value->fld_id]);
						} else {
							$down = '<i class="fa fa-fw"></i>';
						}
					?>
					
					<div class="image-sort">
						<span class="sort sort-up"><?= $up ?></span><span class="sort sort-down"><?= $down ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3"><?= Html::encode($value->feature->fld_name) . ' (title)' ?></label>
		<div class="col-sm-9 image-padding">
			<div class="form-control-static">
				<?= Html::encode($value->getSubvalue('title')) ?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3"><?= Html::encode($value->feature->fld_name) ?></label>
		<div class="col-sm-9 image-padding">
			<?php if ($value->getSubvalue('size')): ?>
				<div class="form-control-static">
					<?= Html::a($fileName, $domainName . \Yii::$app->params['filesPagePath'] . $fileName) ?>&nbsp;&nbsp;&nbsp;<?php if (\Yii::$app->user->can('cms/content/update', ['content' => $value->content])): ?><span class="labels"><?= Html::a('Usuń plik', ['/value/delete', 'id' => $value->fld_id], ['class' => 'btn btn-xxs btn-danger']) ?></span><?php endif ?>
				</div>
			<?php else: ?>
				<div class="form-control-static">
					(brak pliku)
				</div>
			<?php endif ?>
		</div>
	</div>
<?php endif ?>