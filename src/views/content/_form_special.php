<div class="form-group">
	<label class="control-label col-sm-3"><?= $property->getParam('label') ?></label>
	<div class="col-sm-9">
		<?php
			if ($widget = $property->feature->getParam('widget')) {
				switch ($widget) {
					case 'import-potencjalni':
						echo ImportPotencjalni::widget(['content' => $model, 'property' => $property]);

						break;
				}
			}
		?>
	</div>
</div>