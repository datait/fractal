<?php

use datait\fractal\models\Content;
use datait\fractal\models\Feature;
use yii\helpers\Html;

?>
<div class="section">
    <form method="post">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->csrfToken ?>" />
    </form>
	<?= \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => '{items}<div class="row"><div class="col-lg-6">{summary}</div><div class="col-lg-6">{pager}</div></div>',
		'tableOptions' => [
			'class' => 'table table-hover sortable'
		],
		'rowOptions' => function($model, $key, $index, $grid) {
			return ['id' => $model->fld_id];
		},
		'pager' => [
			'options' => [
				'class' => 'pagination pull-right'
			],
		],
		'columns' => [
			[
				'label' => 'Nazwa',
				'value' => function($data) {
					if ($data->fld_parent_id && $list = $data->parent->getParam('list')) {
						if (isset($list['left-top']) && $leftTop = $list['left-top']) {
							if (is_array($leftTop)) {
								$feature = $leftTop['feature'];

								if ($leftTop['value'] == 'name') {
									if ($value = $data->getValueByFeatureId($feature)) {
										$header = Content::findOne($value->fld_value)->fld_name;
									}
								}
							} else {
								$feature = $leftTop;

								if (\app\models\Feature::findOne($feature)->fld_type_id == Feature::TYPE_DROPDOWNLIST) {
									if ($value = $data->getValueByFeatureId($feature)) {
										$header = Content::findOne($value->fld_value)->fld_name;
									}
								} else {
									if ($value = $data->getValueByFeatureId($feature)) {
										$header = $value->fld_value;
									}
								}
							}
						}

						if (isset($list['left-bottom']) && $leftBottom = $list['left-bottom']) {
							if (is_array($leftBottom)) {
								$feature = $leftBottom['feature'];

								if ($leftBottom['value'] == 'name') {
									if ($value = $data->getValueByFeatureId($feature)) {
										$adds = Content::findOne($value->fld_value)->fld_name;
									}
								}
							} else {
								$feature = $leftBottom;

                                if (\app\models\Feature::findOne($feature)->fld_type_id == Feature::TYPE_DROPDOWNLIST) {
                                    if ($value = $data->getValueByFeatureId($feature)) {
                                        $adds = Content::findOne($value->fld_value)->fld_name;
                                    }
                                } else {
                                    if ($value = $data->getValueByFeatureId($feature)) {
                                        $adds = $value->fld_value;
                                    }
                                }
							}
						}
					}

					$mode = Yii::$app->user->can('cms/content/update', ['content' => $data])
					    ? 'update'
					    : 'view';

					$defaultLabel = $data->fld_default_id == Content::DEFAULT_YES
                        ? '<span class="btn btn-xxs btn-primary" disabled="disabled">Domyślna</span> '
                        : '';

                    $main = '<strong>' . Html::a(Html::encode($data->fld_name) . (strlen($data->fld_name) == 0 ? ' <span class="none">(brak)</span>' : ''), ['/cms/content/' . $mode, 'id' => $data->fld_id]) . '</strong> <span class="labels">' . $defaultLabel . '<span class="btn btn-xxs btn-' . ($data->fld_active_id == 'yes' ? 'success' : 'danger') . '" disabled="disabled">' . ($data->fld_active_id == 'yes' ? 'aktywna' : 'nieaktywna') . '</span></span>';

					$uri = ($data->fld_uri) ? 'uri: ' . Html::a(Html::encode($data->fld_uri), \Yii::$app->params['domain'] . '/' . $data->fld_uri) : '';

					return (isset($header) && strlen($header) ? '<span class="header">' . $header . '</span><br>' : '') . $main . (isset($adds) ? '<br><small>' . $adds . '</small>' : '');
				},
				'format' => 'html',
				'headerOptions' => [
					'class' => 'col-main'
				],
				'contentOptions' => [
					'class' => 'col-main'
				],
			],
			[
				'value' => function($data) {
					if ($data->fld_parent_id && $list = $data->parent->getParam('list')) {
						foreach (['top', 'middle', 'bottom'] as $position) {
							if (isset($list['right-' . $position]) && $content = Feature::findOne($list['right-' . $position])) {
                                if (is_array($list['right-' . $position])) {
                                    $feature = $list['right-' . $position]['feature'];

                                    if ($list['right-' . $position]['value'] == 'name') {
                                        if ($value = $data->getValueByFeatureId($feature)) {
                                            $$position = Content::findOne($value->fld_value)->fld_name;
                                        }
                                    }
                                } else {
                                    $feature = $list['right-' . $position];

                                    if (\app\models\Feature::findOne($feature)->fld_type_id == Feature::TYPE_DROPDOWNLIST) {
                                        if ($value = $data->getValueByFeatureId($feature)) {
                                            $$position = Content::findOne($value->fld_value)->fld_name;
                                        }
                                    } else {
                                        if ($value = $data->getValueByFeatureId($feature)) {
                                            $$position = $value->fld_value;
                                        }
                                    }
                                }
							}
						}

						return '<div class="header">' . (isset($top) ? $top : '') . '</div><div>' . (isset($middle) ? $middle : '') . '</div><div><small>' . (isset($bottom) ? $bottom : '') . '</small></div>';
					} else {
						return '';
					}
				},
				'format' => 'html',
			],
			[
				'value' => function($data) {
					if (\Yii::$app->user->can('cms/content/delete', ['content' => $data])) {
						return '<span class="delete">' . Html::a('<i class="fa fa-fw fa-times text-danger"></i>', ['/cms/content/delete', 'id' => $data->fld_id]) . '</span>';
					} else {
						return '<span class="delete"><i class="fa fa-fw"></i></span>';
					}
				},
				'format' => 'html',
				'contentOptions' => [
					'class' => 'col-xxs text-center col-delete',
					'style' => 'vertical-align: middle'
				],
			],
		],
	]); ?>
</div>