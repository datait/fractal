<?php

use yii\helpers\Html;

?>

<div class="section section-search">
	<?php if (Yii::$app->user->can('cms/content/create', ['content' => isset($model) ? $model : null])): ?>
		<div class="btn-group">
			<?= Html::a($model->getParam('createButtonLabel') ? $model->getParam('createButtonLabel') : 'Utwórz zawartosć', ['/cms/content/create', 'parent_id' => $model->fld_id], ['class' => 'btn btn-primary']) ?>
		</div>
	<?php endif ?>
</div>