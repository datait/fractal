<?php

use datait\fractal\models\Log;
use yii\data\ActiveDataProvider;

?>

<?php
	$dataProvider = new ActiveDataProvider([
		'query' => Log::find()->where(['fld_kind_id' => Log::KIND_WORKER, 'fld_object_id' => $model->fld_id])->orderBy('fld_id DESC')
	]);

	echo $this->render('/log/_index', ['dataProvider' => $dataProvider]);
?>