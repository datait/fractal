<?php

use yii\helpers\Html;

?>

<?php if ($model->getValues()->andWhere(['fld_feature_id' => $property->fld_feature_id])->all()): ?>
	<?= $this->render('_view_' . $property->feature->type['fld_id'], ['model' => $model, 'property' => $property]) ?>
<?php endif ?>
<div class="form-group">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) . ' (nazwa pliku)' ?></label>
	<div class="col-sm-9">
		<?= Html::textInput('Feature[' . $property->fld_feature_id . '][name]', null, ['class' => 'form-control']) ?>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) . ' (alt)' ?></label>
	<div class="col-sm-9">
		<?= Html::textInput('Feature[' . $property->fld_feature_id . '][alt]', null, ['class' => 'form-control']) ?>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) ?></label>
	<div class="col-sm-9">
		<div class="form-control-static">
			<?= Html::fileInput('Feature[' . $property->fld_feature_id . '][file]') ?>
		</div>
	</div>
</div>