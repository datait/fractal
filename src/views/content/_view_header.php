<?php

use yii\helpers\Html;

?>
<h4>
	<?php if (strlen(trim($model->getValueByFeatureId($property->fld_feature_id)['fld_value']))) {
			echo nl2br(Html::encode($model->getValueByFeatureId($property->fld_feature_id)['fld_value']));
		} else {
			echo Html::encode(Html::encode($property->feature->fld_name));
		}
	?>
</h4>
<?= !is_null($property->feature->getParam('description')) ? '<p>' . $property->feature->getParam('description') . '</p>' : '' ?>