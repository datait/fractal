<?php

use yii\helpers\Html;
use datait\fractal\models\Worker;

?>

<?php
    $this->title = 'Tworzenie zawartości';
?>

<div class="row row-title">
	<div class="col col-main">
		<div class="labels-top">
			<span class="btn btn-xxs btn-<?= $model->fld_active_id == 'yes' ? 'success' : 'warning' ?>" disabled="disabled"><?= $model->fld_active_id == 'yes' ? 'aktywna' : 'nieaktywna' ?></span>
		</div>
		<h1><?= Html::encode($this->title) ?></h1>
	</div>
</div>

<?php
	if ($model->fld_parent_id) {
		if ($model->root->getParam('showParent') == 'false' && !in_array(Yii::$app->user->identity->ident->fld_type_id, [Worker::TYPE_WEBMASTER, Worker::TYPE_PARTNER])) {
			$active = 'contents';
		} else {
			$items[] = [
				'label' => $model->parent->getParam('tabLabel') ? $model->parent->getParam('tabLabel') : 'Zawartość',
				'content' => $this->render('/content/_view', ['model' => $model->parent, 'view' => '/cms/content/update', 'params' => ['id' => $model->fld_id]]),
				'active' => isset($active) && $active == 'parent',
			];
		}
	}

	$items[] = [
		'label' => $model->fld_parent_id && $model->parent->getParam('maxLevel') > 0 ? ($model->fld_parent_id ? $model->parent->getParam('tabChildLabel', 'Zawartość') : 'Zawartość') : 'Zawartość',
		'content' => $this->render('/content/_form', ['model' => $model, 'view' => '/cms/content/create']),
		'active' => !isset($active) || (isset($active) && $active == 'content'),
		'headerOptions' => ['class' => 'main'],
	];

	echo \yii\bootstrap\Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>