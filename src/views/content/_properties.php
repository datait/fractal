<?php

use datait\fractal\search\PropertySearch;

?>

<?php
	$searchModel = new PropertySearch;
	$searchModel->view = 'properties';

	$searchModel->saveFilters(Yii::$app->request->getQueryParams());
	$searchModel->loadFilters();

	$searchModel->content = $model->fld_id;

	$dataProvider = $searchModel->search();
	$dataProvider->pagination = false;
?>

<?php
	echo $this->render('/property/_search', ['searchModel' => $searchModel, 'model' => $model]);
	echo $this->render('/property/_index', ['dataProvider' => $dataProvider]);
?>