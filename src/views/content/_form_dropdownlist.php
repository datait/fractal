<?php

use yii\helpers\Html;

?>

<div class="form-group">
	<label class="control-label col-sm-3"><?= $property->getParam('label') ? $property->getParam('label') : Html::encode($property->feature->fld_name) ?></label>
	<div class="col-sm-9">
		<?php echo Html::dropdownList('Feature[' . $property->fld_feature_id . ']', $model->getValueByFeatureId($property->fld_feature_id)['fld_value'], $model->getPropertyItems($property), ['class' => 'form-control', 'prompt' => '(dokonaj wyboru)']) ?>
	</div>
</div>