<div class="list list-value sortable">
	<?php foreach ($model->getValues()->andWhere(['fld_feature_id' => $property->fld_feature_id])->orderBy('fld_sort')->all() as $value): ?>
		<div class="list-item" id="<?= $value->fld_id ?>">
			<?= $this->render('_view_imageadvanced_value', ['model' => $model, 'value' => $value]) ?>
		</div>
	<?php endforeach ?>
</div>