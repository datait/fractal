<?php

use yii\helpers\Html;

?>

<div class="form-group">
	<label class="control-label col-sm-3"><?= $property->getParam('label') ? $property->getParam('label') : Html::encode($property->feature->fld_name) ?></label>
	<div class="col-sm-9">
		<div class="checkbox">
			<?php echo Html::checkboxList('Feature[' . $property->fld_feature_id . ']', $model->getValueValueByFeatureId($property->fld_feature_id), $model->getPropertyItems($property)) ?>
		</div>
	</div>
</div>