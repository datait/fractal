<?php

use datait\fractal\models\Content;
use datait\fractal\models\Property;
use datait\fractal\models\Settings;
use datait\fractal\models\Worker;
use yii\helpers\Html;


/* @var $model Content */
?>
<div class="section section-search">
	<?php if (Yii::$app->user->can('cms/content/update', ['content' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Edytuj', ['/cms/content/update', 'id' => $model->fld_id, 'active' => 'content'], ['class' => 'btn btn-primary']) ?>
		</div>
	<?php endif ?>
	<?php if (Yii::$app->user->can('cms/content/activate', ['content' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Aktywuj', ['/cms/content/activate', 'id' => $model->fld_id], ['class' => 'btn btn-success']) ?>
		</div>
	<?php endif ?>
	<?php if (Yii::$app->user->can('cms/content/deactivate', ['content' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Deaktywuj', ['/cms/content/deactivate', 'id' => $model->fld_id], ['class' => 'btn btn-warning']) ?>
		</div>
	<?php endif ?>
	<?php if (Yii::$app->user->can('cms/content/delete', ['content' => $model])): ?>
		<div class="btn-group pull-right">
			<?= Html::a('Usuń', ['/cms/content/delete', 'id' => $model->fld_id], ['class' => 'btn btn-danger']) ?>
		</div>
	<?php endif ?>
</div>

<div class="section">
	<div class="form-horizontal">
		<h3>Informacje podstawowe</h3>
		<?php if ($model->getParam('description')): ?>
			<div class="bs-callout bs-callout-info"><?= $model->getParam('description') ?></div>
		<?php endif ?>
		<?php
			if ($model->getParam('nameFieldLabel')) {
				$nameLabel = $model->getParam('nameLabel');
			} elseif ($model->parent && $model->parent->getParam('nameFieldChildLabel')) {
				$nameLabel = $model->parent->getParam('nameFieldChildLabel');
			} else {
				$nameLabel = $model->getAttributeLabel('fld_name');
			}
		?>
		<div class="form-group form-group-lg">
			<label class="control-label col-sm-3"><?= $nameLabel ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?php if (Yii::$app->user->can('cms/content/update', ['content' => $model])): ?>
						<strong><?= Html::a(Html::encode($model->fld_name), ['/cms/content/update', 'id' => $model->fld_id]) ?></strong>
					<?php else: ?>
						<strong><?= Html::a(Html::encode($model->fld_name), ['/cms/content/view', 'id' => $model->fld_id]) ?></strong>
					<?php endif ?>
				</div>
			</div>
		</div>

		<?php if (isset($model->fld_parent_id)): ?>
			<div class="form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_parent_id') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?php if (Yii::$app->user->can('cms/content/update', ['content' => $model->parent])): ?>
							<?= Html::a(Html::encode($model->parent->fld_name), ['/cms/content/update', 'id' => $model->fld_parent_id]) ?>
						<?php else: ?>
							<?= Html::a(Html::encode($model->parent->fld_name), ['/cms/content/view', 'id' => $model->fld_parent_id]) ?>
						<?php endif ?>
					</div>
				</div>
			</div>
		<?php endif ?>

		<?php if ((int)Yii::$app->user->identity->ident->fld_type_id === Worker::TYPE_PARTNER
            || $model->root->getParam('showSystemField') === 'true'): ?>
			<div class="form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_system') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?= Html::encode($model->fld_system) ?>
					</div>
				</div>
			</div>
		<?php endif ?>

		<?php if ((int)Yii::$app->user->identity->ident->fld_type_id === Worker::TYPE_PARTNER): ?>
			<div class="form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_type') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?= Html::encode($model->fld_type) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_group') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?= Html::encode($model->fld_group) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_params') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?= Html::encode($model->fld_params) ?>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-3"></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<div class="checkbox">
							<?= Html::checkbox(
                                'fld_sort_id',
                                $model->fld_sort_id === Content::SORT_DESC_YES,
                                ['label' => $model->getAttributeLabel('fld_sort_id'), 'disabled' => 'disabled']
                            ) ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif ?>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_active_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<span class="label label-<?= $model->fld_active_id === 'yes' ? 'success' : 'warning' ?>" disabled="disabled">
                        <?= $model->fld_active_id === 'yes' ? 'aktywna' : 'nieaktywna' ?>
                    </span>
				</div>
			</div>
		</div>

		<?php if ((int)Yii::$app->user->identity->ident->fld_type_id === Worker::TYPE_PARTNER): ?>
			<div class="form-group form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_uri') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?php if (strlen($model->fld_uri)) {
                            if (strpos($model->fld_uri, '//')) {
                                $url = $model->fld_uri;
                            } else {
                                $url = Settings::find()->one()->fld_domain . '/' . $model->fld_uri;
                            }

                            echo Html::a(Html::encode($url), $url, ['target' => '_blank']);
                        } ?>
					</div>
				</div>
			</div>

			<?php if (!$model->fld_parent_id): ?>
				<div class="form-group">
					<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_icon') ?></label>
					<div class="col-sm-9">
						<div class="form-control-static">
							<i class="fa fa-<?= Html::encode($model->fld_icon) ?>"></i>&nbsp;&nbsp;&nbsp;<?= Html::encode($model->fld_icon) ?>
						</div>
					</div>
				</div>
			<?php endif ?>
		<?php endif ?>

		<?php if ($model->getProperties(Property::OWN_YES)): ?>
			<?php foreach ($model->getProperties(Property::OWN_YES) as $property): ?>
				<?= !empty($property->getParam('header')) ? '<h4>' . $property->getParam('header') . '</h4>': '' ?>
				<?= !empty($property->getParam('description')) ? '<p>' . $property->getParam('description') . '</p>' : '' ?>
				<?= $this->render('_view_' . $property->feature->type['fld_id'], ['model' => $model, 'property' => $property]) ?>
			<?php endforeach ?>
		<?php endif ?>

		<?php if (isset($model->fld_parent_id) && $model->parent->getProperties(Property::OWN_NO)): ?>
			<?php foreach ($model->parent->getProperties(Property::OWN_NO) as $property): ?>
				<?= !empty($property->getParam('header')) ? '<h4>' . $property->getParam('header') . '</h4>': '' ?>
				<?= !empty($property->getParam('description')) ? '<p>' . $property->getParam('description') . '</p>' : '' ?>
				<?= $this->render('_view_' . $property->feature->type['fld_id'], ['model' => $model, 'property' => $property]) ?>
			<?php endforeach ?>
		<?php endif ?>
		
	</div>
</div>

<?= $this->render('/log/_footer', ['model' => $model]) ?>