<div class="section">
	<div class="form-horizontal">
		<h3>Parametry</h3>
		<table class="table">
			<thead>
				<tr>
					<th class="col col-md">Parametr</th>
					<th>Opis</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>canCreate</th>
					<td>
						Uprawnia do dodawania zawartości podrzędnych lub zabiera taką możliwość.
						<br><strong>możliwe wartości</strong>: true|false
						</br><strong>wartość domyślna</strong>: true
						<br><code>{"canCreate":false}</code>
					</td>
				</tr>
				<tr>
					<th>canEditNameField</th>
					<td>
						Uprawnia do zmiany tekstu w polu <code>Nazwa</code> zawartości głównej przez użytkownika z rolą Edytor.
						<br><strong>możliwe wartości</strong>: true|false
						</br><strong>wartość domyślna</strong>: false
						<br><code>{"canEditNameField":true}</code>
					</td>
				</tr>
				<tr>
					<th>canEditNameChildField</th>
					<td>
						Uprawnia do zmiany tekstu w polu <code>Nazwa</code> zawartości podrzędnych przez użytkownika z rolą Edytor.
						<br><strong>możliwe wartości</strong>: true|false
						</br><strong>wartość domyślna</strong>: false
						<br><code>{"canEditNameChildField":true}</code>
					</td>
				</tr>
				<tr>
					<th>createButtonLabel</th>
					<td>
						Tekst na przycisku dodawania nowej zawartości podrzędnej. Jeśli jest podany zastąpi domyślny tekst "Utwórz zawartość".
						<br><strong>możliwe wartości</strong>: <i>string</i>|null
						</br><strong>wartość domyślna</strong>: null
						<br><code>{"createButtonLabel":"Dodaj nową osobę"}</code>
					</td>
				</tr>
				<tr>
					<th>hide</th>
					<td>
						Ukrywa daną pozycję menu przed użytkownikami o roli Edytor.
						<br><strong>możliwe wartości</strong>: true|false
						<br><strong>wartość domyślna</strong>: false
						<br><code>{"hide":true}</code>
					</td>
				</tr>
				<tr>
					<th>list</th>
					<td>
						Określa które właściwości będą prezentowane na liście elementów. Argumentem właściwości list jest lista elementów składająych się z par "pozycja":"nazwa cechy" gdzie pozycja jest połączeniem pozycji poziomej left|right, myślnika (-) i pozycji pionowej top|middle|bottom.
						<br><strong>możliwe wartości</strong>: <i>mixed</i>
						<br><code>{"list":{left-top":"kod","right-middle":"nazwa"}}</code>
					</td>
				</tr>
				<tr>
					<th>maxLevel</th>
					<td>
						Ogranicza możliwość dodawania zawartości podrzędnych do podanej głębokości. W celu zablokowania możliwości dodawania elementów podrzędnych należy podać wartość 0. Żeby umożliwić dodawanie tylko jednego poziomu zawartości należy podać wartość 1.
						<br><strong>możliwe wartości</strong>: <i>integer unsigned</i>|null
						</br><strong>wartość domyślna</strong>: null
						<br><code>{"maxLevel":0}</code>
					</td>
				</tr>
				<tr>
					<th>menuLabel</th>
					<td>
						Tekst jaki pokaże się w menu. Używane najczęściej gdy pełny tekst nie mieści się w menu.
						<br><strong>możliwe wartości</strong>: <i>string</i>|null
						</br><strong>wartość domyślna</strong>: null
						<br><code>{"menuLabel":"Formularz rejestr."}</code>
					</td>
				</tr>
				<tr>
					<th>nameFieldLabel</th>
					<td>
						Tekst przy polu <code>Nazwa</code> dla zawartości głównej. Jeśli parametr jest podany to zastąpi domyślny tekst "Nazwa". Obowiązuje dla zawartości głównej, dla zawartości podrzędnych należy użyć parametru <code>nameFieldChildLabel</code>.
						<br><strong>możliwe wartości</strong>: <i>string</i>|null
						</br><strong>wartość domyślna</strong>: "Nazwa"
						<br><code>{"nameFieldLabel":"Nazwa konferencji"}</code>
					</td>
				</tr>
				<tr>
					<th>nameFieldChildLabel</th>
					<td>
						Tekst przy polu <code>Nazwa</code> dla zawartości podrzędnych. Jeśli parametr jest podany to zastąpi domyślny tekst "Nazwa". Obowiązuje dla zawartości podrzędnych, dla zawartości głównej należy użyć parametru <code>nameFieldLabel</code>.
						<br><strong>możliwe wartości</strong>: <i>string</i>|null
						</br><strong>wartość domyślna</strong>: "Nazwa"
						<br><code>{"nameFieldLabel":"Nazwisko i imię"}</code>
					</td>
				</tr>
				<tr>
					<th>showParent</th>
					<td>
						Powoduje że nie będzie wyświetlana zakładka z zawartością główną (przydatne gdy chcemy umożliwić tylko wprowadzanie zawartości podrzędnych).
						<br><strong>możliwe wartości</strong>: true|false
						</br><strong>wartość domyślna</strong>: true
						<br><code>{"showParent":false}</code>
					</td>
				</tr>
				<tr>
					<th>showSystemChildField</th>
					<td>
						Powoduje wyświetlenie użytkownikowi o roli Edytor pola Nazwa systemowa w zawartościach podrzędnych, które może być edytowane.
						<br><strong>możliwe wartości</strong>: true|false
						</br><strong>wartość domyślna</strong>: false
						<br><code>{"showSystemChildField":true}</code>
					</td>
				</tr>
				<tr>
					<th>tabLabel</th>
					<td>
						Tekst zakładki dla zawartości głównej. Jeśli jest podany zastąpi domyślny "Zawartość".
						<br><strong>możliwe wartości</strong>: <i>string</i>|null
						</br><strong>wartość domyślna</strong>: "Zawartość"
						<br><code>{"tabLabel":"Miejsce konferencji"}</code>
					</td>
				</tr>
				<tr>
					<th>tabChildLabel</th>
					<td>
						Tekst zakładki dla zawartości podrzędnej. Jeśli jest podany zastąpi domyślny "Zawartość".
						<br><strong>możliwe wartości</strong>: <i>string</i>|null
						</br><strong>wartość domyślna</strong>: "Zawartość"
						<br><code>{"tabChildLabel":"Miejsce zakwaterowania"}</code>
					</td>
				</tr>
				<tr>
					<th>tabChildrenLabel</th>
					<td>
						Tekst zakładki dla elementów podrzędnych. Jeśli jest podany zastąpi domyślny "Elementy".
						<br><strong>możliwe wartości</strong>: <i>string</i>|null
						</br><strong>wartość domyślna</strong>: "Elementy"
						<br><code>{"tabChildrenLabel":"Miejsca zakwaterowania"}</code>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>