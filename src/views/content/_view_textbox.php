<?php

use yii\helpers\Html;

?>

<div class="form-group">
	<label class="control-label col-sm-3"><?= $property->getParam('label') ? $property->getParam('label') : Html::encode($property->feature->fld_name) ?></label>
	<div class="col-sm-9">
		<div class="form-control-static">
			<?= nl2br(Html::encode($model->getValueByFeatureId($property->fld_feature_id)['fld_value'])) ?>
		</div>
	</div>
</div>