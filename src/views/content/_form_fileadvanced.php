<?php

use datait\fractal\models\Settings;
use yii\helpers\Html;

?>

<div class="form-group" style="position: relative">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) . ' (nazwa pliku)' ?></label>
	<div class="col-sm-9">
		<?php
			$domainName = Settings::find()->one()->fld_domain;
			$fileName = Html::encode((strlen($model->getSubvalue('name', $property->fld_feature_id)) ? $model->getSubvalue('name', $property->fld_feature_id) : $model->getSubvalue('baseName', $property->fld_feature_id)) . '.' . $model->getSubvalue('extension', $property->fld_feature_id));
		?>

		<?= Html::textInput('Feature[' . $property->fld_feature_id . '][name]', $model->getSubvalue('name', $property->fld_feature_id), ['class' => 'form-control']) ?>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) . ' (title)' ?></label>
	<div class="col-sm-9">
		<?= Html::textInput('Feature[' . $property->fld_feature_id . '][title]', $model->getSubvalue('title', $property->fld_feature_id), ['class' => 'form-control']) ?>
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3"><?= Html::encode($property->feature->fld_name) ?></label>
	<div class="col-sm-9">
		<?php if ($model->getSubvalue('size', $property->fld_feature_id)): ?>
			<div class="form-control-static">
				<?= Html::a($fileName, $domainName . Yii::$app->params['filesPagePath'] . $fileName) ?>&nbsp;&nbsp;&nbsp;<?php if (Yii::$app->user->can('cms/content/update', ['content' => $model])): ?><span class="labels"><?= Html::a('Usuń plik', ['/value/delete', 'id' => $model->getValueByFeatureId($property->fld_feature_id)->fld_id], ['class' => 'btn btn-xxs btn-danger']) ?></span><?php endif ?>
			</div>
		<?php else: ?>
			<div class="form-control-static">
				<?= Html::fileInput('Feature[' . $property->fld_feature_id . '][file]') ?>
			</div>
		<?php endif ?>
	</div>
</div>