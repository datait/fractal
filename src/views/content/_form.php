<?php

use datait\fractal\models\Content;
use datait\fractal\models\LanguageVersion;
use datait\fractal\models\Property;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use datait\fractal\models\Worker;

/* @var $model Content */
/* @var $translations array */
?>
<div class="section section-search">
	<?php if (!$model->isNewRecord && Yii::$app->user->can('cms/content/activate', ['content' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Aktywuj', ['/cms/content/activate', 'id' => $model->fld_id], ['class' => 'btn btn-success']) ?>
		</div>
	<?php endif ?>
	<?php if (!$model->isNewRecord && Yii::$app->user->can('cms/content/deactivate', ['content' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Deaktywuj', ['/cms/content/deactivate', 'id' => $model->fld_id], ['class' => 'btn btn-warning']) ?>
		</div>
	<?php endif ?>
	<?php if (!$model->isNewRecord && Yii::$app->user->can('cms/content/update', ['content' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Wyżej', ['/cms/content/up', 'id' => $model->fld_id], ['class' => 'btn btn-default']) ?>
		</div>
	<?php endif ?>
	<?php if (!$model->isNewRecord && Yii::$app->user->can('cms/content/update', ['content' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Niżej', ['/cms/content/down', 'id' => $model->fld_id], ['class' => 'btn btn-default']) ?>
		</div>
	<?php endif ?>
	<?php if (!$model->isNewRecord && Yii::$app->user->can('cms/content/delete', ['content' => $model])): ?>
		<div class="btn-group pull-right">
			<?= Html::a('Usuń', ['/cms/content/delete', 'id' => $model->fld_id], ['class' => 'btn btn-danger']) ?>
		</div>
	<?php endif ?>
</div>

<div class="section">
	<?php $form = ActiveForm::begin([
		'id' => 'content-form',
		'layout' => 'horizontal',
		'options' => ['enctype' => 'multipart/form-data'],
		'fieldConfig' => [
			'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>',
            'horizontalCssClasses' => [
                'hint' => '',
            ],
		],
	]); ?>

		<h3>Informacje podstawowe</h3>
		<?php if ($model->getParam('description')): ?>
			<div class="bs-callout bs-callout-info"><?= $model->getParam('description') ?></div>
		<?php endif ?>
		<?php
			if ($model->getParam('nameFieldLabel')) {
				$nameLabel = $model->getParam('nameLabel');
			} elseif ($model->parent && $model->parent->getParam('nameFieldChildLabel')) {
				$nameLabel = $model->parent->getParam('nameFieldChildLabel');
			} else {
				$nameLabel = $model->getAttributeLabel('fld_name');
			}
		?>

		<?php if ($model->root->getParam('canEditNameField') === 'true'
            || in_array(Yii::$app->user->identity->ident->fld_type_id, [Worker::TYPE_PARTNER, Worker::TYPE_WEBMASTER], false)
            || ($model->fld_parent_id && $model->parent->getParam('canEditNameChildField') === 'true')): ?>
			<?= $form
                ->field($model, 'fld_name', ['options' => ['class' => 'form-group form-group-lg']])
                ->label($nameLabel)
                ->widget(LanguageVersion::class, [
                    'translations' => isset($translations) ? $translations : [],
                    'type' => 'name'
                ]) ?>
		<?php else: ?>
			<div class="form-group form-group-lg">
				<label class="control-label col-sm-3"><?= $nameLabel ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?php if (Yii::$app->user->can('cms/content/update', ['content' => $model])): ?>
							<strong><?= Html::a(Html::encode($model->fld_name), ['/cms/content/update', 'id' => $model->fld_id]) ?></strong>
						<?php else: ?>
							<strong><?= Html::a(Html::encode($model->fld_name), ['/cms/content/view', 'id' => $model->fld_id]) ?></strong>
						<?php endif ?>
					</div>
				</div>
			</div>
		<?php endif ?>

		<?php if (isset($model->fld_parent_id)): ?>
			<div class="form-group">
				<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_parent_id') ?></label>
				<div class="col-sm-9">
					<div class="form-control-static">
						<?php if (Yii::$app->user->can('cms/content/update', ['content' => $model->parent])): ?>
							<?= Html::a(Html::encode($model->parent->fld_name), ['/cms/content/update', 'id' => $model->fld_parent_id]) ?>
						<?php else: ?>
							<?= Html::a(Html::encode($model->parent->fld_name), ['/cms/content/view', 'id' => $model->fld_parent_id]) ?>
						<?php endif ?>
					</div>
				</div>
			</div>
		<?php endif ?>
		
		<?php if ($model->root->getParam('showSystemChildField') === 'true'
            || in_array(Yii::$app->user->identity->ident->fld_type_id, [Worker::TYPE_PARTNER, Worker::TYPE_WEBMASTER], false)): ?>
			<?= $form
                ->field($model, 'fld_system', ['options' => ['class' => 'form-group']])
                ->widget(LanguageVersion::class, [
                    'translations' => isset($translations) ? $translations : [],
                    'type' => 'system'
                ]) ?>
		<?php endif ?>

        <?php if ($model->getParent()): ?>
            <?= $form->field($model, 'fld_default_id')->checkbox(['value' => Content::DEFAULT_YES, 'uncheck' => null]) ?>
        <?php endif ?>

		<?php if (in_array(Yii::$app->user->identity->ident->fld_type_id, [Worker::TYPE_PARTNER, Worker::TYPE_WEBMASTER], false)): ?>
			<?= $form->field($model, 'fld_type', ['options' => ['class' => 'form-group']])->textInput() ?>
			<?= $form->field($model, 'fld_group', ['options' => ['class' => 'form-group']])->textInput() ?>

			<?= $form->field($model, 'fld_params', ['options' => ['class' => 'form-group']])->textArea(['style' => 'height: 114px']) ?>
			<?= $form->field($model, 'fld_sort_id')->checkbox(['value' => Content::SORT_DESC_YES, 'uncheck' => null]) ?>
		<?php endif ?>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_active_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<span class="label label-<?= $model->fld_active_id === 'yes' ? 'success' : 'warning' ?>" disabled="disabled">
                        <?= $model->fld_active_id === 'yes' ? 'aktywna' : 'nieaktywna' ?>
                    </span>
				</div>
			</div>
		</div>
			
		<?php if (in_array(Yii::$app->user->identity->ident->fld_type_id, [Worker::TYPE_PARTNER, Worker::TYPE_WEBMASTER], false)): ?>
			<?= $form
                ->field($model, 'fld_uri', ['options' => ['class' => 'form-group']])
                ->widget(LanguageVersion::class, [
                    'translations' => isset($translations) ? $translations : [],
                    'type' => 'uri'
                ]) ?>

			<?php if (!$model->fld_parent_id): ?>
				<?= $form
                    ->field(
                        $model,
                        'fld_icon',
                        [
                            'options' => [
                                'class' => 'form-group',
                            ],
                        ])
                    ->hint('Wpisz nazwę ikony z zestawu Font Awesome 4.4 bez przedrostka "fa-" (np: "wrench")')
                    ->textInput()
                ?>
			<?php endif ?>
		<?php endif ?>

		<?php if ($properties = $model->getProperties(Property::OWN_YES)): ?>
			<?php foreach ($properties as $property): ?>
				<?= !empty($property->getParam('header')) ? '<h4>' . $property->getParam('header') . '</h4>': '' ?>
				<?= !empty($property->getParam('description')) ? '<p>' . $property->getParam('description') . '</p>' : '' ?>
				<?= $this->render('_form_' . $property->feature->type['fld_id'], ['model' => $model, 'property' => $property]) ?>
			<?php endforeach ?>
		<?php endif ?>

		<?php if ($model->fld_parent_id && ($properties = $model->parent->getProperties(Property::OWN_NO))): ?>
			<?php foreach ($properties as $property): ?>
				<?= !empty($property->getParam('header')) ? '<h4>' . $property->getParam('header') . '</h4>': '' ?>
				<?= !empty($property->getParam('description')) ? '<p>' . $property->getParam('description') . '</p>' : '' ?>
				<?= $this->render('_form_' . $property->feature->type['fld_id'], ['model' => $model, 'property' => $property]) ?>
			<?php endforeach ?>
		<?php endif ?>

		<div class="bottom">
			<?= Html::submitButton($model->isNewRecord ? 'Utwórz' : 'Zapisz', ['class' => 'btn btn-success']) ?>
		</div>

	<?php ActiveForm::end(); ?>
</div>