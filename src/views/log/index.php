<?php

use datait\fractal\models\Log;
use yii\helpers\Html;
use yii\bootstrap\Tabs;

?>

<?php
	$this->title = 'Logi';
?>

<h1><?= Html::encode($this->title) ?></h1>

<?php
	$dataProvider = new \yii\data\ActiveDataProvider([
		'query' => Log::find()->joinWith('creator')->orderBy('fld_id DESC')
	]);

	$items[] = [
		'label' => 'Logi',
		'content' => $this->render('/log/_index', ['dataProvider' => $dataProvider]),
		'active' => !isset($active) || (isset($active) && $active == 'logs') ? true : false,
		'headerOptions' => ['class' => 'main'],
	];

	$items[] = [
		'label' => 'Podsumowanie',
		'content' => $this->render('/log/_summary', ['dataProvider' => $dataProvider]),
		'active' => isset($active) && $active == 'summary' ? true : false,
		'headerOptions' => ['class' => 'pull-right'],
	];

	echo Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>