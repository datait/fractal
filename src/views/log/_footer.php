<?php

use yii\helpers\Html;

?>

<div class="section">
	<div class="logs">
		<hr>
		<p>Utworzony <strong><?= \Yii::$app->formatter->asDate($model->fld_created_at, 'yyyy-MM-dd (HH:mm:ss)') ?></strong><?php if ($model->fld_creator_id): ?> przez <strong><?= Html::encode($model->creator->name) ?></strong><?php endif ?></p>
		<?php if ($model->fld_updated_at): ?>
			<p>Zmodyfikowany <strong><?= \Yii::$app->formatter->asDate($model->fld_updated_at, 'yyyy-MM-dd (HH:mm:ss)') ?></strong><?php if ($model->fld_updater_id): ?> przez <strong><?= Html::encode($model->updater->name) ?></strong><?php endif ?></p>
		<?php endif ?>
	</div>
</div>