<?php

use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="section">
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => '{items}<div class="row"><div class="col-lg-6">{summary}</div><div class="col-lg-6">{pager}</div></div>',
		'tableOptions' => [
			'class' => 'table table-hover'
		],
		'pager' => [
			'options' => [
				'class' => 'pagination pull-right'
			]
		],
		'columns' => [
			[
				'label' => 'Nazwa akcji',
				'value' => function($data) {
					$main = '<strong>' . Html::encode($data->fld_action_name) . '</strong>';
					$adds =  strlen($data->fld_comment) ? '<br><small>' . Html::encode($data->fld_comment) . '</small>' : '';

					return  $main . $adds;
				},
				'format' => 'html',
				'headerOptions' => [
					'class' => 'col-main'
				],
				'contentOptions' => [
					'class' => 'col-main'
				],
			],
			[
				'label' => 'Obiekt',
				'value' => function($data) {
					$main = '';
					if (isset($data->fld_object_id)) {
						if (\Yii::$app->user->can('cms/' . $data->kind['fld_view'] . '/view', [$data->kind['fld_view'] . '_id' => $data->fld_object_id])) {
							$main =  Html::a($data->kind['fld_name'], [$data->kind['fld_view'], 'id' => $data->fld_object_id]);
						} else {
							$main =  $data->kind['fld_name'];
						}
					}

					return $main;
				},
				'format' => 'html',
				'headerOptions' => [
					'class' => 'col-sm'
				],
				'contentOptions' => [
					'class' => 'col-sm'
				],
			],
			[
				'label' => 'Data i czas',
				'value' => function($data) {
					$main = Yii::$app->formatter->asDate($data->fld_created_at, 'yyyy-MM-dd');
					$adds = '<br><small>' . Yii::$app->formatter->asTime($data->fld_created_at, 'HH:mm:ss') . '</small>';

					return $main . $adds;
				},
				'format' => 'html',
				'headerOptions' => [
					'class' => 'col-sm text-right'
				],
				'contentOptions' => [
					'class' => 'col-sm text-right'
				],
			],
			[
				'label' => 'Użytkownik',
				'value' => function($data) {
					$main = '';
					if (isset($data->fld_creator_id)) {
						if (isset($data->creator->fld_worker_id) && \Yii::$app->user->can('cms/worker/view', ['worker_id' => $data->creator->fld_worker_id])) {
							$main = Html::a(Html::encode($data->creator->name), ['/cms/worker/view', 'id' => $data->creator->fld_worker_id]);
						} else {
							$main = Html::encode($data->creator->name);
						}
					}

					return $main;
				},
				'format' => 'html',
				'headerOptions' => [
					'class' => 'col-md text-right'
				],
				'contentOptions' => [
					'class' => 'col-md text-right'
				],
			],
		],
	]); ?>
</div>