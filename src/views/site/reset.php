<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php
	$this->title = 'Zapomniane hasło';
?>

<div class="site-login">
	<h1><?= Html::encode($this->title) ?></h1>

	<p>Wprowadź login w celu uzyskania hasła</p>

	<?php $form = ActiveForm::begin([
		'id' => 'login-form',
		'options' => ['class' => 'form-horizontal'],
		'fieldConfig' => [
			'template' => "{label}<div class='col-md-9'>{input} {error}</div>",
			'labelOptions' => ['class' => 'col-md-3 control-label'],
		],
	]); ?>

	<?= $form->field($model, 'username', ['options' => ['class' => 'form-group form-group-lg']]) ?>

	<div class="form-group">
		<div class="col-lg-offset-3 col-lg-9">
			<?= Html::submitButton('Prześlij hasło', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>
