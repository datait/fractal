<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Logowanie';
?>
<div class="site-login">
	<h1><?= Html::encode($this->title) ?></h1>

	<p>Wypełnij poniższe pola w celu zalogowania</p>

	<?php $form = ActiveForm::begin([
		'id' => 'login-form',
		'options' => ['class' => 'form-horizontal'],
		'fieldConfig' => [
			'template' => "{label}<div class='col-md-9'>{input} {error}</div>",
			'labelOptions' => ['class' => 'col-md-3 control-label'],
		],
	]); ?>

	<?= $form->field($model, 'username', ['options' => ['class' => 'form-group form-group-lg']]) ?>
	<?= $form->field($model, 'password')->passwordInput() ?>

	<div class="form-group">
		<div class="col-md-offset-3 col-md-9">
			<?= Html::submitButton('Zaloguj', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>

</div>

<div class="row row-policy">
	<div class="col-md-offset-3 col-md-9">
		<p>Administrator przypomina, że użytkownicy systemu są zobowiązani do zabezpieczenia nazwy użytkownika i hasła służących do uzyskania dostępu do systemu i nie udostępniania poufnych danych osobom niepowołanym.</p><p>Próby nieautoryzowanego dostępu do systemu są zabronione i traktowane będą jako naruszenie obowiązujących norm prawnych.</p>
	</div>
</div>