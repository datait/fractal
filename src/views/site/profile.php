<?php

use datait\fractal\models\Account;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $user Account */
/* @var $changed ?bool */

?>

<?php
    $this->title = 'Hasło konta';
?>
<h1><?= Html::encode($user->name) ?></h1>
<?php if ($changed !== null): ?>
    <div class="alert alert-<?= $changed ? 'success' : 'danger' ?>" role="alert">
        <?= $changed ? 'Hasło zostało zmienione' : 'Wystąpił błąd podczas zmiany hasła' ?>
    </div>
<?php endif; ?>
<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
    <?= $form->field($user, 'new_pass')->passwordInput() ?>
    <?= $form->field($user, 'repeat_pass')->passwordInput() ?>
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <?= Html::submitButton('Zapisz', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>