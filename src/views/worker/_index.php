<?php

use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="section">
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => '{items}<div class="row"><div class="col-lg-6">{summary}</div><div class="col-lg-6">{pager}</div></div>',
		'tableOptions' => [
			'class' => 'table table-hover'
		],
		'pager' => [
			'options' => [
				'class' => 'pagination pull-right'
			]
		],
		'columns' => [
			[
				'label' => 'Nazwisko i imię',
				'value' => function($data) {
					$header = '<span class="header">' . $data->type['fld_name'] . '</span>';
					$main = '<br><strong>' . Html::a(Html::encode($data->name) . (strlen($data->name) == 0 ? ' <span class="none">(brak)</span>' : ''), ['/cms/worker/view', 'id' => $data->fld_id]) . '</strong> <span class="labels"><span class="btn btn-xxs btn-' . $data->status['fld_class'] . '" disabled="disabled">' . $data->status['fld_name'] . '</span></span>';
					$adds = isset($data->fld_patron_id) ? '<br><small>Opiekun: <strong>' . Html::a(Html::encode($data->patron->name), ['/cms/customer/view', 'id' => $data->fld_patron_id]) . '</strong></small>' : '';

					return $header . $main . $adds;
				},
				'format' => 'html',
				'headerOptions' => [
					'class' => 'col-main'
				],
				'contentOptions' => [
					'class' => 'col-main'
				],
			],
		],
	]); ?>
</div>