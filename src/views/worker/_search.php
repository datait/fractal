<?php

use datait\fractal\search\WorkerSearch;
use yii\bootstrap\ButtonDropdown;
use datait\fractal\models\Worker;

?>

<div class="section section-search">
	<?php if (Yii::$app->user->can('cms/worker/create')): ?>
	    <?= ButtonDropdown::widget([
            'label' => 'Utwórz pracownika',
            'dropdown' => [
                'items' => Worker::getTypesButtonDropdown(),
            ],
            'options' => [
                'class' => 'btn btn-primary'
            ],
        ]) ?>
	<?php endif ?>

	<?= ButtonDropdown::widget([
        'label' => isset($searchModel->type) ? Worker::getTypes()[$searchModel->type]['fld_name'] : '(bez ogr.)',
        'dropdown' => [
            'items' => WorkerSearch::getTypeFilter('/cms/worker/index', $searchModel->type),
        ],
        'options' => [
            'class' => 'btn btn-default'
        ],
    ]) ?>
</div>