<?php

use yii\helpers\Html;

?>

<div class="section section-search">
	<?php if (Yii::$app->user->can('cms/worker/update', ['worker' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Edytuj', ['/cms/worker/update', 'id' => $model->fld_id], ['class' => 'btn btn-primary']) ?>
		</div>
	<?php endif ?>
	<?php if (Yii::$app->user->can('cms/worker/activate', ['worker' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Aktywuj', ['/cms/worker/activate', 'id' => $model->fld_id], ['class' => 'btn btn-success']) ?>
		</div>
	<?php endif ?>
	<?php if (Yii::$app->user->can('cms/worker/deactivate', ['worker' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Deaktywuj', ['/cms/worker/deactivate', 'id' => $model->fld_id], ['class' => 'btn btn-danger']) ?>
		</div>
	<?php endif ?>
    <?php if (Yii::$app->user->can('cms/worker/delete', ['worker' => $model])): ?>
        <div class="btn-group pull-right">
            <?= Html::a('Usuń', ['/cms/worker/delete', 'id' => $model->fld_id], ['class' => 'btn btn-danger']) ?>
        </div>
    <?php endif ?>
</div>

<div class="section">
	<div class="form-horizontal">
		<h3>Informacje podstawowe</h3>
		<div class="form-group form-group-lg">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_full_name') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<strong><?= Html::a(Html::encode($model->name), ['/cms/worker/view', 'id' => $model->fld_id]) ?></strong>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_email') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::a(Html::encode($model->fld_email), ['mailto:' . $model->fld_email]) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_mobile') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_mobile) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_status_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<span class="label label-<?= $model->status['fld_class'] ?>" disabled="disabled"><?= $model->status['fld_name'] ?></span>
				</div>
			</div>
		</div>

		<h3>Dane do logowania</h3>
		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->account->getAttributeLabel('fld_login') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->account->fld_login) ?>
				</div>
			</div>
		</div>

		<h3>Informacje dodatkowe</h3>
		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_description') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::encode($model->fld_description) ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?= $this->render('/log/_footer', ['model' => $model]) ?>