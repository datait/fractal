<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

?>

<?php
	$this->title = 'Edycja pracownika #' . $model->fld_id . ': ' . $model->name;
?>

<div class="row row-title">
	<div class="col col-main">
		<div class="labels-top">
			<span class="btn btn-xxs btn-<?= $model->type['fld_class'] ?>" disabled="disabled"><?= $model->type['fld_name'] ?></span>
			<span class="btn btn-xxs btn-<?= $model->status['fld_class'] ?>" disabled="disabled"><?= $model->status['fld_name'] ?></span>
		</div>
		<h1><?= Html::a($this->title, ['/cms/worker/view', 'id' => $model->fld_id]) ?></h1>
	</div>
</div>

<?php
	$items[] = [
		'label' => 'Pracownik',
		'content' => $this->render('/worker/_form', ['model' => $model, 'account' => $account, 'view' => '/cms/worker/update', 'params' => ['id' => $model->fld_id]]),
		'active' => !isset($active) || (isset($active) && $active == 'worker') ? true : false,
		'headerOptions' => ['class' => 'main'],
	];

	echo Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>