<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="section">
	<?php $form = ActiveForm::begin([
		'id' => 'worker-form',
		'layout' => 'horizontal',
		'fieldConfig' => [
			'template' => '{label}<div class="col-sm-9">{input} {error}</div>'
		],
	]); ?>

		<h3>Informacje podstawowe</h3>
		<p>Wprowadź podstawowe dane osobowe <code>Nazwisko i Imię</code> oraz dane kontaktowe <code>E-mail</code>. Bezpieczny adres e-mail jest niezbędny gdyż będzie używany do różnych celów, np. do odzyskania hasła w przypadku jego utraty oraz do wysyłania powiadomień z systemu.</p>
		<?= $form->field($model, 'fld_last_name', ['options' => ['class' => 'form-group form-group-lg']])->textInput() ?>
		<?= $form->field($model, 'fld_first_name', ['options' => ['class' => 'form-group form-group-lg']])->textInput() ?>
		<?= $form->field($model, 'fld_email')->textInput() ?>
		<?= $form->field($model, 'fld_mobile')->textInput() ?>

		<h3>Dane do logowania</h3>
		<p>Wypełniając poniższe pola <code>Login i Hasło</code> umożliwiasz tej osobie logowanie się do systemu z uprawnieniami roli <?= $model->type['fld_name'] ?>. Pamiętaj aby hasło miało co najmniej 8 znaków oraz zawierało małe i duże litery, cyfry oraz przynajmniej jeden znak specjalny np. <code>!@#$%^&*</code>. Logować mogą się tylko osoby mające <code>Status: Aktywne</code>.</p>
		<p>Jeżeli nie chcesz zmieniać aktualnego hasła - pozostaw pole <code>Hasło</code> puste.</p>
		<?= $form->field($account, 'fld_login')->textInput() ?>
		<?= $form->field($account, 'fld_password')->passwordInput() ?>

		<h3>Informacje dodatkowe</h3>
		<?= $form->field($model, 'fld_description')->textarea() ?>

		<div class="bottom">
			<?= Html::submitButton($model->isNewRecord ? 'Utwórz' : 'Zapisz', ['class' => 'btn btn-success']) ?>
		</div>

	<?php ActiveForm::end(); ?>
</div>