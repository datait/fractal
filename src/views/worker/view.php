<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

?>

<?php
	$this->title = 'Pracownik #' . $model->fld_id . ': ' . Html::encode($model->name);
?>

<div class="row row-title">
	<div class="col col-main">
		<div class="labels-top">
			<span class="btn btn-xxs btn-<?= $model->type['fld_class'] ?>" disabled="disabled"><?= $model->type['fld_name'] ?></span>
			<span class="btn btn-xxs btn-<?= $model->status['fld_class'] ?>" disabled="disabled"><?= $model->status['fld_name'] ?></span>
		</div>
		<h1><?= Html::a($this->title, ['/cms/worker/view', 'id' => $model->fld_id]) ?></h1>
	</div>
</div>

<?php
	$items[] = [
		'label' => 'Pracownik',
		'content' => $this->render('/worker/_view', ['model' => $model, 'view' => '/cms/worker/view', 'params' => ['id' => $model->fld_id]]),
		'active' => !isset($active) || (isset($active) && $active == 'worker') ? true : false,
		'headerOptions' => ['class' => 'main'],
	];

	if (Yii::$app->user->can('cms/log/index')) {
		$items[] = [
			'label' => 'Logi',
			'content' => $this->render('/worker/_logs', ['model' => $model, 'view' => '/cms/worker/view', 'params' => ['id' => $model->fld_id]]),
			'active' => isset($active) && $active == 'logs' ? true : false,
			'headerOptions' => ['class' => 'pull-right'],
		];
	}

	echo Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>