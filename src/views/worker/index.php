<?php

use datait\fractal\search\WorkerSearch;
use yii\helpers\Html;

?>

<h1><?= Html::encode($this->title = 'Pracownicy') ?></h1>

<?php
	$searchModel = new WorkerSearch;
	$searchModel->view = 'workers';

	$searchModel->saveFilters(Yii::$app->request->getQueryParams());
	$searchModel->loadFilters();
	$dataProvider = $searchModel->search();

	$items[] = [
		'label' => 'Pracownicy',
		'content' => $this->render('/worker/_workers', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]),
		'active' => !isset($active) || (isset($active) && $active == 'workers') ? true : false,
		'headerOptions' => ['class' => 'main'],
	];

	if (Yii::$app->user->can('cms/worker/summary')) {
		$items[] = [
			'label' => 'Podsumowanie',
			'content' => $this->render('/worker/_summary', ['dataProvider' => $dataProvider]),
			'active' => isset($active) && $active == 'summary' ? true : false,
			'headerOptions' => ['class' => 'pull-right'],
		];
	}

	echo \yii\bootstrap\Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>