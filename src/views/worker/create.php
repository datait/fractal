<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

?>

<?php
	$this->title = 'Tworzenie pracownika';
?>

<div class="row row-title">
	<div class="col col-main">
		<div class="labels-top">
			<span class="btn btn-xxs btn-<?= $model->type['fld_class'] ?>"><?= $model->type['fld_name'] ?></span>
			<span class="btn btn-xxs btn-<?= $model->status['fld_class'] ?>"><?= $model->status['fld_name'] ?></span>
		</div>
		<h1><?= Html::encode($this->title) ?></h1>
	</div>
</div>

<?php
	$items[] = [
		'label' => 'Pracownik',
		'content' => $this->render('/worker/_form', ['model' => $model, 'account' => $account, 'view' => '/worker/view']),
		'active' => !isset($active) || (isset($active) && $active == 'worker') ? true : false,
		'headerOptions' => ['class' => 'main'],
	];

	echo Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>