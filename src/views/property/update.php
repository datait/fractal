<?php

use yii\helpers\Html;

?>

<?php
    $this->title = 'Właściwość #' . $model->fld_id . ': ' . Html::encode($model->feature->fld_name);
?>

<div class="row row-title">
	<div class="col col-main">
		<h1><?= Html::a($this->title, ['/cms/property/view', 'id' => $model->fld_id]) ?></h1>
	</div>
</div>

<?php
	$items[] = [
		'label' => 'Zawartość',
		'content' => $this->render('/content/_view', ['model' => $model->content, 'view' => '/property/update']),
		'active' => isset($active) && $active == 'content',
	];

	$items[] = [
		'label' => 'Elementy',
		'content' => $this->render('/content/_contents', ['model' => $model->content, 'view' => '/cms/property/update', 'params' => ['id' => $model->fld_id]]),
		'active' => isset($active) && $active == 'contents',
	];

	$items[] = [
		'label' => 'Pomoc',
		'content' => $this->render('/property/_help'),
		'active' => isset($active) && $active == 'help',
		'headerOptions' => ['class' => 'pull-right'],
	];

	$items[] = [
		'label' => 'Właściwość',
		'content' => $this->render('/property/_form', ['model' => $model, 'view' => '/cms/property/update']),
		'active' => !isset($active) || (isset($active) && $active == 'property'),
		'headerOptions' => ['class' => 'main pull-right'],
	];

	echo \yii\bootstrap\Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>