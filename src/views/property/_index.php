<?php

use yii\helpers\Html;

?>

<div class="section">
	<?= \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => '{items}<div class="row"><div class="col-lg-6">{summary}</div><div class="col-lg-6">{pager}</div></div>',
		'tableOptions' => [
			'class' => 'table table-hover'
		],
		'pager' => [
			'options' => [
				'class' => 'pagination pull-right'
			]
		],
		'columns' => [
			[
				'label' => 'Nazwa',
				'value' => function($data) {
					if (\Yii::$app->user->can('cms/property/update', ['property' => $data])) {
						$main = '<strong>' . Html::a(Html::encode($data->feature->fld_name) . (strlen($data->feature->fld_name) == 0 ? ' <span class="none">(brak)</span>' : ''), ['/cms/property/update', 'id' => $data->fld_id]) . '</strong> <span class="labels"><span class="btn btn-xxs btn-' . $data->public['fld_class'] . '" disabled="disabled">' . $data->public['fld_name'] . '</span> <span class="btn btn-xxs btn-' . $data->own['fld_class'] . '" disabled="disabled">' . $data->own['fld_name'] . '</span></span>';
					} else {
						$main = '<strong>' . Html::a(Html::encode($data->feature->fld_name) . (strlen($data->feature->fld_name) == 0 ? ' <span class="none">(brak)</span>' : ''), ['/cms/property/view', 'id' => $data->fld_id]) . '</strong> <span class="labels"><span class="btn btn-xxs btn-' . $data->public['fld_class'] . '" disabled="disabled">' . $data->public['fld_name'] . '</span> <span class="btn btn-xxs btn-' . $data->own['fld_class'] . '" disabled="disabled">' . $data->own['fld_name'] . '</span></span>';
					}
					
					$adds = '<strong>' . Html::encode($data->feature->fld_id) . '</strong>';

					return $main . (strlen($adds) ? '<br><small>' . $adds . '</small>' : '');
				},
				'format' => 'html',
				'headerOptions' => [
					'class' => 'col-main'
				],
				'contentOptions' => [
					'class' => 'col-main'
				],
			],
			[
				'value' => function($data, $key, $index, $column) {
					if (\Yii::$app->user->can('cms/property/update')) {
						if ($index > 0) {
							$up = Html::a('<i class="fa fa-fw fa-caret-up"></i>', ['/cms/property/up', 'id' => $data->fld_id]);
						} else {
							$up = '<i class="fa fa-fw"></i>';
						}

						if ($index < $column->grid->dataProvider->totalCount - 1) {
							$down = Html::a('<i class="fa fa-fw fa-caret-down"></i>', ['/cms/property/down', 'id' => $data->fld_id]);
						} else {
							$down = '<i class="fa fa-fw"></i>';
						}

						return '<span class="sort sort-up">' . $up . '</span><span class="sort sort-down">' . $down . '</span>';
					} else {
						return '<span class="sort sort-up"><i class="fa fa-fw"></i></span><span class="sort sort-down"><i class="fa fa-fw"></i></span>';
					}
				},
				'format' => 'html',
				'contentOptions' => [
					'class' => 'col-xs text-center',
					'style' => 'vertical-align: middle'
				],
			],
			[
				'value' => function($data) {
					if (\Yii::$app->user->can('cms/property/delete', ['property' => $data])) {
						return '<span class="delete">' . Html::a('<i class="fa fa-fw fa-times text-danger"></i>', ['/cms/property/delete', 'id' => $data->fld_id]) . '</span>';
					} else {
						return '<span class="delete"><i class="fa fa-fw"></i></span>';
					}
				},
				'format' => 'html',
				'contentOptions' => [
					'class' => 'col-xxs text-center',
					'style' => 'vertical-align: middle'
				],
			],
		],
	]); ?>
</div>