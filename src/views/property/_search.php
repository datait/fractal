<?php

use datait\fractal\models\Property;
use yii\helpers\Html;

?>

<div class="section section-search">
	<?php if (Yii::$app->user->can('cms/property/create')): ?>
        <div class="btn-group">
            <?= Html::a('Utwórz właściwość własną', ['/cms/property/create', 'content_id' => $model->fld_id, 'own_id' => Property::OWN_YES], ['class' => 'btn btn-primary']) ?>
        </div>
    <?php endif ?>
	<?php if (Yii::$app->user->can('cms/property/create')): ?>
		<div class="btn-group">
			<?= Html::a('Utwórz właściwość dziedziczoną', ['/cms/property/create', 'content_id' => $model->fld_id, 'own_id' => Property::OWN_NO], ['class' => 'btn btn-primary']) ?>
		</div>
	<?php endif ?>
</div>