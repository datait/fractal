<div class="section">
	<div class="form-horizontal">
		<h3>Parametry</h3>
		<table class="table">
			<thead>
				<tr>
					<th class="col col-md">Parametr</th>
					<th>Opis</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>default</th>
					<td>
						Domyślna wartość właściwości automatycznie wstawiana przy tworzeniu nowej właściwości.
						<br><code>{"default":"Wyrażam zgodę na przetwarzanie danych osobowych."}</code>
					</td>
				</tr>
				<tr>
					<th>description</th>
					<td>
						Tekst jaki pokaże się pod nagłówkiem a nad polem formularza. Z reguły pełni funkcję pomocniczą, sugerując użytkownikowi jakie dane są oczekiwane w polu formularza.
						<br><code>{"description":"Wprowadź tekst wstępny aby zachęcić odwiedzającego do rejestracji."}</code>
					</td>
				</tr>
				<tr>
					<th>header</th>
					<td>
						Tekst nagłówkowy jaki pokaże się nad polem formularza.
						<br><code>{"header":"Tytuł konferencji"}</code>
					</td>
				</tr>
				<tr>
					<th>label</th>
					<td>
						Tekst etykiety przy polu formularza. Jeśli jest podany zastąpi tekst, który wynika z cechy.
						<br><code>{"label":"Wprowadź treść"}</code>
					</td>
				</tr>
			</tbody>
		</table>

		<h3>Parametry specyficzne dla poszczególnych typów pól</h3>
		<h4>Lista dropdown</h4>
		<table class="table">
			<thead>
				<tr>
					<th class="col col-md">Parametr</th>
					<th>Opis</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>ifSelect</th>
					<td>
						Uruchom funkcję podaną jako argument gdy zostanie wybrany ten obiekt. Funkcje należy wybrać z predefiniowanych lub stworzyć własne (we własnej przestrzeni nazw).
						<br><code>{"ifSelect",{"set":{"used":"yes"}}}</code>
					</td>
				</tr>
				<tr>
					<th>ifUnselect</th>
					<td>
						Uruchom funkcję podaną jako argument gdy zostanie wybrany ten obiekt. Funkcje należy wybrać z predefiniowanych lub stworzyć własne (we własnej przestrzeni nazw).
						<br><code>{"ifUnselect",{"set":{"used":"no"}}}</code>
					</td>
				</tr>
			</tbody>
		</table>
		<h5>Predefiniowane funkcje</h5>
		<ul>
			<li>set - ustawia podany jako argument atrybut obiektu na wskazaną wartość.</li>
		</ul>
	</div>
</div>