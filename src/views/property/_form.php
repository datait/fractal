<?php

use datait\fractal\models\Property;
use yii\helpers\Html;

?>

<div class="section section-search text-right">
	<?php if (!$model->isNewRecord && \Yii::$app->user->can('cms/property/delete', ['property' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Usuń', ['/cms/property/delete', 'id' => $model->fld_id], ['class' => 'btn btn-danger']) ?>
		</div>
	<?php endif ?>
</div>

<div class="section">
	<?php $form = \yii\bootstrap\ActiveForm::begin([
		'id' => 'content-form',
		'layout' => 'horizontal',
		'fieldConfig' => [
			'template' => '{label}<div class="col-sm-9">{input} {error}</div>'
		],
	]); ?>

		<h3>Informacje podstawowe</h3>
		<?= $form->field($model, 'fld_feature_id')->dropDownList($model->getFeaturesDropDown()) ?>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_content_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::a(Html::encode($model->content->fld_name), ['/cms/content/view', 'id' => $model->fld_content_id]) ?>
				</div>
			</div>
		</div>
		
		<?= $form->field($model, 'fld_public_id')->checkbox(['value' => Property::PUBLIC_YES, 'uncheck' => null]) ?>
		<?= $form->field($model, 'fld_params')->textArea() ?>

		<div class="bottom">
			<?= Html::submitButton($model->isNewRecord ? 'Utwórz' : 'Zapisz', ['class' => 'btn btn-success']) ?>
		</div>

	<?php \yii\bootstrap\ActiveForm::end(); ?>
</div>