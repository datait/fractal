<?php

use yii\helpers\Html;

?>

<div class="section section-search">
	<?php if (Yii::$app->user->can('cms/property/update', ['property' => $model])): ?>
		<div class="btn-group">
			<?= Html::a('Edytuj', ['/cms/property/update', 'id' => $model->fld_id], ['class' => 'btn btn-primary']) ?>
		</div>
	<?php endif ?>
	<?php if (Yii::$app->user->can('cms/property/delete', ['feature' => $model])): ?>
		<div class="btn-group pull-right">
			<?= Html::a('Usuń', ['/cms/property/delete', 'id' => $model->fld_id], ['class' => 'btn btn-danger']) ?>
		</div>
	<?php endif ?>
</div>

<div class="section">
	<div class="form-horizontal">
		<h3>Informacje podstawowe</h3>
		<div class="form-group form-group-lg">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_feature_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<strong><?= Html::a(Html::encode($model->feature->fld_name), ['/cms/feature/view', 'id' => $model->fld_feature_id]) ?></strong>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_content_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<?= Html::a(Html::encode($model->content->fld_name), ['/cms/content/view', 'id' => $model->fld_content_id]) ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_public_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<span class="label label-<?= $model->public['fld_class'] ?>" disabled="disabled"><?= $model->public['fld_name'] ?></span>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-3"><?= $model->getAttributeLabel('fld_own_id') ?></label>
			<div class="col-sm-9">
				<div class="form-control-static">
					<span class="label label-<?= $model->own['fld_class'] ?>" disabled="disabled"><?= $model->own['fld_name'] ?></span>
				</div>
			</div>
		</div>
	</div>
</div>