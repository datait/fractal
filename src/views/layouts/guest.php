<?php

use datait\fractal\assets\FractalAppAsset;
use yii\helpers\Html;

FractalAppAsset::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
	</head>
	<body>
		<?php $this->beginBody() ?>
			<div class="wrap">
				<div class="container container-guest">
					<div class="row">
						<div class="col-md-offset-3 col-md-6 col-xs-offset-2 col-xs-8 col-content">
							<?= $content ?>
						</div>
					</div>
				</div>
			</div>
		<?php $this->endBody() ?>
	</body>
</html>

<?php $this->endPage() ?>
