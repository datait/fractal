<?php

use app\models\Content;
use yii\helpers\Html;
use yii\bootstrap\ButtonGroup;

$groups = Content::find()->where(['fld_parent_id' => null])->orderBy('fld_group')->groupBy('fld_group')->all();
?>
<div id="side-menu">
	<?php if (Yii::$app->user->can('cms/dashboard/index')): ?>
		<ul class="list-group">
			<li class="list-group-item">
				<?= ButtonGroup::widget([
                    'buttons' => [
                        Html::a(
                            '<i class="fa fa-fw fa-desktop"></i> Pulpit',
                            ['/cms/dashboard'],
                            ['class' => 'btn' . (Yii::$app->controller->id === 'dashboard' ? ' active' : '')]
                        )
                    ]
                ]) ?>
			</li>
		</ul>
	<?php endif; ?>

	<?php foreach ($groups as $group): ?>
		<?php if ($items = Content::find()->where(['fld_parent_id' => null, 'fld_group' => $group->fld_group])->orderBy('fld_sort')->all()): ?>
			<ul class="list-group">
				<?php if (Yii::$app->controller->id === 'content') {
                    if (Yii::$app->controller->action->id === 'create' && Yii::$app->request->getQueryParam('parent_id')) {
                        $content = Content::findOne(Yii::$app->request->getQueryParam('parent_id'));
                    } elseif (Yii::$app->request->getQueryParam('id')) {
                        $content = Content::findOne(Yii::$app->request->getQueryParam('id'));
                    }
                } ?>
				<?php foreach ($items as $item): ?>
					<?php if (Yii::$app->user->can('cms/content/view', ['content' => $item])): ?>
						<li class="list-group-item">
                            <?= ButtonGroup::widget([
                                'buttons' => [
                                    Html::a(
                                        '<i class="fa fa-fw fa-' . $item->fld_icon . '"></i> ' . $item->getParam('menuLabel', $item->fld_name),
                                        [
                                            '/cms/content/' . (Yii::$app->user->can('cms/content/update', ['content' => $item]) ? 'update' : 'view'),
                                            'id' => $item->fld_id
                                        ],
                                        ['class' => 'btn' . (Yii::$app->controller->id === 'content' && isset($content) && $content->root->fld_id == $item->fld_id ? ' active' : '')]
                                    )
                                ]
                            ]) ?>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	<?php endforeach; ?>

	<?php if (Yii::$app->user->can('cms/feature/index')): ?>
		<ul class="list-group">
			<li class="list-group-item">
				<?= ButtonGroup::widget([
                    'buttons' => [
                        Html::a(
                            '<i class="fa fa-fw fa-ellipsis-v"></i> Cechy',
                            ['/cms/feature'],
                            ['class' => 'btn' . (Yii::$app->controller->id === 'feature' ? ' active' : '')]
                        )
                    ]
                ]) ?>
			</li>
		</ul>

		<?php
			$items = [
				['button' => Html::a('<i class="fa fa-fw fa-user"></i> Pracownicy', ['/cms/worker'], ['class' => 'btn' . (Yii::$app->controller->id === 'worker' ? ' active' : '')])],
				['button' => Html::a('<i class="fa fa-fw fa-code"></i> Logi', ['/cms/log'], ['class' => 'btn' . (Yii::$app->controller->id === 'log' ? ' active' : '')])],
				['button' => Html::a('<i class="fa fa-fw fa-wrench"></i> Ustawienia', ['/cms/settings'], ['class' => 'btn' . (Yii::$app->controller->id === 'settings' ? ' active' : '')])],
			];
		?>
		<ul class="list-group">
			<?php foreach ($items as $item): ?>
				<li class="list-group-item">
					<?=	ButtonGroup::widget(['buttons' => [$item['button']]]) ?>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</div>
