<?php

use datait\fractal\assets\FractalAppAsset;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;

FractalAppAsset::register($this);

/**
 * @var string $content
 */

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
	</head>
	<body>

	<?php $this->beginBody(); ?>
		<div class="wrap">
			<?php
				NavBar::begin([
					'brandLabel' => Yii::$app->params['name'],
					'brandUrl' => Yii::$app->homeUrl,
					'options' => [
						'class' => 'navbar-inverse navbar-fixed-top',
					],
				]);

				echo Nav::widget([
					'options' => ['class' => 'navbar-nav navbar-right'],
					'items' => [
						'<li class="navbar-text">Zalogowany <a href="'
                        . Url::to(['site/profile'])
                        . '" class="btn btn-info" style="display:inline;padding:2px 5px">'
                        . Yii::$app->user->identity->name
                        . '</a> ('
                        . Yii::$app->user->identity->ident->type['fld_name']
                        . ')</li>',
						[
							'label' => 'Wyloguj',
							'url' => ['/cms/user/logout'], 'linkOptions' => ['data-method' => 'post']
						],
					],
				]);

				NavBar::end();
			?>

			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-menu">
						<?= $this->render('/layouts/_menu') ?>
					</div>
					<div class="col-md-9 col-sm-8 col-content">
						<?= $content ?>
					</div>
				</div>
				<?php foreach (Yii::$app->session->getAllFlashes(true) as $key => $message): ?>
					<?= Alert::widget([
						'options' => [
							'class' => 'alert-' . $key
						],
						'body' => $message,
					]) ?>
				<?php endforeach ?>
			</div>
		</div>

	<?php $this->endBody() ?>
	</body>
</html>

<?php $this->endPage() ?>
