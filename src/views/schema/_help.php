<div class="section">
	<h3>Parametry</h3>
	<table class="table">
		<tr>
			<th>PARAMETR</th>
			<th>OPIS</th>
		</tr>
		<tr>
			<td>showParent</td>
			<td>true|false (default: true) Określa widoczność rodzica<br>
				{"showParent":"true"}
			</td>
		</tr>
		<tr>
			<td>contentParams</td>
			<td>mixed Parametry contentu<br>
				{"params":{"templateOwn":"page-aktualnosc","createButtonLabel":"Utwórz nową aktualność"}}
			</td>
	</table>
</div>