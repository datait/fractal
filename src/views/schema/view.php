<?php

use yii\helpers\Html;

?>

<?php
    $this->title = 'Cecha #' . $model->fld_id . ': ' . Html::encode($model->fld_name);
?>

<div class="row row-title">
	<div class="col col-main">
		<h1><?= Html::a($this->title, ['/cms/feature/view', 'id' => $model->fld_id]) ?></h1>
	</div>
</div>

<?php
	$items[] = [
		'label' => 'Cecha',
		'content' => $this->render('/feature/_view', ['model' => $model, 'view' => '/cms/feature/view', 'params' => ['id' => $model->fld_id]]),
		'active' => !isset($active) || (isset($active) && $active == 'feature'),
		'headerOptions' => ['class' => 'main'],
	];

	echo \yii\bootstrap\Tabs::widget([
		'items' => $items,
		'encodeLabels' => false,
	]);
?>