<?php

namespace app\commands;

use app\models\Account;
use app\models\Worker;
use Yii;
use yii\base\Exception;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use function mb_strlen;
use function preg_match;
use function sprintf;

class AdminController extends Controller
{
    /**
     * Adds new user.
     * @return int
     * @throws Exception
     */
    public function actionAdd()
    {
        $login = $this->prompt(
            'Enter new user login:',
            [
                'required' => true,
                'validator' => static function ($input, &$error) {
                    if (!preg_match('/^[0-9a-zA-Z@._+]{1,255}$/', $input)) {
                        $error = 'Login must be shorter than 256 characters and consist only of latin letters, digits, and @, _, +, or dot!';
                        return false;
                    }

                    if (Account::find()->where(['fld_login' => $input])->exists()) {
                        $error = 'Provided login is already taken!';
                        return false;
                    }

                    return true;
                }
            ]
        );

        if (!empty($login)) {
            $firstName = $this->prompt(
                'Enter new user first name:',
                [
                    'required' => true,
                    'validator' => static function ($input, &$error) {
                        if (mb_strlen($input, 'UTF-8') > 255) {
                            $error = 'First name must be shorter than 255 characters!';
                            return false;
                        }

                        return true;
                    }
                ]
            );

            if (!empty($firstName)) {
                $lastName = $this->prompt(
                    'Enter new user last name:',
                    [
                        'required' => true,
                        'validator' => static function ($input, &$error) {
                            if (mb_strlen($input, 'UTF-8') > 255) {
                                $error = 'Last name must be shorter than 255 characters!';
                                return false;
                            }

                            return true;
                        }
                    ]
                );

                if (!empty($lastName)) {
                    $password = $this->prompt(
                        'Enter new user password:',
                        [
                            'required' => true,
                            'validator' => static function ($input, &$error) {
                                if (mb_strlen($input, 'UTF-8') < 6) {
                                    $error = 'Password must be longer than 6 characters!';
                                    return false;
                                }

                                return true;
                            }
                        ]
                    );

                    if (!empty($password)) {
                        $role = $this->select(
                            'Enter new user role (1 - Editor, 2 - Partner)',
                            [
                                Worker::TYPE_EDITOR => 'Editor',
                                Worker::TYPE_PARTNER => 'Partner',
                            ]
                        );

                        if (!empty($role)) {
                            $worker = new Worker();
                            $worker->fld_email = '';
                            $worker->fld_status_id = Worker::STATUS_ACTIVE;
                            $worker->fld_first_name = $firstName;
                            $worker->fld_last_name = $lastName;
                            $worker->fld_type_id = $role;
                            $worker->fld_mobile = '';
                            $worker->fld_description = '';

                            if (!$worker->save()) {
                                $this->stdout("Error while saving worker!\n");
                                $this->stdout(Console::errorSummary($worker, ['showAllErrors' => true]));
                                return ExitCode::UNSPECIFIED_ERROR;
                            }

                            $user = new Account();
                            $user->fld_login = $login;
                            $user->fld_email = '';
                            $user->fld_first_name = $firstName;
                            $user->fld_last_name = $lastName;
                            $user->fld_password = Yii::$app->security->generatePasswordHash($password);
                            $user->fld_status_id = Account::STATUS_ACTIVE;
                            $user->fld_worker_id = $worker->fld_id;
                            $user->fld_reset_password = '';

                            if (!$user->save()) {
                                $this->stdout("Error while saving account!\n");
                                $this->stdout(Console::errorSummary($user, ['showAllErrors' => true]));
                                return ExitCode::UNSPECIFIED_ERROR;
                            }

                            $this->stdout("Account has been added.\n");
                        }
                    }
                }
            }
        }

        return ExitCode::OK;
    }

    /**
     * Changes user's password.
     * @return int
     * @throws Exception
     */
    public function actionChangePassword()
    {
        $login = $this->prompt(
            'Enter login of the user you want to change password for:',
            [
                'required' => true,
                'validator' => static function ($input, &$error) {
                    if (!Account::find()->where(['fld_login' => $input])->exists()) {
                        $error = 'Provided login does not exist!';
                        return false;
                    }

                    return true;
                }
            ]
        );

        if (!empty($login)) {
            $password = $this->prompt(
                'Enter new user password:',
                [
                    'required' => true,
                    'validator' => static function ($input, &$error) {
                        if (mb_strlen($input, 'UTF-8') < 6) {
                            $error = 'Password must be longer than 6 characters!';
                            return false;
                        }

                        return true;
                    }
                ]
            );

            if (!empty($password)) {
                $account = Account::findByUsername($login);
                $account->fld_password = Yii::$app->security->generatePasswordHash($password);
                if (!$account->save(false, ['fld_password'])) {
                    $this->stdout("Error while saving account!\n");
                    return ExitCode::UNSPECIFIED_ERROR;
                }

                $this->stdout("Account's password has been changed.\n");
            }
        }

        return ExitCode::OK;
    }
}
