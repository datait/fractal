<?php

namespace datait\fractal\search;

use datait\fractal\models\Search;
use datait\fractal\models\Worker;
use Yii;

class WorkerSearch extends \yii\base\Model {
	public $view;
	public $type;
	public $search;

	public function rules() {
		return [
			[['type'], 'integer'],
		];
	}

	public function attributeLabels() {
		return [
			'type' => 'Typ',
		];
	}

	public static function getFilterTable() {
		return [
			'type',
			'search',
		];
	}

	public static function getTypeFilter($view, $current = null) {
		$return[] = [
			'label' => '(bez ogr.)',
			'url' => Yii::$app->urlManager->createUrl(['/worker/index', 'type' => 'null']),
			'options' => [
				'class' => is_null($current) ? 'active' : '',
			]
		];

		foreach (Worker::getTypes() as $key => $value) {
			$return[] = [
				'label' => $value['fld_name'],
				'url' => Yii::$app->urlManager->createUrl(['/worker/index', 'type' => $key]),
				'options' => [
					'class' => !is_null($current) && $key == $current ? 'active' : '',
				]
			];
		}

		return $return;
	}

	public function search() {
		$query = Worker::find()->where(['fld_deleted_at' => null])->orderBy('fld_last_name, fld_first_name');

		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => $query,
		]);

		$query->andFilterWhere(['fld_type_id' => $this->type]);

		return $dataProvider;
	}

	public function saveFilters($params) {
		Search::saveFilters($params, $this->view, self::getFilterTable());
	}

	public function loadFilters() {
		foreach (Search::loadFilters($this->view) as $filter) {
			$this[$filter->fld_name] = $filter->fld_value;
		}
	}
}
