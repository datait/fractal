<?php

namespace datait\fractal\search;

use datait\fractal\models\Feature;
use datait\fractal\models\Search;

class FeatureSearch extends \yii\base\Model {
	public $view;
	public $search;

	public static function getFilterTable() {
		return [
			'search',
		];
	}

	public function search() {
		$query = Feature::find()->orderBy('fld_name');

		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => $query,
		]);

		return $dataProvider;
	}

	public function saveFilters($params) {
		Search::saveFilters($params, $this->view, self::getFilterTable());
	}

	public function loadFilters() {
		foreach (Search::loadFilters($this->view) as $filter) {
			$this[$filter->fld_name] = $filter->fld_value;
		}
	}
}
