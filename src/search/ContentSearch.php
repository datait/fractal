<?php

namespace datait\fractal\search;

use datait\fractal\models\Content;
use datait\fractal\models\Search;

class ContentSearch extends \yii\base\Model {
	public $view;
	public $parent;
	public $search;
	public $sort;

	public function attributeLabels() {
		return [
			'parent' => 'Rodzic',
		];
	}

	public static function getFilterTable() {
		return [
			'parent',
			'search',
		];
	}

	public function search() {
		$query = Content::find()->orderBy($this->sort);

		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => $query,
		]);

		$query->andWhere(['fld_parent_id' => $this->parent]);

		return $dataProvider;
	}

	public function saveFilters($params) {
		Search::saveFilters($params, $this->view, self::getFilterTable());
	}

	public function loadFilters() {
		foreach (Search::loadFilters($this->view) as $filter) {
			$this[$filter->fld_name] = $filter->fld_value;
		}
	}
}
