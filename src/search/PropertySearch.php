<?php

namespace datait\fractal\search;

use datait\fractal\models\Property;
use datait\fractal\models\Search;

class PropertySearch extends \yii\base\Model {
	public $view;
	public $content;
	public $search;

	public function attributeLabels() {
		return [
			'content' => 'Zawartość',
		];
	}

	public static function getFilterTable() {
		return [
			'content',
			'search',
		];
	}

	public function search() {
		$query = Property::find()->orderBy('fld_sort');

		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => $query,
		]);

		$query->andWhere(['fld_content_id' => $this->content]);

		return $dataProvider;
	}

	public function saveFilters($params) {
		Search::saveFilters($params, $this->view, self::getFilterTable());
	}

	public function loadFilters() {
		foreach (Search::loadFilters($this->view) as $filter) {
			$this[$filter->fld_name] = $filter->fld_value;
		}
	}
}
