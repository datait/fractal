<?php

namespace datait\fractal\perm;

use datait\fractal\models\Account;
use datait\perm\models\Perm;
use Yii;

class FractalPerm extends Perm
{
    /**
     * @return bool
     */
    public function isCmsUser(): bool
    {
        $user = Yii::$app->user;

        if ($user->isGuest) {
            return false;
        }

        return $user->identity->isInRole(Account::ROLE_CMS_USER);
    }

    /**
     * @param $types
     * @return bool
     */
    public function isWorkerType($types): bool
    {
        $user = Yii::$app->user;

        if ($user->isGuest) {
            return false;
        }

        if ($user->identity->ident) {
            return is_string($types)
                ? $user->identity->ident->fld_type_id == $types
                : is_array($types)
                    && in_array(
                        $user->identity->ident->fld_type_id,
                        $types
                    );
        }

        return false;
    }
}