<?php

namespace datait\fractal\perm;

use datait\fractal\models\Worker;

class FractalFeaturePerm extends FractalPerm
{
    /**
     * @return bool
     */
    public function create(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        $isTypeOk = $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);

        $hasProperties = $this->params['feature']->properties;

        return $isTypeOk
            && !$hasProperties;
    }

    /**
     * @return bool
     */
    public function index(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);
    }

    /**
     * @return bool
     */
    public function view(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);
    }
}