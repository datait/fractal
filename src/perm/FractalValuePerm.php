<?php

namespace datait\fractal\perm;

use datait\fractal\models\Worker;

class FractalValuePerm extends FractalPerm
{
    /**
     * @return bool
     */
    public function delete(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);
    }

    /**
     * @return bool
     */
    public function activate(): bool
    {
        $istypeOk = $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);

        $isStatusActive = $this->params['worker']->fld_status_id == Worker::STATUS_ACTIVE;

        return $istypeOk
            && !$isStatusActive;
    }

    /**
     * @return bool
     */
    public function create(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);
    }

    /**
     * @return bool
     */
    public function deactivate(): bool
    {
        $istypeOk = $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);

        $isStatusActive = $this->params['worker']->fld_status_id == Worker::STATUS_ACTIVE;

        return $istypeOk
            && $isStatusActive;
    }
}