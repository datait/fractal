<?php

namespace datait\fractal\perm;

use datait\fractal\models\Content;
use datait\fractal\models\Worker;

class FractalContentPerm extends FractalPerm
{
    /**
     * @return bool
     */
    public function view(): bool
    {
        $isWorkerTypeEditor = $this->isWorkerType(Worker::TYPE_EDITOR);
        $isWorkerTypeWebmasterOrPartner = $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);

        $isContentHide = $this->params['content']->getParam('hide') == 'true';

        return $isWorkerTypeEditor
            || (
                $isWorkerTypeWebmasterOrPartner
                && !$isContentHide
            );
    }

    /**
     * @return bool
     */
    public function update(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
            Worker::TYPE_EDITOR,
        ]);
    }

    /**
     * @return bool
     */
    public function activate(): bool
    {
        $isTypeOk = $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
            Worker::TYPE_EDITOR,
        ]);

        $isContentActive = $this->params['content']->fld_active_id == Content::ACTIVE_YES;

        return $isTypeOk
            && !$isContentActive;
    }

    /**
     * @return bool
     */
    public function deactivate(): bool
    {
        $isTypeOk = $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
            Worker::TYPE_EDITOR,
        ]);

        $isContentActive = $this->params['content']->fld_active_id == Content::ACTIVE_YES;

        return $isTypeOk
            && $isContentActive;
    }

    /**
     * @return bool
     */
    public function index(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
            Worker::TYPE_EDITOR,
        ]);
    }

    /**
     * @return bool
     */
    public function help(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        $hasContents = $this->params['content']->contents;

        $isEditor = $this->isWorkerType(Worker::TYPE_EDITOR);
        $isPartner = $this->isWorkerType(Worker::TYPE_PARTNER);
        $isWebmaster = $this->isWorkerType(Worker::TYPE_WEBMASTER);

        if (
            (
                $isPartner
                || $isWebmaster
            )
            && !$hasContents
        ) {
            return true;
        } elseif (
            $isEditor
            && $this->params['content']
            && $this->params['content']->root->getParam('canDelete') != 'false'
            && !$hasContents
        ) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function create(): bool
    {
        $isEditor = $this->isWorkerType(Worker::TYPE_EDITOR);
        $isPartner = $this->isWorkerType(Worker::TYPE_PARTNER);
        $isWebmaster = $this->isWorkerType(Worker::TYPE_WEBMASTER);

        if (
            $isPartner
            || $isWebmaster
        ) {
            return true;
        } elseif (
            $isEditor
            && isset($this->params['content'])
            && $this->params['content']->root->getParam('canCreate') != 'false'
        ) {
            return true;
        }

        return false;
    }
}