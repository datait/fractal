<?php

namespace datait\fractal\perm;

class FractalDashboardPerm extends FractalPerm
{
    /**
     * @return bool
     */
    public function index(): bool
    {
        return $this->isCmsUser();
    }
}