<?php

namespace datait\fractal\perm;

use datait\fractal\models\Worker;

class FractalLogPerm extends FractalPerm
{
    /**
     * @return bool
     */
    public function index(): bool
    {
        return $this->isWorkerType([
            Worker::TYPE_WEBMASTER,
            Worker::TYPE_PARTNER,
        ]);
    }
}