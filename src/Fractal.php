<?php

namespace datait\fractal;

use datait\fractal\models\Worker;

class Fractal extends \yii\base\Module {
    public $defaultRoute = 'dashboard';
    public $viewPath = '@vendor/datait/fractal/views';
    public $layoutPath = '@vendor/datait/fractal/views/layout';

    public $classMap;
}